<?php
session_start();
$thispage = $_SERVER['PHP_SELF'];
include_once("checklang.php");
$_SESSION['age'] = 0;
//CONTROLEER TAAL
if (isset($_GET['lng'])) {
	$lng = $_GET['lng'];
	$_SESSION['lng'] = $lng;
} else {
	if (isset($_SESSION['lng'])) $lng = $_SESSION['lng'];
	else $lng = 0;
}
$_SESSION['langfile'] = check_lang($lng);
include_once($_SESSION['langfile']);

//CONTROLEER IMG PATH
if ($lng==0) $imgpath = "nl/";
elseif ($lng==2) $imgpath = "en/";
else $imgpath = "fr/";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="imagetoolbar" content="no">
<title>index</title>
<link rel="stylesheet" href="css/style_general.css" type="text/css" media="screen" />
<script src="js/sifr.js" type="text/javascript"></script>
  <script type="text/javascript">
  //<![CDATA[
  sIFR.prefetch({
    src: 'din.swf',
    highsrc: 'din.swf'
  });

  sIFR.compatMode = true;
  sIFR.activate();

  sIFR.replace({
    selector: 'h2',
    src: 'dinB.swf',
    highsrc: 'dinB.swf',
	wmode: 'transparent',
	 css: {
      '.sIFR-root': { 'color': '#2b2b2b' },
	  'a': { 'text-decoration': 'none' },
      'a:link': { 'color': '#2b2b2b' },
      'a:hover': { 'color': '#FFFFFF' }
    }
  });
  //]]>
  // ageAlert
  function showAgeAlert(lng) {
	 switch (lng){
		case 0:
		alert("Sorry, om onze site te bekijken moet je minstens 18 jaar zijn.");
		break;
		case 1:
		alert("Désolé, pour visiter notre site vous devez au moins avoir 18 ans.");
		break;
		case 2:
		alert("Sorry, to visit our site you have to be at least 18 years old.");
		break;
		default:
		alert("Sorry, to visit our site you have to be at least 18 years old.");
	  }
  }

  </script>
  <script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); </script> <script type="text/javascript"> try { var pageTracker = _gat._getTracker("UA-4619313-23"); pageTracker._trackPageview(); } catch(err) {}</script>
</head>

<body>
<div id="wrapper">
	<div id="header">
    	<div id="lng">
        	<br />
            <h1><a href=""><img src="img/logoBrew.png" height="90" border="0" width="150" alt="Brewery Visit" /></a></h1>
        </div>
        <div id="flashcontent">
        </div>
	</div>
	<div id="nav">
		<ul id="navlist">
			<li> </li>
        </ul>
    </div>
    <div id="container">
    	<div id="cont_t"> </div>
        <div id="content">
        	<div id="welcome">
            	<img src="img/welcome.gif" width="114" height="18" border="0" alt="welcome" />
            </div>
            
<!--
  <LI>Breweryvisits.com is tijdelijk onbereikbaar om plaats te maken voor de vernieuwde website. Vanaf morgen kan u hier weer terecht.
<LI>Breweryvisits.com est momentanément inaccessible. Veuillez revenir dès demain pour consulter notre nouveau site.
<LI>Breweryvisits.com is temporarily unavailable. Please come back tomorrow to visit the new site.
  -->          

          <div id="lngblckNL">
                OM DEZE SITE TE BEKIJKEN<br />
                MOET JE 18 JAAR OF OUDER ZIJN.<br />
                BEN JE 18 JAAR OF OUDER?<br /><br />
                <center><span><a href="home.php?age=1&lng=0" class="btn_ja"></a> <a href="javascript:showAgeAlert(0);" class="btn_nee"></a></span></center>
            </div>
            <div id="lngblckFR">
            	CE SITE EST RESERVE AUX<br />
                PERSONNES DE 18 ANS ET PLUS.<br />
                TU AS 18 ANS OU PLUS?<br /><br />
                <center><span><a href="home.php?age=1&lng=1" class="btn_oui"></a> <a href="javascript:showAgeAlert(1);" class="btn_non"></a></span></center>
            </div>
            <div id="lngblckEN">
            	TO VISIT THIS SITE<br />
                YOU MUST BE 18 OR OLDER.<br />
                ARE YOU 18 OR OLDER?<br /><br />
                <center><span><a href="home.php?age=1&lng=2" class="btn_yes"></a> <a href="javascript:showAgeAlert(2);" class="btn_no"></a></span></center>
            </div>
          
      </div>
        <div id="cont_b"> </div>
    </div>
    <div id="footer">
    	<span class="ftr_l">Anheuser-Busch InBev &copy; 2009 - <a href="">Terms and conditions</a></span><span class="ftr_r">Beer brewed carefully, to be consumed with care.- Une bière brassée avec savoir, se déguste avec sagesse. - Bier met liefde gebrouwen,drink je met verstand.</span>
    </div>
</div>
</body>
</html>
