<?

global $TEXT;



$TEXT["title"]="InBev Belgium - Site Internet Visites Brasserie";

$TEXT["logo"]="Visites brasserie";
$TEXT["sorry"]="Désolé, vous n'avez pas l'âge requis pour entrer sur ce site.";
$TEXT["booking"]="RESERVER";

$TEXT["menu_info"]="Brasserie";
$TEXT["leffe_menu_info"]="Musée";
$TEXT["menu_tourist"]="Informations touristiques";
$TEXT["menu_shop"]="Magasin";
$TEXT["menu_route"]="Itinéraire";
$TEXT["menu_reservation"]="Réservations";

$TEXT["select"]="<p>Choisissez une brasserie:</p>";

$TEXT["stellaartois_info"]="<p>La brasserie est située dans la ville universitaire de Louvain.  La technologie la plus récente et six siècles de tradition brassicole garantissent une bière de qualité, plein de vigueur et d’arôme: Stella Artois.</p><p>Lors de votre visite vous découvrez tous les secrets du processus de brassage. Vous visitez la salle de brassage, la filtration et les lignes de soutirage, où vous pouvez voir remplir des canettes, des bouteilles et des fûts à une vitesse vertigineuse.</p><p>Chaque visite se termine par une délicieuse Stella Artois bien fraîche.  Pour nos visiteurs de moins de 18 ans, des boissons non-alcoolisée sont prévues. </p><p>Après votre visite vous avez l’occasion de visiter notre Shop Stella Artois.  Votre guide vous aidera avec plaisir.</p>";
$TEXT["stellaartois_info"].="<p><B>NOUVEAU!<br/>Vous n’êtes pas libres en journée? Stella Artois vous offre maintenant la possibilité de visiter la brasserie en soirée!<br/>Pour la première fois, la brasserie ouvrira ses portes aussi pendant les mois d’été, du mardi au samedi!</b></p>";

$TEXT["jupiler_info"]="<p>Découvrez le secret et le goût unique de la bière la plus appréciée en Belgique lors d’une visite à la brasserie Jupiler. </p><p>Vous visitez la salle de brassage, où vous découvrirez tous nos ingrédients naturels qui sont utilisés pour brasser la bière. Après la salle de contrôle, vous visitez la filtration et les lignes de soutirage où vous pouvez voir remplir des bouteilles à une vitesse vertigineuse.</p><p>Chaque visite se termine par une délicieuse Jupiler bien fraîche dans notre nouvelle salle de réception. Pour nos visiteurs de moins de 18 ans, des boissons non-alcoolisée sont prévues. </p><p>Après votre visite vous êtes le bienvenu dans le nouveau Shop Jupiler.  Votre guide vous aidera avec plaisir.</p>";
$TEXT["bellevue_info"]="<p>L’histoire de Belle-Vue commence depuis déjà 80 ans, en 1913. Lambic, la base de toutes les bières Belle-Vue, est brassée avec du blé, du moût, de l’eau et du houblon suranné. Mais comment se déroule dans la pratique le processus de brassage ?</p><p>Dans la première partie “interactive” de votre visite, vous visitez la salle de brassage, le bac de refroidissement et le local des accises de la vieille brasserie. Lors de la deuxième partie de la visite, votre guide vous prendra dans les caves énormes de la brasserie où des milliers de tonneaux en chêne plein de bière sont stockés. </p><p>A la fin de votre visite, avant la dégustation, vous passez dans la tonnellerie où les tonneaux étaient réparés dans le temps. Pour nos visiteurs de moins de 18 ans, des boissons non-alcoolisée sont prévues.</p><p>Après votre visite vous êtes le bienvenu dans le Shop Belle-Vue.  Votre guide vous aidera avec plaisir.</p>";
$TEXT["hoegaarden_info"]="<p>Cherchez-vous une idée rafraîchissante ?  Venez avec vos amis à la brasserie Hoegaarden.  Interactif est le mot-clé ! La première partie de votre visite démarre dans le jardin de bière où vous pouvez découvrir tous nos ingrédients naturels qui sont utilisés pour brasser la bière. Puis vous visitez la salle de brassage, la salle de fermentation et finalement le conditionnement où vous voyez comment les bouteilles sont remplies. Lors de la deuxième partie de votre visite, vous découvrirez tous les secrets de la bière blanche en parcourrant le circuit interactif. Vous pouvez sentir, goûter, et beaucoup d’autres choses !</p><p>Finalement vous apprendriez à débiter comme un vrai patron de café.  Bien sûr vous pouvez déguster votre propre Hoegaarden ! Pour nos visiteurs de moins de 18 ans des boissons non-alcoolisée sont prévues.</p><p>Après votre visite vous êtes le bienvenu dans le Shop Hoegaarden.  Votre guide vous aidera avec plaisir.</p>";
$TEXT["hoegaarden_tourist"]="<p>Hoegaarden possède de nombreuses curiosités, entre autres l\'église Saint-Gorgonius, la plus grande église rococo du pays et les Jardins de Hoegaarden.</p><p>Hoegaarden est une grosse bourgade du Brabant Flamand, située au confluent de deux rivières le Nermbeek et la Grande Gette, à deux pas de la ville de Tienen (Tirlemont). De mémoire d\'Hoegaardiers (les habitants du village), le pays a toujours connu le brassage de la bière. Mais si Hoegaarden existe depuis un peu plus de 1000 ans, la premère mention de l\'existence d\'une brasserie dans le bourg remonte seulement à ... 1318.</p><p>Plus d’info sur:</p>";

$TEXT["leffe_info"]="<p>Leffe: bière de dégustation par excellence! D’où vient-elle, comment est-elle brassée et surtout quel est son goût? Vous découvrirez bien plus encore pendant une visite au musée de la Leffe, en face de l’abbaye de Leffe.</p>";

$TEXT["stellaartois_tourist"]="<p>Leuven, la capitale de la province du Brabant flamand, doit sa notoriété principalement à son université. En effet, la ville abrite non seulement la plus grande, mais aussi la plus ancienne université de notre pays. Quoi de plus normal, donc, que l’apparence de chaque rue ou place de Leuven ait été définie d’après les bâtiments universitaires édifiés dans des styles variés à des périodes différentes? D’autres monuments font également partie du paysage urbain comme, par exemple, l’hôtel de ville de style gothique tardif datant du 15e siècle et l’église Saint-Pierre, restaurée, où vous pouvez admirer la célèbre «Cène» de Dieric Bouts, sans oublier les innombrables autres églises et magnifiques bâtiments civils, la Halle aux Draps ainsi que le béguinage comptant parmi les plus beaux bâtiments du pays. Les plus beaux espaces verts de la ville sont le Parc Saint-Donat et le Jardin Botanique. En bordure de la ville, vous découvrirez plusieurs grands parcs tels que le Domaine du château d’Arenberg à Heverlee et le Domaine provincial de Kessel-Lo.</p>";
$TEXT["stellaartois_tourist_pic1"]="Hôtel de ville";
$TEXT["stellaartois_tourist_pic2"]="L’église Saint-Pierre";

$TEXT["jupiler_tourist_header"]="<h1>Jupiler: Liège</h1>";
$TEXT["jupiler_tourist"]="<p>Ancienne capitale d'une principauté indépendante pendant plus de huit siècles, Liège, métropole d'une grande richesse architecturale et culturelle, possède 17 musées, beaucoup d'églises et bâtiments civils dignes d'intérêt. Comportant une grande série de théâtres, elle est également fière de son Opéra royal et de son orchestre philharmonique qui sont tous deux largement renommés. Conçue pour le commerce, Liège est dotée d'un palais des congrès, d'un hall de foires et d'expositions et naturellement, dans le centre ville, d’une grande zone piétonnière.</p>";

$TEXT["bellevue_tourist_header"]="<h1>Belle-Vue: Bruxelles</h1>";
$TEXT["bellevue_tourist"]="<p>Bruxelles et les Bruxellois, inimitables, profondément humains. Une ville douée pour le bonheur...Partez à la rencontre de la capitale européenne et revivez les étapes de la création de l'UE à travers des monuments emblématiques.</p><p>Bruxelles foisonne de musées et d'attractions. Aucune ville au monde n'effleure d'aussi près l'étrange et le bizarre. Les amateurs d'art et d'histoire se régaleront à la découverte d'un patrimoine exceptionnel. Dans ses musées, par l'Art Nouveau ou la BD, Bruxelles entretient d'étroits rapports avec l'imaginaire et la beauté.</p>";
$TEXT["bellevue_tourist_pic1"]="Hôtel de ville";
$TEXT["bellevue_tourist_pic2"]="Manneken Pis";


$TEXT["hoegaarden_tourist_pic1"]="FR: Tuinen van Hoegaarden";
$TEXT["hoegaarden_tourist_pic2"]="FR: Subtropisch moerasbos<br/>dat 54,9 miljoen jaar oud is";
$TEXT["hoe_sundays"]="Visites sans réservation chaque dimanche à 14H et 16H sauf en décembre et janvier.";


$TEXT["leffe_tourist"]="<p>Dinant, chel-lieu d'arrondissement, est situé en Belgique, en plein coeur de la Province de NAMUR et constitue la porte de l'Ardenne toute proche. La ville présente la particularité d'être implantée le long du fleuve, ce qui lui a valu d'être appelée \"FILLE DE MEUSE\". En outre, les célèbres rochers aux pieds desquels le fleuve progresse donnent à la cité son visage typique. Dotée d'un tel patrimoine naturel, DINANT constitue une ville touristique et un pôle d'attraction de premier ordre.</p>";
$TEXT["leffe_tourist_pic2"]="L’abbaye de Leffe";

$TEXT["stellaartois_route_header"]="<h1>Comment arriver à la brasserie Stella Artois</h1>";
$TEXT["stellaartois_route"]="<p>Vous allez remarquer que la brasserie Stella Artois a différents bâtiments à Louvain. Pour la visite, nous vous attendons à la Vuurkruisenlaan.</p><p>En venant de Bruxelles, Anvers ou Gand suivez l’autoroute E40 direction Liège.</p><p>En venant de Liège, suivez l’autoroute E40 direction Bruxelles.</p><p>Prenez la route E314 Leuven – Hasselt – Genk. Prenez la sortie 18 “Leuven – Mechelen”. A la sortie prenez à droite, ensuite tout droit. Après le troisième feu rouge vous tournez à votre gauche (le ring autour de Louvain). Vous tournez à droite au deuxième feu rouge, direction Diest – Kessel-Lo. Vous êtes arrivé à la Vuurkruisenlaan. Les bâtiments de la brasserie se trouvent à votre gauche.</p><p>Continuez la route jusqu’à l’entrée principale et présentez-vous à la réception.</p>";

$TEXT["jupiler_route_header"]="<h1>Comment arriver à la brasserie Jupiler</h1>";
$TEXT["jupiler_route"]="<p>En venant de Mons – Charleroi – Namur (E42), direction Liège et reprendre la E40 ring en direction de Aachen à l’échangeur de Loncin. En venant de Bruxelles – Louvain (E40) direction Liège – Aachen et continuer sur la E40 ring direction Aachen. En venant de Antwerpen – Tongeren (E 313) direction de Liège et à la jonction avec la E40-ring, direction Aachen.</p><p>Après avoir traversé la Meuse, suivre direction Liège, Bressoux. En venant de Maastricht – Visé (E25), direction Liège – Bressoux A25 et longer la Meuse. En venant de Aachen – Herve – Verviers (E40), direction Liège – Ardennes et longer la Meuse.</p><p>Sur la A25 prendre la sortie n° 6 Jupille. A la fin de la sortie, vous tournez à gauche et après directement à droite. A la fin de la rue, vous tournez à gauche et après avoir passé 2 rond-points, vous voyez la brasserie à votre gauche.</p>";

$TEXT["bellevue_route_header"]="<h1>Comment arriver à la Brasserie Belle-Vue</h1>";
$TEXT["bellevue_route"]="<p>En venant de la E40 (la Côte) – E19 et A12 (Anvers) – E411 (Namur):<br/>Prenez la sortie n° 13 RO (Ninove – Molenbeek) sur le ring de Bruxelles et suivez la Chaussée de Ninove jusqu’au bout. Traversez le canal Bruxelles – Charleroi. Vous êtes maintenant sur la petite ceinture. Prenez à gauche et suivez le canal jusqu’au premier pont, que vous devez traverser à nouveau. Prenez immédiatement à gauche et vous êtes au Quai du Hainaut. L’entrée pour les visiteurs se trouve au n° 33.</p><p>En venant de la E40 (Liège):<br/>Ou vous prenez le ring direction Mons – Bergen et suivez la description ci-dessus. Vous pouvez également prendre la direction Bruxelles en passant par la place Meiser, le Boulevard Lambermont, jusqu’au Pont Van Praet. Traversez le pont, prenez à gauche et suivez le canal Bruxelles – Charleroi, jusqu’au Quai du Hainaut. L’entrée pour les visiteurs se trouve au n° 33.</p><p>Si vous venez en par les transports en commun, descendez à la station de métro ’Comte de Flandres’, qui se trouve à deux pas de la Brasserie.</p>";

$TEXT["hoegaarden_route_header"]="<h1>Comment arriver à la Brasserie Hoegaarden</h1>";
$TEXT["hoegaarden_route"]="<p>Sur l’autoroute E40 Bruxelles – Liège vous prenez la sortie n° 25 Tienen – Hoegaarden. Prenez à droite Hoegaarden jusqu’au rond-point et suivez les flèches ‘Brouwerij Hoegaarden – Kouterhof’.</p><p>Notre guide vous attend dans le Stoopkensstraat n° 24. Pour les voitures, il y a un parking en face de la brasserie. Pour les autocars, nous vous prions d’utiliser les parkings Paenhuys ou Post, à  200m de la brasserie.</p>";

$TEXT["leffe_route_header"]="<h1>Comment arriver à la musée Leffe</h1>";
$TEXT["leffe_route"]="<p>Venant de l’autoroute E411 vous prenez sortie 19 Spontin.  Vous suivez direction Spontin.  Ensuite vous suivez direction Dinant/Leffe, après quelques kilomètres vers Dinant, vous arrivez à l’abbaye de Leffe.  Le Musée de la Leffe se trouve juste en face de l’abbaye: Rue du Moulin 18.</p><p>La gare de DINANT est intégrée au réseau Inter-City. La gare de Namur, à 28 Km., est le noeud ferroviaire international le plus proche.</p>";

$TEXT["leffe_reservation"]="<p><b>Le musée est ouvert:</b><br/>Avril-Juin et Septembre-Octobre: chaque samedi et dimanche de 13u00 - 18u00<br/>Juillet-Août: de mardi à dimanche de 13u00 - 18u00</p><p><b>Prix par visiteur:</b> 4€<br/>< 12 ans: gratuit</p><p>Le magasin de souvenirs Leffe sera ouvert après chaque visite, votre guide vous aidera avec plaisir</p>";

$TEXT["privacy"]="<h1>Clausule Privacy</h1><p>InBev Belgium nv/sa<br/>Siege social:<br/><br/>Industrielaan 21 Boulevard Industriel 1070 Anderlecht<br/>BTW/TVA: BE 0433.666.709<br/>RPR Brussel/RPM Bruxelles 0433.666.709<br/><br/>Vos données sont enregistrées dans un fichier de la InBev Belgium nv/sa. InBev Belgium nv/sa peut utiliser ces données pour vous tenir informé de ses produits, services et actions futures. Si vous ne souhaitez pas recevoir ces informations, cochez la case suivante.</p><p>La loi sur la protection de la vie privée vous donne le droit de consulter, corriger ou supprimer vos données.</p><p>Vos données personnelles ne seront pas vendues ni communiquées à des tiers.</p>";
$TEXT["booking_gone"]="La réservation a déjà été annulée.";

//----------TRANSLATION1  FROM TIM -------------------------------------->
$TEXT["reservation"]="Réservation";
$TEXT["howmany"]="Combien de personnes y a-t-il dans votre groupe?";
$TEXT["go_day"]="Retourner vers l'aperçu journalier";
$TEXT["go_cal"]="Retourner vers l'aperçu mensuel";
$TEXT["formcontinue"]="Continuer";
$TEXT[error]="<br/><B>A Une erreur c'est produite.</B><br/>";
$TEXT[nodata]=" Le système ne peut pas procéder, l'information introduite n'est pas complète.";
$TEXT[error_groupcode]=" Le système ne peut pas procéder, l'information introduite n'est pas complète.";
$TEXT[confirmation]="Confirmation";
$TEXT[please_confirm]="<br/>Merci pour votre réservation.<br/>Votre information a été sauvegardé et un résumé a été envoyé à <AX_EMAIL><br/>Ce e-mail contient également un lien de confirmation, suiver le lien afin de confirmer réservation.";
$TEXT[group_too_big]="<br/><B>Il y a déjà une réservation pour cette visite.<br/>Votre groupe est trop nombreux pour cette visite.<br/>Sélectionner une date ou une heure alternative pour votre visite.</B>";
$TEXT[link_email]="Merci pour votre réservation.\nCliquer sur le lien ci-dessous pour confirmer ou annuler votre réservation.\n\n<AX_CONFIRMLINK>\n\nBien à vous\nInBev Belgium";
$TEXT[link_email_subject]="E-mail confirmation InBev";


$TEXT[email_optional]="Comme votre groupe n'atteint pas 15 personnes, votre réservation est une 'option'. Si le minimum de 15 personnes n'est pas atteint 2 semaines avant la visite, votre visite sera annulée. Dans ce cas vous sera mis au courant.";
$TEXT[email_link_conf]="Veuillez cliquer sur le lien ci-dessous pour confirmer votre réservation.";
$TEXT[email_link_del]="Cliquez également sur ce lien pour annuler votre réservation.";


$TEXT[booking_confirmed]="Merci de confirmer votre réservation.";
$TEXT[confirmed_email_subject]="Confirmation réservation InBev";
$TEXT[confirmed_email]="Merci de confirmer votre réservation.";
$TEXT[whyspecial]="Bienvenu sur les pages spéciales.";
//----------BOOKING_FORM----------------------

$TEXT["name"]="Nom";
$TEXT["email"]="e-mail";
$TEXT["email2"]="Confirmer e-mail";

$TEXT["lang"]="Langue";
//----------BOOKING_FORM----------------------

$TEXT["fr"]="Français";
$TEXT["nl"]="Néerlandais";
$TEXT["en"]="Anglais";

$day_names = array();
$day_names[0]="Lun";
$day_names[1]="Mar";
$day_names[2]="Mer";
$day_names[3]="Jeu";
$day_names[4]="Ven";
$day_names[5]="Sam";
$day_names[6]="Dim";

$month_names = array();
$month_names[0]="Janvier";
$month_names[1]="Février";
$month_names[2]="Mars";
$month_names[3]="Avril";
$month_names[4]="Mai";
$month_names[5]="Juin";
$month_names[6]="Juillet";
$month_names[7]="Août";
$month_names[8]="Septembre";
$month_names[9]="Octobre";
$month_names[10]="Novembre";
$month_names[11]="Décembre";
//----------TRANSLATION2  FROM TIM -------------------------------------->
$TEXT["warn"]="<span class=\"warn\">!!!</span>&nbsp;";
$TEXT["warn2"]="<font color=\"RED\">!!!</FONT>&nbsp;";
$TEXT["freeslots"]="Période libre pour les visites ";
$TEXT["full"]="Complet";
$TEXT["forr"]=" pour ";
$TEXT["endat"]=" Se termine à ";
$TEXT["startat"]=" Commence à ";
$TEXT["onthe"]=" le ";
$TEXT["people"]=" personnes";
$TEXT["person"]=" personne";
$TEXT["change"]="Changer";
$TEXT["tourlang"]="Langue de la visite ";
$TEXT["tlang"]="Langue visite";

$TEXT["saving"]="Sauvegarder l'information";

$TEXT["group_too_big"]="<B>Ce système de réservation online ne peut être utilisé que pour les groupes de maximum 60 personnes.<br/>Pour les groupes de plus de 60 personnes, envoyer un email à <a href=\"mailto:info@$HTTP_HOST\">info@$HTTP_HOST</a>.</b>";
$TEXT["optional"]="* Cette session ne contient pas un assez grand nombre de visiteurs pour être confirmé.";


//----------BOOKING_FORM----------------------
$TEXT["booking_form_title"]="Formulaire de réservation";
$TEXT["name"]="Nom";
$TEXT["fname"]="Prénom";
$TEXT["sname"]="Nom";

$TEXT["street"]="Rue";
$TEXT["num"]="Nr.";
$TEXT["zip"]="Code postal";
$TEXT["city"]="Ville";
$TEXT["country"]="Pays";
$TEXT["mobile"]="Portable";
$TEXT["mobile_message"]="possibilité de vous contacter le jour même en cas de problèmes.";
$TEXT["gname"]="Groupe/Nom de société";
$TEXT["dept"]="Département";
$TEXT["vcount"]="Age visiteur";
$TEXT["junior"]="Agé de 0-11";
$TEXT["senior"]="Agé de 12 ans ou plus";
$TEXT["student"]="&nbsp;";  // only for Stella empty for others, gets set in brewery_data.inc.php
$TEXT["payment"]="Payement";
$TEXT["pay1"]="Cash";
$TEXT["pay2"]="Carte de crédit";
$TEXT["pay3"]="Bancontact";
$TEXT["pay4"]="Facture (min. 25&euro;)";

$TEXT["invoice"]="Facturee";
$TEXT["details"]="Détails";
$TEXT["vat"]="TVA";
$TEXT["cname"]="Nom de la société";
$TEXT["cstreet"]="Adresse";
$TEXT["cnum"]="Nr.";
$TEXT["czip"]="Code postale";
$TEXT["ccity"]="Ville";

$TEXT["vname"]="Nom";
$TEXT["vemail"]="e-mail";
$TEXT["vage"]="Age";




$TEXT["email"]="e-mail";
$TEXT["lang"]="Langue";

$TEXT["fr"]="Français";
$TEXT["nl"]="Néerlandais";
$TEXT["en"]="Anglais";

$TEXT["group_details"]="Information groupe";

$TEXT[vip]="Je voudrais réserver une visite <b>VIP</b> pour mon groupe<br/>";
$TEXT[itw]="Je suis employé d’InBev et je voudrais réserver une visite exclusive. Pour cela j’utiliserai mon numéro <b>‘costcenter’</b><br/>";
$TEXT[costcenter]="costcenter";
$TEXT[notspecial]=" Aucune des options mentionnées ci-dessus<br/>";
$TEXT[legal]="InBev Belgium nv/sa<br/>Siege social:<br/><br/>Industrielaan 21 Boulevard Industriel 1070 Anderlecht<br/>BTW/TVA: BE 0433.666.709<br/>RPR Brussel/RPM Bruxelles 0433.666.709<br/><br/>Désirez-vous être tenu au courant des nouveaux produits, services et initiatives de la société InBev Belgium nv/sa?<br/>Complétez d'abord le formulaire ci-dessus et cliquez sur \"<b>Continuer</b>\"<br/>Si vous n'êtes pas intéressé(e), cliquez directement sur \"<b>Continuer</b>\" <br/>La loi relative à la protection de la vie privée voit octroie le droit de consulter, corriger et supprimer vos coordonnées. Celles-ci ne seront en aucun cas vendues ou communiquées à des tiers.<br/>";


$TEXT["draught"]="Vous voulez-vous initier à l’art de servir une bonne bière ?<br/><br/>Des cours sont organisés pour les étudiants (uniquement).<br/> Rendez vous chaque  Mardi, Mercredi et Jeudi de 14.30 à 17.00<br/>(Min 15 personnes – max 25 personnes)<br/>Pour réserver envoyez un e-mail à l'adresse suivante: <a href=\"mailto:breweryvisitsbelgium@inbev.com\">breweryvisitsbelgium@inbev.com</a>";




//----------END OF TRANSLATION FROM TIM -------------------------------------->


$TEXT["discount"]="Étudiants: 8.50€ seulement sur présentation de la carte d’étudiant ";
$TEXT["formcancel"]="Annuler";
$TEXT["next"]="Suivant";
$TEXT["prev"]="Précédent";
$TEXT["visitor"]="Visiteur";
$TEXT["groupsize"]="nombre de personnes";
$TEXT["special"]="Arrangements Spéciales";
$TEXT[pleaseselect]="Sélectionnez une des options suivantes:<br/>";
$TEXT["underconstruction"]="<H1>En construction</h1>En construction<br/>";

$TEXT["special"]="Arrangement spécial";
$TEXT["lastminute"]="Last Minute";
$TEXT["fields_required"]="<b>les champs suivants doivent être complétés pour réserver une visite:</b><br/>";

$TEXT["nomatch_members"]="Le nombre total de personnes défini par l'âge ne correspond pas au nombre de personnes pour qui la réservation a été faite";
$TEXT["noadult"]="Au moins un membre du groupe doit être un adulte";

$TEXT["emails_nomatch"]="Les adresses e-mail ne coorespondent pas.";
$TEXT["invoice_fields"]="Les données suivantes sont demandées pour établir une facture:";

$TEXT["wrongdate"]="<b>Reservation ne peut être faite pour ce jour</b><br/>";
$TEXT["nodate"]="<b>Reservation ne peut être faite sans une date valable.</b><br/>";
$TEXT["nospecials"]="<b>La réservation ne peut pas se faire dans cette periode car cette date a déjà été bloquée.</b><br/>";

$TEXT["lastminute_group_too_big"]="<B>Last minute réservations peut être faite pour un groupe avec maximum 15 personnes.</b>";



$TEXT["invoicedetails"]="Données pour le facturation";
$TEXT["cancelbooking"]="Cet email a déjà été confirmé, voulez vous annuler votre réservation?";
$TEXT["cancelbooking_yes"]="Oui annuler ma réservation. Je suis conscient que mes places seront perdues et je dois de nouveau réserver.";
$TEXT["del_booking_button"]="Annuler ma réservation";
$TEXT["confirm_ok_askdel"]='cet email de réservation a été confirmé.<br/>Si vous voulez annuler votre réservation, svp utilisez le lien ci-dessous.<br/>';
$TEXT["cancellation"]='Annulation';
$TEXT["notunique"]="Le code de réservation n'est pas unique et ne peut être annulé.";
$TEXT["reservation_deleted"]= "Votre réservation a été annulée.";
$TEXT["lostemail"]="Si vous avez perdu le lien de réservation, svp utilisez le formulaire ci-dessous pour faire une nouvelle confirmation.";
$TEXT["enteremail"]="Svp indiquez le même email de groupe qu'utilisez pour la réservation.";
$TEXT["resendemail"]="Envoyer Email";
$TEXT["email_notfound"]="L'email <b><EMAIL></b> ne peut être trouvé dans notre système de réservation. <br/>Svp soyez certain que cette email est le même qu'utilisé pour faire la réservation.";
$TEXT["reminder_sent"]="Un email de rappel vous a été envoyé à";

$TEXT["email_notfound2"]="Aucune réservation valide n'a été trouvé pour cette addresse email: <b><EMAIL></b>. <br/>Assurez-vous que cette addresse email est la même que pendant la réservation.";
$TEXT["costcenterdetails"]="Détails de cost Center";
$TEXT["email_notok"]="Email syntax de <b><AX_EMAIL></b> n'est pas valide.";
$TEXT["cancel_toolate"]="Il est trop tard pour annuler votre réservation online, appelez le numéro suivant pour annuler votre réservation.<br/>Tel. +32 (0)70/222 914";

$TEXT[cancelinfo]="Veuillez utiliser ce lien si vous voulez annuler votre réservation.";

$TEXT["not25"]="&euro;&nbsp;Montant trop bas. Factures sont seulement possibles pour un montant de 25&euroet plus.";
$TEXT[open]="Ouvert";
$TEXT[lastminute]="Last Minute";
$TEXT[closed]="Fermé";
$TEXT[past]="Date passé";
$TEXT[frozen]="Pas Possible";
$TEXT[bad_date]="Cette date n'est pas valable, veuillez choisir une nouvelle date.";

$TT="<B><font color=\"RED\">NL</font></b>:";  // TT = TO TRANSLATE
//----------------------------------------------------------------------------
//NEW TO BE TRANSLATED
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
// END
//----------------------------------------------------------------------------







// Old stuff following

$format1="<br/><br/><br/><center>";


$TEXT["not_registered"]="User is not registered.";
$TEXT["wrong_password"]="Invalid Login.";
$TEXT["nochmal"]="Please try again.";

$TEXT["group_exists"]="<B>Group <GROUP> already exists.</B>";

$TEXT["email_exists"]="<B>E-mail <EMAIL> already exists.</B>";

$TEXT["nogetgroup"]="Cannot access group \$editid</B></center>";

$HELP["customise"]="To customise a standard template complete the following steps:<br/><li>select a standard template</li><li>complete the dynamic content fields <li>Pressing save will create a new template in your uploaded folder";



$TITLE["index.htm"]="Welcome";

?>
