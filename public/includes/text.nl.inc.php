<?

global $TEXT;

$TEXT["title"]="InBev Belgium - Website Brouwerij Bezoeken";
$TEXT["logo"]="Brouwerij bezoeken";
$TEXT["sorry"]="Sorry, deze site is enkel toegankelijk vanaf 18 jaar.";
$TEXT["booking"]="RESERVEREN";

$TEXT["menu_info"]="Brouwerij";
$TEXT["leffe_menu_info"]="Museum";
$TEXT["menu_tourist"]="Toeristische Info";
$TEXT["menu_shop"]="Shop";
$TEXT["menu_route"]="Routebeschrijving";
$TEXT["menu_reservation"]="Reservatie";

$TEXT["select"]="<p>Kies een brouwerij:</p>";

$TEXT["stellaartois_info"]="<p>De brouwerij Stella Artois is gelegen in de universiteitsstad Leuven, waar zes eeuwen brouwtraditie en de modernste technologie borg staan voor een smaakvol en pittig kwaliteitsbier: Stella Artois. </p><p>Tijdens uw bezoek aan de brouwerij ontdekt u alle geheimen achter het brouwproces. U bezoekt de brouwzaal, de filtratie en de afvullingslijnen, waar u kunt zien hoe aan een duizelingwekkend tempo zowel flessen, blikken als vaten gevuld worden.</p><p> Uiteraard wordt het bezoek op gepaste wijze afgesloten met een overheerlijke Stella Artois.  Voor minderjarigen is er frisdrank voorzien. </p><p>Na uw bezoek kan u eveneens terecht in onze Stella Artois Shop.  Uw gids helpt u graag verder.</p>";
$TEXT["stellaartois_info"].="<p><b>NIEUW!<br/>Bent u overdag niet vrij? Stella Artois biedt nu ook de mogelijkheid om de brouwerij �s avonds te bezoeken!<br/>Voor het eerst opent de brouwerij ook zijn deuren tijdens de zomermaanden, en dit van dinsdag tot en met zaterdag!</b></p>";

$TEXT["jupiler_info"]="<p>Ontdek tijdens een boeiend bezoek aan de brouwerij Jupiler de unieke smaak en het geheim achter het meest populaire pilsbier van Belgi�.  U bezoekt eerst en vooral de brouwzaal waar u de basisingredi�nten leert kennen. Daarna bezoekt u de filtratie en tot slot de spectaculaire afvullingslijnen, waar u kunt zien hoe aan een duizelingwekkend tempo zowel flessen als vaten gevuld worden. Uiteraard wordt het bezoek op gepaste wijze afgesloten met een overheerlijke Jupiler. Voor minderjarigen is er frisdrank voorzien.</p><p>Na uw bezoek kan u eveneens terecht in onze splinternieuwe Jupiler Shop. Uw gids helpt u graag verder.</p>";
$TEXT["bellevue_info"]="<p>Het verhaal van Belle-Vue begint ruim 80 jaar geleden, in 1913. Lambic, de basis van alle Belle-Vue bieren, wordt gebrouwen van tarwe, mout, water en overjarige hop. Hoe gaat dat nu in zijn werk? In het eerste �interactieve� deel van de rondleiding bezoekt u de brouwzaal, het koelschip en de accijnskamer van de vroegere Belle-Vue brouwerij. Tijdens het tweede deel van uw bezoek neemt de gids u mee in de enorme kelders van de brouwerij waar duizenden eiken houten vaten opgeslagen liggen. Alvorens over te gaan tot het proeven, lopen we nog even langs bij de kuiperij waar vroeger de houten tonnen hersteld werden. Voor minderjarigen is er frisdrank voorzien.</p><p>Na uw bezoek kan u eveneens terecht in onze Belle-Vue Shop. De gids helpt u graag verder.</p>";
$TEXT["hoegaarden_info"]="<p>Op zoek naar een verfrissend idee? Kom met je vrienden naar de brouwerij van Hoegaarden. Interactiviteit is het sleutelwoord. Het eerste deel van het bezoek start in de biertuin van de brouwerij waar u de verschillende natuurlijke ingredi�nten kan ontdekken. Vervolgens bezoekt u de brouwzaal, de gisting en tot slot de afvulling, waar u kunt zien hoe de flesjes gevuld worden. Tijdens het tweede deel van het bezoek ontdekt u doorheen het interactieve parcours alle geheimen van het Hoegaards witbier. U kunt er voelen, proeven, ruiken en nog zoveel meer! Tot slot leert u nog tappen als een echte caf�baas en mag u uiteraard uw zelfgetapte Hoegaarden degusteren. Voor minderjaren zijn er frisdranken voorzien.</p><p>Na uw bezoek kan u eveneens terecht in de Hoegaarden Shop. Uw gids helpt u graag verder.</p>";
$TEXT["leffe_info"]="<p>Leffe: het degustatiebier bij uitstek! Vanwaar komt het, hoe wordt het gebrouwen en vooral hoe smaakt het? Dit en nog veel meer kan je ontdekken tijdens een bezoek aan het Leffe museum recht tegenover de alombekende abdij van Leffe.</p>";

$TEXT["stellaartois_tourist"]="<p>Leuven geniet vooral haar bekendheid als universiteitsstad. Hier is immers niet alleen de grootste, maar ook de oudste universiteit van het land gevestigd. Het uitzicht van menig straat of plein is bepaald door universiteitsgebouwen, daterend uit verschillende stijlperioden. Ook een aantal andere monumenten typeren het stadsbeeld, het 15de-eeuwse, laatgotische stadhuis en de Sint-Pieterskerk, waar u het beroemde �Laatste Avondmaal� van Dirk Bouts kan bewonderen.  En dan zijn er de talloze andere kerken en burgelijke gebouwen, de lakenhalle en het begijnhof dat tot de mooiste van het land behoort.</p><p>Bezoek zeker het Stedelijk Museum Vander Kelen-Mertens, waar een indrukwekkende verzameling kunstwerken een uitstekend overzicht bieden van de hoogstaande creativiteit in Brabant tussen de 15de en de 18de eeuw. Aan de rand van de stad bevinden zich enkele grote parken zoals het kasteeldomein Arenberg te Heverlee en het Provinciedomein te Kessel-Lo.</p>";
$TEXT["stellaartois_tourist_pic1"]="Stadhuis";
$TEXT["stellaartois_tourist_pic2"]="Sint-Pieterskerk";

$TEXT["jupiler_tourist_header"]="<h1>Jupiler: Luik</h1>";
$TEXT["jupiler_tourist"]="<p>De stad Luik is de voormalige hoofdplaats van een zelfstandig prinsdom gedurende meer dan acht eeuwen. De stad is befaamd wegens de rijkdom van haar architecturale en culturele erfgoed, de 17 musea, de talrijke kerken en burgerlijke gebouwen. De Koninklijke Opera, het filharmonische orkest en de schouwburgen hebben een befaamdheid verworven die onze grenzen overschrijdt. En ook een winkelstad: boetieks, het winkelcentrum en de voetgangersstraten evenals de jaarbeurshallen trekken talrijke bezoekers aan.</p>";

$TEXT["bellevue_tourist_header"]="<h1>Belle-Vue: Brussel</h1>";
$TEXT["bellevue_tourist"]="<p>Brussel en de Brusselaars. Onnavolgbaar, door en door menselijk. Een stad waar het geluk u toelacht�Vertrek op ontdekking naar de hoofdstad van Europa en herbeleef de verschillende etappes van de creatie van de Europese Unie terwijl u de verschillende monumenten bezichtigt. Brussel bruist van de musea en attracties. Nergens ter wereld is een stad zo in de greep van het bizarre en het ongerijmde.</p><p>Van de serres op het Koninklijk Domein tot het Koninklijk Museum voor Midden-Afrika: een onvergetelijke wandeling .Brussel, waar creativiteit de vrije teugels krijgt en schoonheid kind aan huis is. Art Nouveau, schilderkunst, de wereld van de strips? U zegt het maar.</p>";
$TEXT["bellevue_tourist_pic1"]="Stadhuis";
$TEXT["bellevue_tourist_pic2"]="Manneken Pis";

//$TEXT["hoegaarden_tourist"]="<p>Hoegaarden is een van de kleinere gemeenten in de jonge provincie Vlaams-Brabant. Hoegaarden was ooit de hoofdplaats van een autonoom graafschap onder gravin Alpa�dis. Nadien bleef Hoegaarden doorheen de woelige Middeleeuwen een eigenzinnige enclave van het prinsbisdom Luik, binnen het gebied van het hertogdom Brabant. De monumentale Sint-Gorgoniuskerk fungeert als een baken voor wandelaars en fietsers in de wijde omgeving.</p><p>Jaarlijks zakken duizenden toeristen naar Hoegaarden af om er de unieke sfeer te proeven van een zes eeuwen oude biercultuur (met het hoogtepunt in de 18de eeuw) en er even tot rust te komen in oase van groen in onze thematuinen.</p>";
$TEXT["hoegaarden_tourist"]="<p>Hoegaarden heeft heel wat bezienswaardigheden onder andere de Sint Gorgoniuskerk, de grootste kerk in rococo-stijl van het land en de Tuinen van Hoegaarden</p><p>Het is een uitgestrekt dorp gelegen in Vlaams Brabant, aan de samenloop van de beek Nerm en de rivier Grote Gete, vlak bij de stad Tienen. Dat er sinds mensenheugenis in Hoegaarden bier wordt gebrouwen, weet elke Hoegaardier. Maar Hoegaarden is iets ouder dan 1000 jaar en de eerste vermelding van een brouwerij op het grondgebied dateert van... 1318.</p><p>Meer info op:</p>";
$TEXT["hoegaarden_tourist_pic1"]="Tuinen van Hoegaarden";
$TEXT["hoegaarden_tourist_pic2"]="Subtropisch moerasbos<br/>dat 54,9 miljoen jaar oud is";
$TEXT["hoe_sundays"]="Bezoeken zonder reservatie op zondag om 14 en 16u behalve in december en januari";

$TEXT["leffe_tourist"]="<p>Dinant ligt in Belgi�, in het hartje van de provincie NAMEN en maakt deel uit van de Ardennen in de directe omgeving. Bij het zien van de stad lijkt het net alsof zij is opgenomen door de rivier, waardoor ze ook wel \"DOCHTER VAN DE MAAS\" wordt genoemd. Voorts geven de vermaarde rotsen, aan de voet waarvan de rivier stroomt, het stadscentrum z�n eigen, zo typerende, aanblik. Geheel opgaand in zo�n natuurlijke omgeving, maakt DINANT tot een toeristische stad en een centrum met eerste klas attracties.</p>";
$TEXT["leffe_tourist_pic2"]="Abdij van Leffe";

$TEXT["stellaartois_route_header"]="<h1>Hoe bereikt u de brouwerij Stella Artois</h1>";
$TEXT["stellaartois_route"]="<p>U zal merken dat er in Leuven verschillende gebouwen van de brouwerij zijn. Voor het bezoek verwachten we u in de Vuurkruisenlaan z/n.</p><p>Komende van Brussel, Antwerpen, Gent volgt u de E40 richting Luik.</p><p>Komende van Luik, volgt u op de E40 richting Brussel.</p><p>Neem de E314 richting Leuven � Hasselt � Genk  Neem vervolgens afrit 18 �Leuven-Mechelen�. Aan de afrit draait u rechtsaf en vervolgens rijdt u rechtdoor. Na de derde lichten draait u linksaf (= ring om Leuven). Aan de tweede lichten draait u rechtsaf, richting Diest � Kessel-Lo. U bent nu op de Vuurkruisenlaan. De gebouwen van de brouwerij liggen aan uw linkerkant.</p><p>Rij verder tot aan de hoofdingang en meld u aan de receptie aan.</p>";

$TEXT["jupiler_route_header"]="<h1>Hoe bereikt u de brouwerij Jupiler</h1>";
$TEXT["jupiler_route"]="<p>Komende van Mons � Charleroi � Namen (E42) richting Luik, richting Aachen volgen aan de verkeerswisselaar van Loncin. Komende van Brussel � Leuven (E40), richting Luik � Aachen en verder de E-40 ring volgen richting Aachen. Komende van Antwerpen � Tongeren (E313) richting Luik en aan de verbinding met de E40 � ring richting Aachen.</p><p>Na het oversteken van de Maas richting Li�ge � Bressoux volgen (A25). Komende van Maastricht � Vis� (E25) richting Li�ge � Bressoux en steeds de Maas volgen. Komende van Aachen � Herve � Verviers (E40) richting Li�ge � Bressoux en steeds de Maas volgen.</p><p>Op de A25 neemt u afrit 6 Jupille. Op het einde van de afrit gaat u links en onmiddellijk rechts. Op het einde van deze straat slaat u linksaf. Na het tweede ronde punt ziet u op uw linkerzijde de brouwerij Jupiler.</p>";

$TEXT["bellevue_route_header"]="<h1>Hoe de brouwerij Belle-Vue bereiken</h1>";
$TEXT["bellevue_route"]="<p>Vanaf E40 (kust) � vanaf E19 en A12 (Antwerpen) � vanaf E411 (Namen):<br/>U volgt de Ring richting Mons/Bergen tot afrit 13RO (Ninove � Molenbeek). Links over de ring rijdt u tot aan het einde van de Ninoofse Steenweg. U steekt het kanaal Brussel - Charleroi over. U komt op de kleine ring. U slaat links af en volgt het kanaal tot aan de eerste brug die u opnieuw oversteekt. Onmiddellijk terug links langs het kanaal. U bevindt zich nu op de Henegouwenkaai. De ingang voor bezoeken bevindt zich op het nummer 33.</p><p>Vanaf E40 (Luik):<br/>Ofwel via grote ring eerst richting Gent, dan richting Mons/Bergen en nadien bovenstaande beschrijving volgen. Ofwel richting Brussel en via uitrit Meiser en de Lambermontlaan tot aan de Van Praetbrug. Over de brug links afslaan en het kanaal Brussel - Charleroi volgen. Via de Havenlaan kruist u de Leopold II laan (Saincteletteplein) en volgt u nog steeds het kanaal. De ingang van de brouwerij (voor bezoeken) is gelegen aan de Henegouwenkaai 33.</p><p>Met het openbaar vervoer komt u via het metrostation �Graaf van Vlaanderen� heel dicht in de buurt.</p>";

$TEXT["hoegaarden_route_header"]="<h1>Hoe de brouwerij Hoegaarden bereiken</h1>";
$TEXT["hoegaarden_route"]="<p>Op de E40 Brussel-Luik neemt u uitrit 25 (Tienen-Hoegaarden). Rechts Hoegaarden volgen tot aan het rondpunt. Daarna volgt u de witte pijlen �Brouwerij Hoegaarden-Kouterhof�. Het is in de Stoopkensstraat nr. 24 waar onze gids op u wacht.</p><p>Voor auto�s is er parking tegenover de brouwerij. Autocars dienen te parkeren op 200 m van de brouwerij: parking Paenhuys (Stoopkensstraat) of parking Post (Brouwerij Loriersstraat).</p>";

$TEXT["leffe_route_header"]="<h1>Leffe Routebeschrijving</h1>";
$TEXT["leffe_route"]="<p>Vanop de E411 neemt u afrit 19 Spontin, volg de N948 richting Spontin. Vervolgens volgt u de richting Dinant/Leffe tot u na een afdaling beneden aan de Abdij van Leffe uitkomt.  Recht tegenover de abdij bevindt zich het Leffe museum: Rue du Moulin 18.</p><p>Het station van Dinant is opgenomen in het Inter-City net. Station Namen, op 28 km afstand, is het dichtstbijzijnde internationale station.</p>";
$TEXT["leffe_reservation"]="<p><b>Het museum is geopend:</b><br/>April-Juni en September-Oktober: elke zaterdag en zondag van 13u00 - 18u00<br/>Juli-Augustus: van dinsdag tot en met zondag 13u00 - 18u00</p><p><b>Prijs per bezoeker:</b> 4�<br/>< 12 jaar: gratis</p><p>De cadeau shop van Leffe is open na elk bezoek, uw gids zal u graag assisteren</p>";

$TEXT["privacy"]="<h1>Privacy</h1><p>InBev Belgium nv/sa<br/>Siege social:<br/><br/>Industrielaan 21 Boulevard Industriel 1070 Anderlecht<br/>BTW/TVA: BE 0433.666.709<br/>RPR Brussel/RPM Bruxelles 0433.666.709<br/><br/>Uw gegevens worden opgenomen in een bestand van de InBev Belgium nv/sa. InBev Belgium nv/sa kan deze gegevens gebruiken om u informatie over toekomstige producten, diensten en initiatieven te verstrekken. Wenst u deze informatie niet te ontvangen, kruis dan dit vakje aan.</p><p>De wet op de bescherming van de persoonlijke levenssfeer verleent u het recht van inzage, verbetering en schrapping van uw gegevens.</p><p>Uw persoonsgegevens worden niet verkocht noch doorgegeven aan derden.</p>";

$TEXT["booking_gone"]="De reservatie werd reeds geannuleerd.";

//----------TRANSLATION1  FROM TIM -------------------------------------->
$TEXT["reservation"]="Reservatie";
$TEXT["howmany"]="Uit hoeveel personen bestaat uw groep?";
$TEXT["go_day"]="Ga terug naar dag overzicht";
$TEXT["go_cal"]="Ga terug naar maand overzicht";
$TEXT["formcontinue"]="Ga verder";
$TEXT[error]="<br/><B>A Er deed zich een probleem voor.</B><br/>";
$TEXT[nodata]=" De opgegeven informatie is niet compleet, het systeem kan niet verder gaan.";
$TEXT[error_groupcode]=" De opgegeven informatie is niet compleet, het systeem kan niet verder gaan.";
$TEXT[confirmation]="Bevestiging";
$TEXT[please_confirm]="<br/>Bedankt voor uw reservatie.<br/>Uw reservatie is doorgegeven en een samenvattig is verzonden naar <AX_EMAIL><br/>De email bevat eveneens een bevestigingslink, gelieve via deze link definitief te bevestigen.";
$TEXT[group_too_big]="<br/><B>Een groep reserveerde reeds voor dit bezoek.<br/>Er zijn geen voldoende plaatsen meer vrij voor uw groep.<br/>Gelieve een andere datum of een ander tijdstip te kiezen voor uw bezoek.</B>";

$TEXT[link_email]="Bedankt voor uw reservatie.\nGelieve op onderstaande link te klikken om uw reservatie te bevestigen of annuleren.\n\n<AX_CONFIRMLINK>\n\nVriendelijke groeten\nInBev Belgium";
$TEXT[link_email_subject]="InBev-bevestigings email";

$TEXT[email_optional]="Aangezien uw groep minder dan 15 personen telt, is uw reservatie een 'optie'. Indien het minimum van 15 personen niet behaald wordt 2 weken voor de datum van het bezoek zal uw optie geannuleerd worden. In dit geval ontvangt u een verwittiging van uw annulatie.";
$TEXT[email_link_conf]="Gelieve op onderstaande link te klikken om uw reservatie te bevestigen.";
$TEXT[email_link_del]="Gelieve voor de annulatie van uw reservatie eveneens bovenstaande link te gebruiken.";



$TEXT[booking_confirmed]="Bedankt voor het bevestigen van uw reservatie.";
$TEXT[confirmed_email_subject]="InBev-Reservatie Bevestiging";
$TEXT[confirmed_email]="Bedankt voor het bevestigen van uw reservatie.";
$TEXT[whyspecial]="Welcome to the specials page.";


//----------BOOKING_FORM----------------------
$TEXT["fr"]="Frans";
$TEXT["nl"]="Nederlands";
$TEXT["en"]="Engels";

$day_names = array();
$day_names[0]="Ma";
$day_names[1]="Di";
$day_names[2]="Woe";
$day_names[3]="Do";
$day_names[4]="Vrij";
$day_names[5]="Zat";
$day_names[6]="Zon";


$month_names = array();
$month_names[0]="Januari";
$month_names[1]="Februari";
$month_names[2]="Maart";
$month_names[3]="April";
$month_names[4]="Mei";
$month_names[5]="Juni";
$month_names[6]="Juli";
$month_names[7]="Augustus";
$month_names[8]="September";
$month_names[9]="Oktober";
$month_names[10]="November";
$month_names[11]="December";

//----------TRANSLATION2  FROM TIM -------------------------------------->
$TEXT["warn"]="<span class=\"warn\">!!!</span>&nbsp;";
$TEXT["warn2"]="<font color=\"RED\">!!!</FONT>&nbsp;";
$TEXT["freeslots"]="Vrije bezoeken ";
$TEXT["full"]="Volzet";
$TEXT["forr"]=" voor ";
$TEXT["endat"]=" Eindigt om ";
$TEXT["startat"]=" Start om ";
$TEXT["onthe"]="op ";
$TEXT["people"]="mensen";
$TEXT["person"]=" persoon";

$TEXT["change"]="Verander";
$TEXT["tourlang"]="De rondleiding gaat door in het";
$TEXT["tlang"]="De rondleiding gaat door in het";

$TEXT["saving"]="Informatie wordt opgeslagen";

$TEXT["group_too_big"]="<B>Er kunnen enkel reservaties gemaakt worden voor groepen niet groter dan 60 personen.<br/>Voor groepen groter dan 60 personen, gelieve te mailen naar <a href=\"mailto:info@$HTTP_HOST\">info@$HTTP_HOST</a>.</b>";
$TEXT["optional"]="* Dit bezoek kan nog niet bevestigd worden aangezien er nog geen bezoekers genoeg zijn.";


//----------BOOKING_FORM----------------------
$TEXT["booking_form_title"]="Reservatie formulier";
$TEXT["name"]="Naam";
$TEXT["fname"]="Voornaam";
$TEXT["sname"]="Naam";
$TEXT["street"]="Straat";
$TEXT["num"]="Nr.";
$TEXT["zip"]="Postcode";
$TEXT["city"]="Stad";
$TEXT["country"]="Land";
$TEXT["mobile"]="GSM";
$TEXT["mobile_message"]="dit nummer kan gebruikt worden om u te contacteren in geval van problemen de dag zelf";
$TEXT["gname"]="Groeps - bedrijfsnaam";
$TEXT["dept"]="Departement";
$TEXT["vcount"]="Leeftijd bezoeker";
$TEXT["junior"]="0-11 jaar";
$TEXT["senior"]="12 jaar of ouder";
$TEXT["student"]="&nbsp;";  // only for Stella empty for others, gets set in brewery_data.inc.php
$TEXT["payment"]="Betalingswijze";
$TEXT["pay1"]="Cash";
$TEXT["pay2"]="Krediet kaart";
$TEXT["pay3"]="Bancontact";
$TEXT["pay4"]="Factuur (min. 25&euro;)";

$TEXT["invoice"]="Factuur";
$TEXT["details"]="Details";
$TEXT["vat"]="BTW";
$TEXT["cname"]="Bedrijfsnaam";
$TEXT["cstreet"]="Straat";
$TEXT["cnum"]="Nr.";
$TEXT["czip"]="Postcode";
$TEXT["ccity"]="Stad";

$TEXT["vname"]="Naam";
$TEXT["vemail"]="e-mail";
$TEXT["vage"]="leeftijd";

$TEXT["email"]="e-mail";
$TEXT["email2"]="confirm e-mail";
$TEXT["lang"]="Taal";

$TEXT["fr"]="Frans";
$TEXT["nl"]="Nederlands";
$TEXT["en"]="Engels";

$TEXT["group_details"]="Groep informatie";

$TEXT[vip]="Ik wil een <b>VIP</b> bezoek boeken voor mijn groep<br/>";
$TEXT[itw]=" Ik ben een InBev werknemer en wil een exclusief bezoek boeken. Hiervoor maak ik gebruik van mijn <b>�costcenter�</b> nummer<br/>";
$TEXT[costcenter]="costcenter";
$TEXT[notspecial]="Geen van de hierboven vernoemde opties<br/>";

$TEXT[legal]="InBev Belgium nv/sa<br/>Siege social:<br/><br/>Industrielaan 21 Boulevard Industriel 1070 Anderlecht<br/>BTW/TVA: BE 0433.666.709<br/>RPR Brussel/RPM Bruxelles 0433.666.709<br/><br/>Wenst u op de hoogte gehouden te worden over toekomstige producten, diensten en initiatieven van de InBev Belgium nv/sa? Vul dan de benodigde gegevens hierboven in en klik dan op '<b>ga verder</b>'.<br/>Wenst u dat u gegevens niet worden opgenomen? Klik dan gewoon op '<b>ga verder</b>'.<br/>De wet op de bescherming van de persoonlijke levenssfeer verleent u het recht van inzage, verbetering en schrapping van uw gegevens.<br/>Uw persoonsgegevens worden niet verkocht noch doorgegeven aan derden.<br/>";

$TEXT["draught"]="Tap cursus: enkel mogelijk voor studenten op:<br/>Dinsdag, Woensdag en Donderdag van 14.30 tot 17.00<br/>(Min 15 personen - max 25 personen)<br/>Om te reserveren stuur een mail naar volgend adres: <a href=\"mailto:breweryvisitsbelgium@inbev.com\">breweryvisitsbelgium@inbev.com</a>";

$TEXT["discount"]="Studenten: 7.50€ op vertoon van studentenkaart ";
$TEXT["formcancel"]="Annuleren";
$TEXT["underconstruction"]="<H1>In ontwikkeling</h1>In ontwikkeling.<br/>";

$TEXT["next"]="Volgend";
$TEXT["prev"]="Vorig";
$TEXT["visitor"]="Bezoeker";
$TEXT["groupsize"]="aantal personen";
$TEXT["special"]="Speciale Arrangementen";

$TEXT[pleaseselect]="Gelieve ��n van de volgende opties te kiezen:<br/>";


$TEXT["underconstruction"]="<H1>Under Construction</h1>Webpagina in aanbouw.<br/>";
$TEXT["special"]="Speciaal arrangement";
$TEXT["lastminute"]="Last Minute";
$TEXT["fields_required"]="<b>De volgende gegevens moeten ingevuld worden om te kunnen reserveren:</b><br/>";

$TEXT["nomatch_members"]="De som van het aantal personen per leeftijd komt niet overeen met het totaal aantal personen waarvoor werd gereserveerd.";
$TEXT["noadult"]="Mintens ��n groepslid moet een volwassene zijn.";

$TEXT["emails_nomatch"]="De e-mail adressen stemmen niet overeen met elkaar.";
$TEXT["invoice_fields"]="De volgende gegevens zijn vereist voor het opmaken van een factuur:";


$TEXT["wrongdate"]="<b>Een reservatie is niet mogelijk voor deze dag.</b><br/>";
$TEXT["nodate"]="<b>Zonder een geldige datum kan de reservatie niet gemaakt worden.</b><br/>";
$TEXT["nospecials"]="<b>Speciale reservaties kunnen niet gemaakt worden binnen deze afgesloten periode.</b><br/>";

$TEXT["lastminute_group_too_big"]="<B>Last minute reservaties enkel mogelijk voor groepen niet groter dan 15 personen.</b>";


$TEXT["invoicedetails"]="Facturatiegegevens";
$TEXT["cancelbooking"]="Deze email werd reeds bevestigd, wenst u uw boeking te annuleren?";
$TEXT["cancelbooking_yes"]="Ja, gelieve mijn reservatie te annuleren. Ik ben op de hoogte dat ik hierdoor mijn slot kan verliezen en dat ik een nieuwe reservatie moet maken.";
$TEXT["del_booking_button"]="Verwijder mijn reservatie";
$TEXT["confirm_ok_askdel"]='Deze reservatie email werd al bevestigd.<br/>Indien u uw reservatie wenst te annuleren gelieve onderstaande link te gebruiken.<br/>';
$TEXT["cancellation"]='Annulatie';
$TEXT["notunique"]="U heeft geen unieke reservatie code, de annulatie kan niet worden doorgevoerd.";
$TEXT["reservation_deleted"]="Uw reservatie werd geannuleerd.";
$TEXT["lostemail"]="Als u de link Mijnboeking kwijt bent, gelieve dan het onderstaande formulier te gebruiken om een nieuwe bevestigingsmail aan te maken.";
$TEXT["enteremail"]="Gelieve het email adres in te geven dat u gebruikte bij het maken van uw reservatie.";
$TEXT["resendemail"]="Email verzonden";
$TEXT["email_notfound"]="Het emailadres kon niet worden teruggevonden in ons reservatie systeem <b><EMAIL></b>. <br/>Gelieve na te zien of het ingevoerde email adres overeenkomt met de email die u gebruikte bij het maken van uw reservatie.";
$TEXT["reminder_sent"]="Een herinneringsmail werd zopas verzonden naar <EMAIL>.";

$TEXT["email_notfound2"]="Er kon geen geldige reservatie gevonden overeenkomstig met dit email adres: <b><EMAIL></b>. <br/>Gelieve na te zien of het ingevoerde email adres overeenkomt met de email die u gebruikte bij het maken van uw reservatie.";
$TEXT["costcenterdetails"]="Cost Center Details";
$TEXT["email_notok"]="Email syntax van <b><AX_EMAIL></b> blijkt niet geldig te zijn.";
$TEXT["cancel_toolate"]="De reservatie online annuleren is niet meer mogelijk, gelieve contact op te nemen met .<br/>Tel. +32 (0)70/222 914 om uw reservatie te annuleren";

$TEXT["invoicedetails"]="Factuurgegevens";
$TEXT[cancelinfo]="Gelieve onderstaande link te gebruiken om uw reservatie te annuleren.";



$TEXT["costcenterdetails"]="Cost Center Details";
$TEXT["email_notok"]=" De syntax van deze e-mail <b><AX_EMAIL></b> is incorrect.";

$TEXT["cancel_toolate"]="U kunt niet langer uw reservatie online annuleren, gelieve contact op te nemen op volgend nummer: <br/>Tel. +32 (0)70/222 914 ";
$TEXT["not25"]="&euro;&nbsp;Bedrag te laag. Facturen worden opgemaakt vanaf een min. bedrag van 25�.";
$TEXT[open]="Open";
$TEXT[lastminute]="Last Minute";
$TEXT[closed]="Gesloten";
$TEXT[past]="Datum reeds voorbij";
$TEXT[frozen]="Onmogelijk";
$TEXT[bad_date]="Deze datum is ongeldig, gelieve een nieuwe datum te kiezen.";
$TT="<B><font color=\"RED\">NL</font></b>:";  // TT = TO TRANSLATE
//----------------------------------------------------------------------------
//NEW TO BE TRANSLATED
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// END
//----------------------------------------------------------------------------


// Old stuff following

$format1="<br/><br/><br/><center>";

$TEXT["not_registered"]="User is not registered.";
$TEXT["wrong_password"]="Invalid Login.";
$TEXT["nochmal"]="Please try again.";

$TEXT["group_exists"]="<B>Group <GROUP> already exists.</B>";

$TEXT["email_exists"]="<B>E-mail <EMAIL> already exists.</B>";

$TEXT["nogetgroup"]="Cannot access group $editid</B></center>";

$HELP["customise"]="To customise a standard template complete the following steps:<br/><li>select a standard template</li><li>complete the dynamic content fields <li>Pressing save will create a new template in your uploaded folder";



$TITLE["index.htm"]="Welcome";

?>
