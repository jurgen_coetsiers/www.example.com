<?

global $TEXT;

$TEXT["title"]="InBev Belgium - Brewery Visits Website";

$TEXT["logo"]="Brewery visits";
$TEXT["sorry"]=" Unfortunately, you're not old enough yet to explore this site.";

$TEXT["booking"]="BOOKING";

$TEXT["menu_info"]="Brewery";
$TEXT["leffe_menu_info"]="Museum";
$TEXT["menu_tourist"]="Tourist Info";
$TEXT["menu_shop"]="Shop";
$TEXT["menu_route"]="Route description";
$TEXT["menu_reservation"]="Reservation";

$TEXT["select"]="<p>Please select a brewery:</p>";

$TEXT["stellaartois_info"]="<p>The Stella Artois brewery is situated in the university city of Louvain, where six centuries of brewing tradition and latest technology stand for a refreshing and thirst-quenching quality beer: Stella Artois.</p><p>During your visit to the brewery, you will discover all the secrets of the brewing process. You will visit the brewing hall, the filtration and the filling plant, where you can witness how bottles, cans and kegs are filled at an enormous speed.</p><p>After the visit, you will have the opportunity to taste a delicious Stella. For our visitors under legal drinking age, soft drinks are available. </p><p>After your visit the Stella Artois Shop is open. Your tour guide will be happy to help you.</p>";
$TEXT["stellaartois_info"].="<p><b>NEW!<br/>You can�t reach us during the daytime? No problem because now Stella Artois also offers you the possibility to visit the brewery in the evening! For the first time, the brewery also opens its doors during summertime, from Tuesday until Saturday!</b></p>";
$TEXT["jupiler_info"]="<p>Discover the secret and the unique taste of the most appreciated Belgian beer during a fascinating visit to the Jupiler brewery. First of all you will visit the brewing hall, where you will find out which natural ingredients are used. Then, you will visit the filtration and control room. Additionally you can witness how bottles are filled at an enormous speed. </p><p>Each visit ends of course with a delicious Jupiler! Of course we have soft drinks available for our visitors under legal drinking age. </p><p>After your visit, the tour guide will open the brand new Jupiler Shop and he will be happy to help you during your shopping activities.</p>";


$TEXT["bellevue_info"]="<p>The history of Belle-Vue began almost one century ago, in 1913. Lambic, the basis for all beers of Belle-Vue, is brewed of wheat, malt, water and hops.  But how is it done?</p><p>In the first interactive part of the tour, you will visit the old Belle-Vue brewery with its brewing hall, its tax room and of course its famous cool ship. During the second part of the tour, your tour guide will take you in the enormous fermentation cellars of the brewery where thousands of wooden barrels filled up with Belle-Vue beer are stocked. At the end of the tour, you will also stop at the former coopery, where the huge wooden barrels were repaired. </p><p>Of course, the tasting is included! For our visitors under legal drinking age, soft drinks are available.</p><p>After your visit the Belle-Vue Shop is open.  Your tour guide will be happy to help you.</p>";
$TEXT["hoegaarden_info"]="<P>Looking for a refreshing idea?</p><p>Come and visit with your friends the Hoegaarden brewery with your friends! �Interactivity� is the keyword during this visit. The first part of the visit starts in the Hoegaarden beer garden, where you can discover all different natural ingredients of our beer. Afterwards you will visit the brewing hall, the fermentation hall and the filling plant where you will see how the bottles are filled. In the second �interactive� part of the visit, you will discover all the secrets of the Hoegaarden wheat beer by walking through the interactive circuit. You can smell, taste, feel and lots more! Finally, you will learn how to draw your own refreshing and cloudy Hoegaarden, which you can taste of course. </p>";
$TEXT["hoegaarden_tourist"]="<p>Hoegaarden features numerous attractions, among which the country�s largest rococo church Sint Gorgonius.  Hoegaarden is a fairly large village in Flemish Brabant, located at the junction of the brook 'Nerm' and the river 'Grote Gete', near the town of Tienen. As far back as Hoegaardiers (the inhabitants) can remember, this area has always been brewing. Yet, although Hoegaarden has existed for over 1000 years, no mention is made of a brewery before... 1318.</p><p>More information at:<br/>";

$TEXT["leffe_info"]="<p>Leffe: an outstanding specialty beer! Where does it come from, how is it brewed and most importantly, how does it taste? This and lots more can be discovered during a visit to the Leffe museum, right across the famous Abbey of Leffe.</p>";
$TEXT["stellaartois_tourist"]="<p>Louvain, the capital of Flemish Brabant is mainly known as a university city and thus for its largest and oldest university in Belgium. Many streets and squares are dominated by university buildings dating from various periods and often constructed in a variety of styles. But Louvain is not only famous for its university but also for its numerous monuments, such as the 15th century late gothic city hall, the restored �Sint Pieters� Church, where you can admire Dirk Bouts� famous painting, namely the �Last Supper�. But this isn�t the end yet of the infinite list of monuments. You can also pay a visit to several other churches, beautiful manor houses, the cloth hall and the beguinage, which is by the way one of the most beautiful in Belgium. In the city centre you can also have a nice walk in the �St. Donatus� park and the City Herb garden. On the outskirts of the town, there are a number of large parks such as Arenberg in Heverlee and the provincial park in Kessel-Lo. </p>";


$TEXT["stellaartois_tourist_pic1"]="City Hall";
$TEXT["stellaartois_tourist_pic2"]="St. Peter�s Church";

$TEXT["jupiler_tourist_header"]="<h1>Jupiler: Li�ge</h1>";
$TEXT["jupiler_tourist"]="<p>Li�ge was the former capital of an independent principality for eight centuries. Now it�s the metropolis architectural and cultural wealth. Li�ge counts 17 museums, a lot of churches and manor houses.  Featuring a large series of theatres, it is also proud of its Op�ra Royal and the Philharmonic Orchestra which are both widely renowned. Designed for trade, Li�ge is provided with a congress hall, a fair and exhibition centre and, of course, with a large shopping area reserved for pedestrians. It has always been a city in a good mood, witness the countless, late closing caf�s, the traditional Sunday market La Batte and the outrageous folklore puppet Tchantch�s, symbol of that individualistic spirit and open-mindedness of the Li�geois.</p>";

$TEXT["bellevue_tourist_header"]="<h1>Belle-Vue: Brussels</h1>";
$TEXT["bellevue_tourist"]="<p>Brussels and the people who live there� inimitable, profoundly human� A city filled with happiness. Discover the European capital and relive the different stages of the creation of the EU by visiting some emblematic monuments. Brussels is packed with museums and attractions. No other city in the world combines the strange and the bizarre in such close proximity.</p><p>From the greenhouses of the Royal Palace to the Royal Museum of Central Africa � the most majestic promenades. From Art Nouveau, to painting, to the comic strip, Brussels nourishes close relations between imagination and beauty.</p>";
$TEXT["bellevue_tourist_pic1"]="City Hall";
$TEXT["bellevue_tourist_pic2"]="Manneken Pis";


$TEXT["hoegaarden_tourist_pic1"]="EN: Tuinen van Hoegaarden";
$TEXT["hoegaarden_tourist_pic2"]="EN: Subtropisch moerasbos<br/>dat 54,9 miljoen jaar oud is";

$TEXT["leffe_tourist"]="<p>Dinant is located in Belgium, right at the heart of the Province of Namur, and is the gateway to the Ardennes, which are very close by. The town has the unique feature of being built alongside the river that has earned its title of 'Daughter of the Meuse'. The famous rocky cliffs, at whose feet the river flows by, give the town its characteristic appearance. Endowed with such a natural heritage, Dinant is a tourist centre and a major attraction for visitors.</p>";

$TEXT["leffe_tourist_pic2"]="Abbey of Leffe";

$TEXT["stellaartois_route_header"]="<h1>How to get to the Stella Artois brewery</h1>";
$TEXT["stellaartois_route"]="<p>The Stella brewery has different buildings in Leuven. The visit starts in the VUURKRUISENLAAN.</p>Coming from Brussels, Antwerp, Gent follow highway E40 direction LUIK.<br/>Coming from Li�ge, follow the highway E40 direction BRUXELLES.<br/>Follow highway E314 �LEUVEN-HASSELT-GENK� Take the exit 18 �LEUVEN-MECHELEN�<br/>On the exit turn to the right and go straight on.<br/>After the third light turn left (=the Leuven ringroad, named LUDENSCHEIDSINGEL)<br/>You turn around the �JM Artoisplein�.  At the traffic lights, you take the direction Diest (at your right).  You�re now in the VUURKRUISENLAAN.<br/>Address yourself to the reception.<br/>";

$TEXT["jupiler_route_header"]="<h1>How to get to the Jupiler Brewery</h1>";
$TEXT["jupiler_route"]="<p>Coming from Mons � Charleroi � Namur (E42), direction Li�ge and follow the E40 ring direction Aachen at the cloverleaf of Loncin. Coming from Bruxelles � Leuven (E40) direction Li�ge � Aachen and continue the E40 ring, direction Aachen. Coming from Antwerpen � Tongeren (E313) directon Li�ge and at the traffic junction with the E40 ring, direction Aachen.</p><p>Go across the Meuse and follow the direction Li�ge � Bressoux (A25).</p><p>Coming from Maastricht � Vis� (E25) direction Li�ge � Bressoux and continue along the Meuse. Coming from Aachen � Herve � Verviers (E40), direction Li�ge � Bressoux and continue along the Meuse.</p><p>On the A25 take exit 6 Jupille. At the end of the exit turn left and after 50 metres, turn right. At the end of the street, turn left.  After having passed 2 roundabouts you see the brewery on your left hand side.</p>";

$TEXT["bellevue_route_header"]="<h1>How to get to the Belle-Vue brewery</h1>";
$TEXT["bellevue_route"]="<p>Coming from E40 (Coast) � E19 or A12 (Antwerp) � E411 Namur:<br/>Follow the Ring direction Mons/Bergen until exit 13 (Ninove � Molenbeek). Follow left, over the Ring and continue until the end of the Ninoofse Steenweg. Now you have to cross the canal �Brussels � Charleroi�. You should be on the �small Ring�. Turn left and follow the canal until the first bridge, which you cross again. Immediately turn left and follow the canal again. You are on the Henegouwenkaai now. You will find the visitor�s entrance at n� 33.</p><p>Coming from E40 (Luik/Li�ge):<br/>Either you turn on the Ring, following direction of Mons/Bergen and then continue as described above. Or you drive straight on direction Brussels and at the exit Meiser and passing the Lambermontlaan, you continue until the Van Praet Bridge. Turn left after the bridge and follow the canal Brussels � Charleroi. Passing the Havenlaan, you cross the Leopold II laan (Saincteletteplein) and you continue following the canal. You are on the Henegouwenkaai now. You will find the visitor�s entrance at n� 33.</p><p>Visitors using public transport should get off at the nearby �Comte de Flandre/Graaf van Vlaanderen� metro station.</p>";

$TEXT["hoegaarden_route_header"]="<h1>How to get to the Hoegaarden Brewery</h1>";
$TEXT["hoegaarden_route"]="<p>Leave the highway E40 Brussel � Luik at exit 25 Tienen � Hoegaarden. Follow �Hoegaarden�, until the roundabout, and then follow the sign �Brouwerij Hoegaarden � Kouterhof�.</p><p>Our guide waits for you at the Stoopkensstraat n� 24. For cars there is parking facility opposite to the brewery. For coaches there are parking facilities at the parkings Post or Paenhuys 200 m further on.</p>";
$TEXT["hoe_sundays"]="Visits without reservation: every Sunday at 2pm and 4pm except for January and December.";


$TEXT["leffe_route_header"]="<h1>How to get to the Leffe museum</h1>";
$TEXT["leffe_route"]="<p>Coming from the highway E411 take exit N� 19 Spontin.  Take the National N948 direction Spontin.  Then follow direction Dinant/Leffe.  After a few kilometres you arrive by descending to Dinant at the Abbey of Leffe.  The Leffe museum is situated opposite to the abbey: Rue du Moulin 18</p><p>Dinant station is on the Inter-City network.  Namur Station, 28 km from Dinant, is the nearest international station.</p>";
$TEXT["leffe_reservation"]="<p><b>The museum is open:</b><br/>April-June and September-October: every Saturday and Sunday from 13h00 - 18h00<br/>July-August: From Tuesday till Sunday from 13h00 - 18h00</p><p><b>Price per visitor:</b> 4�<br/>< 12 years: free</p><p>The Leffe gift shop is open after every visit, your tour guide will be happy to help you</p>";

$TEXT["jupiler_shop"]="<p>After your visit, your tour guide will open the Jupiler shop where you can buy lots of Jupiler stuff.  Glasses, t-shirts, caps, key rings and lots of other Jupiler gadgets are only a few examples of our jupiler product range.</p>";



$TEXT["privacy"]="<h1>Privacy Rules</h1><p>InBev Belgium nv/sa<br/>Corporate Address:<br/><br/>Industrielaan 21 Boulevard Industriel 1070 Anderlecht<br/>BTW/TVA: BE 0433.666.709<br/>RPR Brussel/RPM Bruxelles 0433.666.709<br/><br/>InBev Belgium nv/sa respects your privacy. InBev Belgium nv/sa is entitled to use your address and other data in order to inform you about recent publications. If you do not want to receive this information, please mark this box.</p><p>In honoring the right of privacy, the user may exercise his or her rights of objection, access, rectification or cancellation.</p><p>The data collected shall be used for internal purposes only and shall not be supplied to any third parties.</p>";

$TEXT["reservation"]="Reservation";
$TEXT["howmany"]="How many people are in your group?";
$TEXT["go_day"]="Return to day overview";
$TEXT["go_cal"]="Return to month overview";

$TEXT["formcontinue"]="Continue";
$TEXT["formcancel"]="Cancel";
$TEXT[error]="<br/><B>A problem has occurred.</B><br/>";
$TEXT[nodata]=" System cannot proceed because the information given is not complete.";
$TEXT[error_groupcode]=" System cannot proceed because the information given is not complete.";

$TEXT[confirmation]="Confirmation";
$TEXT[please_confirm]="<br/>Thank you for your reservation.<br/>Your information has been saved and a summary sent to <AX_EMAIL><br/>This email also contains a confirmation link, please follow click on the link to confirm this reservation.";
$TEXT[double_book]="<B>A subsequent booking has been made for this slot.<br/>The required space to accomodate your group is no longer available.<br/>Please select an alternative date or time for your visit.</B><br/>";


$TEXT[link_email]="\nThank you for your reservation.\nPlease click on the link below to confirm or cancel this reservation.\n\n<AX_CONFIRMLINK>\n\nYours Sincerely,\nInBev Belgium";
$TEXT[link_email_subject]="InBev-Confirmation email";


$TEXT[email_optional]="Due to the fact that your reservation involves less then 15 people, this reservation is an �option�.  In case of not completing the required amount of 15 people within two weeks before the visit, the reservation will be cancelled. We will notify you of this cancellation.";
$TEXT[email_link_conf]="Please click on the link below to confirm this reservation.";
$TEXT[email_link_del]="To cancel this reservation, please also use this link.";

$TEXT[booking_confirmed]="Thank you for confirming your reservation.";
$TEXT[cancelinfo]="Please use this link if you need to cancel your reservation.";

$TEXT[confirmed_email_subject]="InBev-Reservation Confirmation";
$TEXT[confirmed_email]="Thank you for confirming your reservation.";

$TEXT[pleaseselect]="Please select one of the following options:<br/>";


$TEXT[vip]="I would like to book an exclusive <b>VIP</b> session for my group.<br/>";
$TEXT[itw]="I am an InBev employee and would like to book an exclusive session using my <B>costcenter</b> number.<br/>";
$TEXT[costcenter]="Cost Center";

$TEXT[notspecial]="None of the above.<br/>";


//--date = 070305

$TEXT["warn"]="<span class=\"warn\">!!!</span>&nbsp;";
$TEXT["warn2"]="<font color=\"RED\">!!!</FONT>&nbsp;";
$TEXT["freeslots"]="Available visits ";
$TEXT["full"]="Full";
$TEXT["forr"]=" for ";
$TEXT["endat"]=" Finishing at ";
$TEXT["startat"]=" Starting at ";
$TEXT["onthe"]=" on the ";
$TEXT["people"]=" people";
$TEXT["person"]=" person";
$TEXT["change"]="Change";
$TEXT["tourlang"]="Tour language is ";
$TEXT["tlang"]="Tour language";

$TEXT["saving"]="Saving Information<br/>";

$TEXT["group_too_big"]="<B>This online reservation system can only accomodate group sizes of up to 60 people.<br/>For groups larger than 60 people, please send an email to <a href=\"mailto:info@$HTTP_HOST\">info@$HTTP_HOST</a>.</b>";
$TEXT["group_not_defined"]="<B>The group size is not correct, please insert a correct group size.<br/>For questions or help, please send an email to <a href=\"mailto:info@$HTTP_HOST\">info@$HTTP_HOST</a>.</b>";


$TEXT["optional"]="* This session does not yet contain enough visitors to be confirmed.";


//----------BOOKING_FORM----------------------
$TEXT["booking_form_title"]="Reservation Details";
$TEXT["name"]="Name";
$TEXT["fname"]="first&nbsp;name";
$TEXT["sname"]="last&nbsp;name";
$TEXT["street"]="Street";
$TEXT["num"]="Nr.";
$TEXT["zip"]="Postal code";
$TEXT["city"]="City";
$TEXT["country"]="Country";
$TEXT["mobile"]="Mobile";
$TEXT["mobile_message"]="Possibility to contact you in case of last minute problems";
$TEXT["gname"]="Group&nbsp;Name";
$TEXT["dept"]="Department";
$TEXT["vcount"]="visitors age";
$TEXT["junior"]="0-11 years old";
$TEXT["senior"]="12 and above";
$TEXT["student"]="&nbsp;";  // only for Stella empty for others, gets set in brewery_data.inc.php
$TEXT["payment"]="Payment";
$TEXT["pay1"]="Cash Payment";
$TEXT["pay2"]="Credit card";
$TEXT["pay3"]="Bancontact";
$TEXT["pay4"]="Invoice (min. 25&euro;)";

$TEXT["invoice"]="Invoice";
$TEXT["details"]="Details";
$TEXT["vat"]="VAT";
$TEXT["cname"]="Company Name";
$TEXT["cstreet"]="Street";
$TEXT["cnum"]="Nr.";
$TEXT["czip"]="Postal code";
$TEXT["ccity"]="City";

$TEXT["vname"]="Name";
$TEXT["vemail"]="e-mail";
$TEXT["vage"]="age";

$TEXT["email"]="e-mail";
$TEXT["email2"]="confirm e-mail";
$TEXT["lang"]="Language";

$TEXT["fr"]="French";
$TEXT["nl"]="Dutch";
$TEXT["en"]="English";

$TEXT["group_details"]="Group Details";
//----------CALENDAR----------------------
$day_names = array();
$day_names[0]="Mon";
$day_names[1]="Tue";
$day_names[2]="Wed";
$day_names[3]="Thu";
$day_names[4]="Fri";
$day_names[5]="Sat";
$day_names[6]="Sun";


//----------CALENDAR----------------------
$month_names = array();
$month_names[0]="January";
$month_names[1]="February";
$month_names[2]="March";
$month_names[3]="April";
$month_names[4]="May";
$month_names[5]="June";
$month_names[6]="July";
$month_names[7]="August";
$month_names[8]="September";
$month_names[9]="October";
$month_names[10]="November";
$month_names[11]="December";

$TEXT["discount"]="Students: 8,50€ only with student card";


$TEXT["draught"]="Draught courses:   available for students only on:<br/>Tuesday &amp; Wednesday  and Thursday from 14.30 till 17.00<br/>(Min 15 persons - max 25 persons)<br/>For reservation email: <a href=\"mailto:breweryvisitsbelgium@inbev.com\">breweryvisitsbelgium@inbev.com</a>";
$TEXT["next"]="Next";
$TEXT["prev"]="Previous";
$TEXT["visitor"]="Visitor";
$TEXT["groupsize"]="groupsize";
$TEXT["legal"]="InBev Belgium nv/sa<br/>Corporate Address:<br/><br/>Industrielaan 21 Boulevard Industriel 1070 Anderlecht<br/>BTW/TVA: BE 0433.666.709<br/>RPR Brussel/RPM Bruxelles 0433.666.709<br/><br/>InBev Belgium nv/sa respects your privacy. <br/>InBev Belgium nv/sa is entitled to use your address and other data in order to inform you about recent publications.<br/>If you want to receive this information, please fill in your data, then press '<b>continue</b>'.<br/>If you do not want to receive this information, please press '<B>continue</b>'.<br/>In honoring the right of privacy, the user may exercise his or her rights of objection, access, rectification or cancellation.<br/>The data collected shall be used for internal purposes only and shall not be supplied to any third parties.<br/>";


$TEXT["underconstruction"]="<H1>Under Construction</h1>This page is currently under construction.<br/>";
$TEXT["special"]="Special arrangement";
$TEXT["lastminute"]="Last Minute";
$TEXT["fields_required"]="<b>The following fields are required to complete a booking:</b><br/>";

$TEXT["nomatch_members"]="The total number of people defined by age does not match the number of people for which the booking was made.";
$TEXT["noadult"]="At least one member of the group must be an adult.";
$TEXT["emails_nomatch"]="The email addresses do not match.";
$TEXT["invoice_fields"]="the following fields are required for an Invoice:";

$TEXT["wrongdate"]="<b>Reservation cannot be made for this day.</b><br/>";
$TEXT["nodate"]="<b>Reservation cannot be made without a valid date.</b><br/>";
$TEXT["nospecials"]="<b>Special reservation cannot be made within this $freeze_days day period.</b><br/>";

$TEXT["lastminute_group_too_big"]="<B>Last minute reservations can only accomodate group sizes of up to 15 people.</b>";


$TEXT["invoicedetails"]="Invoice Details";
$TEXT["cancelbooking"]="This email has already been confirmed, would you like to <B>cancel</b> your booking?";
$TEXT["cancelbooking_yes"]="Yes please delete my reservation. I am aware that my places may be lost and a new booking must be made.";
$TEXT["del_booking_button"]="Delete My Reservation";
$TEXT["confirm_ok_askdel"]='This reservation email has already been confirmed.<br/>If you would like to cancel your reservation, please use the link below.<br/>';
$TEXT["cancellation"]='Cancellation';
$TEXT["notunique"]="Reservation code is not unique and cannot be cancelled.";
$TEXT["reservation_deleted"]="Your reservation has been deleted.";
$TEXT["lostemail"]="If you have lost the MyBooking link, please use the form below to generate a new confirmation email.";
$TEXT["enteremail"]="Please enter the same group email as used for the reservation.";
$TEXT["resendemail"]="Send Email";
$TEXT["email_notfound"]="The email <b><EMAIL></b> could not be found in our reservation system. <br/>Please make sure that this is the same email used to make the reservation.";
$TEXT["reminder_sent"]="A reminder email has been sent to <EMAIL>.";

$TEXT["email_notfound"]="No valid reservations could be found for this email address: <b><EMAIL></b>. <br/>Please make sure that this is the same email used to make the reservation.";
$TEXT["costcenterdetails"]="Cost Center Details";

$TEXT["email_notok"]="Email syntax of <b><AX_EMAIL></b> does not appear to be valid.";
$TEXT["cancel_toolate"]="It is too late to cancel your reservation online, please call the following number to cancel your reservation.<br/>Tel. +32 (0)70/222 914";
$TEXT["not25"]="&euro;&nbsp;too low. Invoices are only available for amounts of 25&euro; and above.";

$TEXT[open]="Open";
$TEXT[lastminute]="Last Minute";
$TEXT[closed]="Closed";
$TEXT[past]="Date Passed";
$TEXT[frozen]="Not Possible";
$TEXT[bad_date]="This date is not valid, please choose a different date.";

$TEXT["booking_gone"]="This reservation has already been cancelled.";
//----------------------------------------------------------------------------
//NEW OR CHANGED TO BE TRANSLATED
//----------------------------------------------------------------------------






//----------------------------------------------------------------------------
// Old stuff following
//----------------------------------------------------------------------------
$format1="<br/><br/><br/><center>";

$TEXT["not_registered"]="User is not registered.";
$TEXT["wrong_password"]="Invalid Login.";
$TEXT["nochmal"]="Please try again.";

$TEXT["group_exists"]="<B>Group <GROUP> already exists.</B>";

$TEXT["email_exists"]="<B>E-mail <EMAIL> already exists.</B>";

$TEXT["nogetgroup"]="Cannot access group $editid</B></center>";

$HELP["customise"]="To customise a standard template complete the following steps:<br/><li>select a standard template</li><li>complete the dynamic content fields <li>Pressing save will create a new template in your uploaded folder";


$TITLE["index.htm"]="Welcome";

?>
