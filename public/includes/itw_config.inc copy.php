<?
	$maxgroupsize=30;
	$maxslots=count($timeslots);
	$maxpeople=$maxslots*$maxgroupsize*2;
	
	
	
	$MINCONF=15;  // MINIMUM NUMBER OF PEOPLE REQUIRED FOR A CONFIRM

	$ENABLE_TESTING=0;

	$service='<a href="mailto:service@breweryvisits.com">service@breweryvisits.com</a>';


	// ----------------------------------------------------------------
	// MAX YEAR
	// ----------------------------------------------------------------


	define("MAXYEAR", 2013);
	define("THISYEAR", date("Y"));

	if( ( isset( $_SESSION[ 'SSMYDATE' ] ) && ( (int)substr( $_SESSION[ 'SSMYDATE' ], 0, 4 ) > 2011 ) ) ||
        ( isset( $slotcodeAB ) && ( (int)substr( $slotcodeAB, 1, 4 ) > 2011 ) ) ||
        ( isset( $slotcode ) && ( (int)substr( $slotcode, 1, 4 ) > 2011 ) ) )
	{
		define("STUDENT_PRICE", 7.5);
		define("ADULT_PRICE", 8.5);

        define("SMALL_GROUP_PRICE", 255);
        define("LARGE_GROUP_PRICE", 510);
	}
	else
	{
		define("STUDENT_PRICE", 5);
		define("ADULT_PRICE", 6);

        define("SMALL_GROUP_PRICE", 180);
        define("LARGE_GROUP_PRICE", 360);
	}
	
	
	
	
	// ----------------------------------------------------------------


	$mandatory="<font color=\"BLACK\">&nbsp;*</FONT>";

	$bf=' class="bookform" ';

	$br='<BR>';
	$js_closewin="<BR><BR><center><a href=\"javascript:window.close()\">Close this Window</a></center>";
	$js_back="<BR><BR><center><a href=\"javascript:history.go(-1)\">Back</a></center>";


	// THIS ENABLES CHANGING SESSION VAR WHICH OVERRIDES DEFAULT ESSENTIALLY FOR TESTING LAST MINUTE OPTIONS.
	if((!isset($_SESSION['SS_FREEZE']))||($_SESSION['SS_FREEZE']==0))
		$freeze_days=21;
	else
		$freeze_days=$_SESSION['SS_FREEZE'];

	//------------------------------------------------------------------------------------
	// ICONS
	//------------------------------------------------------------------------------------
	$icon_del='<img src="/images/icons/i_del.gif" alt="Delete" border=0>';
	$icon_edit='<img src="/images/icons/i_edit.gif" alt="Edit" border=0>';
	$icon_info='<img src="/images/icons/i_info.gif" alt="Info" border=0>';


	//------------------------------------------------------------------------------------
	// ICONS
	//------------------------------------------------------------------------------------
	$table='<table border="0" cellpadding="0" cellspacing="0" width="100%">';
	$table1='<table border="1" cellpadding="0" cellspacing="0" width="100%">';
	$table5='<table border="0" cellpadding="0" cellspacing="5" width="100%">';


	//------------------------------------------------------------------------------------
	// SET UP TIMING RESTRAINTS
	//------------------------------------------------------------------------------------


	$FirstDayOfWeek=1;     // used in datefromweeknr function, it would appear that due to the time difference we get a wrong date


	$freeze_default=21;   // DURATION OF FREEZE BOOKING PERIOD
	$nobook_days=2;		 // ABSOLUTE LOCK OUT OF BOOKINGS X DAYS FROM NOW
	$nocancel_days=14;		 // ABSOLUTE LOCK OUT OF BOOKINGS X DAYS FROM NOW

	$spd=86400;  // SECONDS PER DAY
	$spd2=43200;  // SECONDS PER 12 hours


	$FREEZE_SECONDS=$freeze_days*$spd;
	$NOBOOK_SECONDS=($nobook_days*$spd)+$spd2;

	$NOCANCEL_SECONDS=$nocancel_days*$spd;

	//1209600;  // 2 WEEKS

	$infoecho="1";  // (0 = no)   (1= yes IMG )  (5= yes table)



	$axessinfoecho=1;
	$d8infoecho=1;

	$allflags=$nl_flag.$fr_flag.$en_flag;

	$email_headers = 'From: info@' . $_SERVER['SERVER_NAME'] . "\r\n" .
   'MIME-Version: 1.0' . "\n" . 'Content-type: text/plain; charset=iso-8859-15' . "\r\n" .
   'Reply-To: info@' . $_SERVER['SERVER_NAME'] . "\r\n" .
   'X-Mailer: PHP/' . phpversion();

	//---------------------
	// INITALISE VARIABLES
	//---------------------

	$FEEDBACK="";



?>