Reservation Cancellation
Your <AX_BREWERY> brewery tour reservation for <AX_BOOKCOUNT> people on the <AX_DATE> has been cancelled using your cancellation link.