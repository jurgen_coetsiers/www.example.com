Bevestigingsbrief Brouwerij Jupiler
Welkom in onze Brouwerij Jupiler!

Op <AX_DATE> om <AX_TIME_START> verwachten wij u met <AX_GROUP_COUNT> personen in de Brouwerij Jupile, Avenue J. Prevers z/n, 4020 Jupille, bij het gebouw �Onthaal� (gelegen tegenover de �March� de Li�ge�) en nemen u mee op ontdekkingstocht doorheen de brouwerij. 

<AX_OPTION>

<AX_CONFIRMLINK>

Bij vragen over uw reservatie kunt u contact opnemen met de helpdesk op het nummer: + 32 70/222 914.

Het bezoek wordt gepast afgesloten met een heerlijk verfrissende Jupiler. Voor bezoekers onder de 18 jaar zijn er frisdranken voorzien.
Na uw bezoek kan u terecht in onze Jupiler Shop; uw gids helpt u met veel plezier verder.

Om <AX_TIME_END> nemen we afscheid.

Betaling ter plaatse van <AX_TOTAL_PRICE> Euro

Mogen wij u vragen deze timing strikt op te volgen; voor en na u volgen er nog groepen. In dringende gevallen (file,...) kan u steeds contact met ons opnemen:
0032 16 27 62 30
0032 16 27 79 80


Veel plezier!
Bedankt voor uw reservatie.


Vriendelijke groeten

Het Brewery Visits Team
