Confirmation letter Jupiler Brewery 
Welcome to the Jupiler Brewery!

On <AX_DATE> at <AX_TIME_START> hours you are expected with <AX_GROUP_COUNT> people in the Jupiler Brewery at the �Reception� building (opposite to the �March� de Li�ge) to take you on a discovery trip through the brewery. 

<AX_OPTION>

<AX_CONFIRMLINK>

If you have any questions concerning your reservation, please contact our helpdesk: +32 70/222 914

After the tour we invite you to have a delicious and refreshing Jupiler. For visitors under legal drinking age, soft drinks are served.
After your visit, the Jupiler Shop is open; your tour guide will be happy to help you.

The visit will end at <AX_TIME_END> hours.

To pay upon arrival:  <AX_TOTAL_PRICE> �

May we kindly ask you to respect this timing? In case of emergency (traffic jam...), please contact us:
0032 16 27 62 30
0032 16 27 79 80


We look forward to welcoming you and sharing the experience of Jupiler.

Thank you for your reservation.


Yours Sincerely,

The Brewery Visits Team

		