<h1>%AX_BOOKING_FORM_TITLE%</h1>

<table border="0" cellpadding="0" cellspacing="0">
<form method="POST" >
<input type="hidden" name="reservation" value=1>
<input type="hidden" name="slotcode" value="%AX_VARSLOTCODE%">
<input type="hidden" name="slotcodeAB" value="%AX_VARSLOTCODEAB%">
<input type="hidden" name="bookcount" value="%AX_VARGROUPCOUNT%">


%AX_FUNSLOTCD%

<TR>
	<TD class="bookformup">Comments:%AX_VARMANDATORY%</td>
	<TD class="bookform">
		<textarea cols="40" rows="5" name="bookcomments" onKeyDown="limitText(this.form.bookcomments,this.form.countdown,230);"
onKeyUp="limitText(this.form.bookcomments,this.form.countdown,230);">%AX_VARBOOKCOMMENTS%</textarea><BR>
		<input readonly type="text" name="countdown" size="3" value="230"> characters left.

	</TD>
</TR>
<TR>
	<TD class="bookform" width="120">%AX_FNAME%, %AX_SNAME%:%AX_VARMANDATORY%</TD>
	<TD class="bookform">
		<input name="bookfname" maxlength="50" style="width: 150px" value="%AX_VARBOOKFNAME%">
		<input name="booksname" maxlength="50" style="width: 150px" value="%AX_VARBOOKSNAME%">
	</TD>
</TR>
<TR>
	<TD class="bookform">%AX_STREET%, %AX_NUM%:%AX_VARMANDATORY%</TD>
	<TD class="bookform">
		<input name="bookstreet" maxlength="50" style="width: 150px" value="%AX_VARBOOKSTREET%">
		<input name="bookhnum" maxlength="5" style="width: 50px" value="%AX_VARBOOKHNUM%">
	</TD>
</TR>
<TR>
	<TD class="bookform">%AX_ZIP%, %AX_CITY%:%AX_VARMANDATORY%</TD>
	<TD class="bookform">
		<input name="bookzip" maxlength="8"  style="width: 60px" value="%AX_VARBOOKZIP%">
		<input name="bookcity" maxlength="50"  style="width: 140px" value="%AX_VARBOOKCITY%">
	</TD>
</TR>
<TR>
	<TD class="bookform">%AX_COUNTRY%:%AX_VARMANDATORY%</TD>
	<TD class="bookform">%AX_INCCOUNTRY%</TD>
</TR>
<TR>
	<TD class="bookform">%AX_EMAIL%:%AX_VARMANDATORY%</TD>
	<TD class="bookform"><input name="bookemail" maxlength="50"  style="width: 150px" value="%AX_VARBOOKEMAIL%"></TD>
</TR>
<TR>
	<TD class="bookform">%AX_MOBILE%:%AX_VARMANDATORY%</TD>
	<TD class="bookform"><input onkeydown="f(this)" onkeyup="f(this)" onblur="f(this)" onclick="f(this)" name="bookmobile" maxlength="50"  style="width: 150px" value="%AX_VARBOOKMOBILE%" ></TD>
</TR>
<TR>
	<TD class="bookform">%AX_GNAME%:%AX_VARMANDATORY%</TD>
	<TD class="bookform"><input name="bookgname" maxlength="50"  style="width: 150px" value="%AX_VARBOOKGNAME%"></TD>
</TR>
<TR>
	<TD class="bookform">%AX_DEPT%:%AX_VARMANDATORY%</TD>
	<TD class="bookform"><input name="bookdept" maxlength="50"  style="width: 150px" value="%AX_VARBOOKDEPT%"></TD>
</TR>
<TR>
	<TD class="bookform">%AX_VCOUNT%:%AX_VARMANDATORY%</TD>
	<TD class="bookform">%AX_INCAGE%</TD>
</TR>
<TR>
	<TD class="bookform">%AX_TLANG%:%AX_VARMANDATORY%</TD>
	<TD class="bookform">%AX_FUNCHECKLANG%</TD>
</TR>
<TR>
	<TD class="bookform">&nbsp;</TD>
	<TD class="bookform">&nbsp;</TD>
</TR>
%AX_INCEXTRA_BOOKING_PAYMENT%
<TR>
	<TD class="bookform">&nbsp;</TD>
	<TD class="bookform">&nbsp;</TD>
</TR>
%AX_INCEXTRA_BOOKING_INVOICE%

%AX_FUNEXTRABOOK%

<table border="0" cellpadding="0" cellspacing="0">
<TR>
	<TD class="bookform">&nbsp;</TD>
	<TD class="bookform">&nbsp;</TD>
</TR>
<TR>
	<TD class="bookform">&nbsp;</TD>
	<TD class="bookform">
		<input type="submit" value="%AX_FORMCONTINUE%" style="width: 160px">
		<input onclick="javascript:location.href='admin_showday.html'" type="button" value="%AX_FORMCANCEL%" style="width: 160px">
	</TD>
</TR>
</form>
</table>
