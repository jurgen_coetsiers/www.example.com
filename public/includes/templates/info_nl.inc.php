<div class="fliesstext" style="width: 370px; float: left;">
<h1>Welkom op de %AX_VARMYBREWERY% reservatie site!</h1>
<p>U kunt hier tot 3 weken op voorhand uw rondleiding in de %AX_VARMYBREWERY% brouwerij boeken. Het minimum aantal personen voor een rondleiding is 15 personen. Het maximum aantal personen per rondleiding is 60 personen.</p>
<p>Bent u met minder dan 15 personen dan kan u een optie nemen tot 3 weken op voorhand of last-minute aansluiten bij een bestaande groep tot 2 dagen voor de rondleiding.</p>
<p><b>Om te reserveren klik hier: <a href="showmonth.htm?novip">reserveren</a></b></p>
%AX_FUNHOESUNDAY%
<br/>
<p><b>Voor last-minute aansluitingen, klik hier:&nbsp;<a href="showmonth.htm?lm=1&amp;novip">last-minute</a></b></p>
<p>Prijs per bezoeker:<br/>
0-11 jaar: gratis<br/>
+12 jaar: 8,50&euro;<br/>
%AX_FUNDISCOUNT%
<br/>
<i>Nota: voor bezoekers onder de 18 jaar zijn er frisdranken voorzien.</i></p>
<!--<p><b>Exclusieve bezoeken en VIP bezoeken:</b></p>-->
<b>Voor individuele bezoeken, klik hier: <a href="special.htm">individueel bezoek </a></b><br/>
Individueel bezoek: exclusieve gids voor uw groep.<br />
Classic VIP Tour: €255,00 (1 tot 30 personen)<br />
Classic VIP Tour: €510,00 (31 tot 60 personen)<br />
Student Tour: €255,00 (1 tot 30 personen)<br />
Draught Tour: €500,00 (1 tot 30 personen)<br />
Draught Tour: €1000,00 (31 tot 60 personen)<br />
Beer Tour: €500,00 (1 tot 30 personen)<br />
Beer Tour: €1000,00 (31 tot 60 personen)<br />
Cheese a Beer Tour: prijs op aanvraag<br />
<br />
<!--<p><b>Voor speciale arrangementen (exclusieve bezoeken en VIP bezoeken),  klik hier: <a href="special.htm">speciale arrangementen</a></b><br/>
VIP tour: exclusieve gids voor uw groep 180€ (1-30 personen) - 360€ (31-60 personen).</p>-->

%AX_FUNDRAUGHT%
<br/><br/>
Om uw confirmatie email opnieuw te verzenden of om te annuleren, gelieve volgende link te gebruiken: <a href="mybooking.htm">My Booking</a></p>
</div>