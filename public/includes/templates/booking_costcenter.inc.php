<div style='clear:left;height:50px'></div>
<h3>%AX_COSTCENTERDETAILS%</h3>
<div style='clear:left;height:10px'></div>
<input type=hidden name="bookpayment" value="costcenter" />

<span class="abstand3"/>%AX_COSTCENTER%:%AX_VARMANDATORY%</span>
<span class="abstand2"/><input name="book_cost_center" maxlength="50"  style="width: 150px" value="%AX_VARBOOK_COST_CENTER%"/></span>

<div style="clear:left; padding:5px;"><!-- - --></div>

<span class="abstand3"/>Responsible Person:%AX_VARMANDATORY%</span>
<span class="abstand2"/><input name="book_cost_personfirst" maxlength="50"  style="width: 150px" value="%AX_VARBOOK_COST_PERSONFIRST%"/></span>
<input name="book_cost_personlast" maxlength="50"  style="width: 150px" value="%AX_VARBOOK_COST_PERSONLAST%"/></span>

<div style="clear:left; padding:5px;"><!-- - --></div>

<span class="abstand3"/>Name Inbev affiliate:%AX_VARMANDATORY%</span>
<span class="abstand2"/>
<input name="book_cost_affname" maxlength="100"  style="width: 150px" value="%AX_VARBOOK_COST_AFFNAME%"/>
</span>
<div style="clear:left; padding:5px;"><!-- - --></div>
<span class="abstand3"/>%AX_CNAME%:%AX_VARMANDATORY%</span>
<span class="abstand2"/>
<input name="book_cost_name" maxlength="50" style="width: 150px" value="%AX_VARBOOK_COST_NAME%"/>
</span>
<div style="clear:left; padding:5px;"><!-- - --></div>
<span class="abstand3"/>%AX_CSTREET%, %AX_CNUM%:%AX_VARMANDATORY%</span>
<span class="abstand2"/>
<input name="book_cost_street" maxlength="50" style="width: 150px" value="%AX_VARBOOK_COST_STREET%"/>
<input name="book_cost_hnum" maxlength="5" style="width: 30px" value="%AX_VARBOOK_COST_HNUM%"/>
</span>
<div style="clear:left; padding:5px;"><!-- - --></div>
<span class="abstand3"/>%AX_CZIP%, %AX_CCITY%:%AX_VARMANDATORY%</span>
<span class="abstand2"/>
<input name="book_cost_zip" maxlength="8" style="width: 60px" value="%AX_VARBOOK_COST_ZIP%"/>
<input name="book_cost_city" maxlength="50" style="width: 120px" value="%AX_VARBOOK_COST_CITY%"/>
</span>
<div style="clear:left; padding:5px;"><!-- - --></div>
<span class="abstand3"/>%AX_COUNTRY%:%AX_VARMANDATORY%</span>
<span class="abstand2"/>%AX_INCCOST_COUNTRY%</span>

<div style="clear:left; padding:5px;"><!-- - --></div>
