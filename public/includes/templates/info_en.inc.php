<div class="fliesstext" style="width: 370px; float: left;">
<h1>Welcome to the %AX_VARMYBREWERY% reservation site!</h1>
<p>You can book your guided tour in the %AX_VARMYBREWERY% brewery until 3 weeks before. Minimum number of tour members is 15 persons. Maximum number of tour members is 60 persons.</p>
<p>If your group consists of less than 15 members, you can take an option up to 3 weeks before or join last-minute an existing group up to 2 days before the guided tour.</p>
<p><b>To book, please click here: <a href="showmonth.htm?novip">Book</a></b></p>
%AX_FUNHOESUNDAY%
<br/>
<p><b>To join last-minute, click here: <a href="showmonth.htm?lm=1&amp;novip">last-minute</a></b></p>
<p>Price per visitor:<br/>
Age 0-11: free<br/>
Age +12: 8,50&euro;<br/>
%AX_FUNDISCOUNT%
<br/><i>Note: for visitors under legal drinking age, soft drinks are available.</i></p>
<!--<p>Exclusive and VIP visits:<br/>-->
<br/>
<p><b>For individual visits, click here: <a href="special.htm">individual visit</a></b><br/>
Individual visit: exclusive tour guide for your group.<br />
Classic VIP Tour: €255,00 (1 to 30 participants)<br />
Classic VIP Tour: €510,00 (31 to 60 participants)<br />
Student Tour: €255,00 (1 to 30 participants)<br />
Draught Tour: €500,00 (1 to 30 participants)<br />
Draught Tour: €1000,00 (31 to 60 participants)<br />
Beer Tour: €500,00 (1 to 30 participants)<br />
Beer Tour: €1000,00 (31 to 60 participants)<br />
Cheese a Beer Tour: price on demand<br />
<br />
%AX_FUNDRAUGHT%
<br/><br/>
To resend your confirmation email or cancel, please use the following link: <a href="mybooking.htm">My Booking</a></p>
</div>