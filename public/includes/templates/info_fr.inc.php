
<div class="fliesstext" style="width: 370px; float: left;">
<h1>Bienvenue sur le site de réservation de la brasserie %AX_VARMYBREWERY%!</h1>
<p>Pour visiter la brasserie %AX_VARMYBREWERY%, vous devez réserver votre guide minimum 3 semaines à l’avance. Le nombre minimum de visiteurs par groupe est de 15 personnes, avec un maximum de 60 personnes.</p>
<p>Si votre groupe compte moins de 15 personnes, vous pouvez prendre une option jusqu’à 3 semaines avant la date de votre visite ou vous pouvez vous joindre en dernière-minute à une visite déjà confirmée en réservant minimum 2 jours à l’avance. </p>
<p><b>Pour réserver, cliquez: <a href="showmonth.htm?novip">ici</a></b></p>
%AX_FUNHOESUNDAY%
<br/>
<p><span class="abstand">Pour les dernières-minutes, cliquez: </span><b><a href="showmonth.htm?lm=1&amp;novip">ici</a></b></p>
<p>Prix par visiteur:<br/>
0-11 ans: gratuit<br/>
+12 ans: 8,50&euro;<br/>
%AX_FUNDISCOUNT%
<br/>
<i>Note: L'âge minimum requis pour la dégustation après la visite est de 18 ans. Des softs sont également disponibles.</i></p>
<!--<p><b>Visite exclusive et VIP:</b></p>-->
<p><span class="abstand_1">Pour des visites individuelles, cliquez ici: </span><a href="special.htm">visite individuelle</a><br/>
Visite individuelle : guide exclusive pour votre groupe.<br />
Classic VIP Tour: €255,00 (de 1 à 30 participants)<br />
Classic VIP Tour: €510,00 (de 31 à 60 participants)<br />
Student Tour: €255,00 (de 1 à 30 participants)<br />
Draught Tour: €500,00 (de 1 à 30 participants)<br />
Draught Tour: €1000,00 (de 31 à 60 participants)<br />
Beer Tour: €500,00 (de 1 à 30 participants)<br />
Beer Tour: €1000,00 (de 31 à 60 participants)<br />
Cheese a Beer Tour: prix sur demande<br />
<br />
<!--<p><b>Pour les arrangements spéciaux (visite exclusive et VIP), cliquez: <a href="special.htm">ici</a></b><br/>
VIP tour: Tour exclusive pour votre group  180€ (1-30 personnes) - 360€ (31-60 personnes).</p>-->
%AX_FUNDRAUGHT%
<br/><br/>
Pour renvoyer votre confirmation par email ou annuler, utilisez le lien suivant: <a href="mybooking.htm">My Booking</a></p>


</div>