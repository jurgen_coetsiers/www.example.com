<?

//$d=substr($mydate,6,2);
//$m=substr($mydate,4,2);
//$y=substr($mydate,0,4);

//if(!checkdate($m,$d,$y))
//{
//	echo(gw(bad_date));
//	finito();
//}
if($_SESSION['SS_LASTMINUTE']=="1")   // CHECK SESSION MODE EVEN THOUGH CHECK BELOW SHOULD CATCH IT TOO
	$lastminute=1;

$grouperror=0;
$dateerror=0;
$infoecho=0;			// show debug info icons with message as alt test if set to  1 and  shows tables if set to 5




if(!isset($mydate))
	$mydate=$_SESSION['SSMYDATE'];


if(isset($mydate))
{
	list($y, $m, $d)=explode_date($mydate);


	if(!checkdate($m,$d,$y))
	{
		$dateerror=(gw(warn).gw(wrongdate));
	}
	else
	{
		//-----CHECK IF DAY PAST OR FROZEN
		$nowtime=mktime();

		// NEED TO TAKE SECONDS OF DAY SO FAR AWAY.
		$nowhours=date("G");
		$nowmins=date("i")+1;

		$nowseconds=$nowhours*3600;
		$nowseconds=$nowseconds+($nowmins*60);


		$opentime=$nowtime+$FREEZE_SECONDS-$nowseconds;  // ADD FREEZE TIME

		$thisdaytime=mktime(0,0,0,$m,$d,$y);


		if($thisdaytime < $opentime) // TEST IF WE ARE IN THE LAST MINUTE PERIOD
		{
			if($thisdaytime >$nowtime)  // SO LM IS NOT IN PAGE TITLE
			{
				$t_extra=gw(lastminute,space);
				$lastminute=1;
			}
		}
		else
		{
			$t_extra="";
			$lastminute=0;
		}

	}
}

echo "<H1>".$myBrewery.getword(reservation,space).$t_extra."</h1>";


//------------------------------------------------------------------
//-----SET MYDATE-----
//------------------------------------------------------------------
if((isset($mydate))&&(strlen($mydate)==8))
{
	$_SESSION['SSMYDATE']=$mydate;
}
else
{
	if(strlen($_SESSION['SSMYDATE'])<>8)
	{
		$dateerror=gw(warn).gw(nodate);
	}
	else
		$mydate=$_SESSION['SSMYDATE'];

}

//------------------------------------------------------------------
//-----TEST IF WE ARE IN THE PAST
//------------------------------------------------------------------
if(($thisdaytime< $nowtime)&&(!$dateerror))
{
	echo(gw(warn).gw(wrongdate));
	echo(cal_link(0,0));
	$dateerror=1;

}

//------------------------------------------------------------------
//-----RESET GROUP COUNT SO IT CAN BE CHANGED-----
//------------------------------------------------------------------
if(isset($gc))
{
	$_SESSION['SSGROUP_COUNT']="";
	unset($_SESSION['SSGROUP_COUNT']);
}


//------------------------------------------------------------------
//-----SET GROUP COUNT-----
//------------------------------------------------------------------
if(isset($groupcount))
{

	//-----------------------------------------------------------------------------------------
	// LAST MINUTES GROUPSIZE MUST BE LESS THAN 16 PEOPLE OTHERWISE IT CANNOT BE A LAST MINUTE
	//-----------------------------------------------------------------------------------------
	if(($lastminute)&&($groupcount>15))
	{
		$FEEDBACK=getword(warn).getword(lastminute_group_too_big);
		$grouperror=1;
	}

	//-----------------------------------------------------------------------------------------
	// CHECK MIN AND MAX GROUP SIZE
	//-----------------------------------------------------------------------------------------
	if(($groupcount>0)&&($groupcount<61))
	{
		$_SESSION['SSGROUP_COUNT']=$groupcount;


		//-----------------------------------------------------------------------------------------
		//-----IF SPECIAL ARRANGEMENT ROUND THE GROUP SIZE UP SO WE ONLY SEE WHOLE EMPTY SLOTS-----
		//-----------------------------------------------------------------------------------------
		if(special())
		{
			$_SESSION['SSGROUP_SPECIAL_COUNT']=$groupcount;  // BUT STORE THE REAL GROUP SIZE

			if($groupcount>30)
				$_SESSION['SSGROUP_COUNT']=60;
			else
				$_SESSION['SSGROUP_COUNT']=30;
		}
		else
			$_SESSION['SSGROUP_SPECIAL_COUNT']=0;
	}
	else  // GROUPSIZE IS NOT OK -----
	{
		$grouperror=1;

		if($groupcount>61)
			$FEEDBACK=getword(warn).getword(group_too_big);
		else
			$FEEDBACK=getword(warn).getword(group_not_defined);

	}
}

if($_SESSION['SSGROUP_COUNT']=="")
	$grouperror=1;

$size_of_group=$_SESSION['SSGROUP_COUNT'];



//-------------------------------------------------------------
//-----ASK FOR GROUP COUNT IF WE DO NOT HAVE IT OR IT WAS WRONG
//-------------------------------------------------------------
if (!isset($_SESSION['SSGROUP_COUNT'])||($grouperror)||($dateerror))
{
	//$_SESSION['SSMYDATE']="";
	if($grouperror==1)
		$t=getTemplate(how_many_people,1,1);  // ONLY IF ERROR HAS TO DO WITH GROUPCOUNT AND NOT DATE
	if($dateerror)
	{
		echo $dateerror;
		echo (cal_link(0,0));
	}
}
else
{
	//---------------------
	//----NOW SHOW THE DAY
	//---------------------

	list($y, $m, $d) =explode_date($mydate);




	//echo "lm=$lm &s =".special();  IT WOULD APPEAR THAT SPECIAL AND LM ARE ALREADY MUTUALLY EXCLUSIVE

	if($lm && special())
	{
		echo(gw(warn).gw(nospecials));
		finito();
	}
	//---------------------------------------------------
	//-----SHOW BOOKING INFO-----SAVE DATE FOR LATER
	//---------------------------------------------------
	//$_SESSION['SS_MYDATE']=$mydate;  // REPEAT LINE SEE ABOVE???



	//---------------------------------------------------
	// CHECK IF SPECIAL ARRANGEMENT
	//---------------------------------------------------
	$spec=special();
	$spec_slot="";

	if($spec)
	{
		echo "<b>$spec Special </b><BR>";

		if($_SESSION['SSGROUP_COUNT']==30)
			$spec_slot="(require 1 slot)";
		else
			$spec_slot="(require 2 slots)";

		$lookingfor=$_SESSION['SSGROUP_SPECIAL_COUNT'];
	}
	else
		$lookingfor=$_SESSION['SSGROUP_COUNT'];


	if($lookingfor==1)
		echo gw(freeslots).gw(onthe)." $d.$m.$y".gw(forr)." ".$lookingfor." ".gw(person).".&nbsp;	<BR><a href=\"$PHP_SELF?gc=0\">".getword(change)." ".gw(groupsize)."</a>";
	else
		echo gw(freeslots).gw(onthe)." $d.$m.$y".gw(forr)." ".$lookingfor." ".gw(people).".&nbsp;	<BR><a href=\"$PHP_SELF?gc=0\">".getword(change)." ".gw(groupsize)."</a>";

	echo(cal_link()."<BR><BR>");

	$daytable=getTemplate(table_day);
	$dayrow_clean=getTemplate(table_day_row);

	$slots="";
	$bookings_today=0;
	$maxslots=count($timeslots);

	$possible=array();
	$possible=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$possibleA=array();
	$possibleA=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$possibleB=array();
	$possibleB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE


	$possibleAB=array();
	$possibleAB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$langAB=array();
	$langAB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$infoAB=array();
	$infoAB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$bookingsA_today=0;
	$bookingsB_today=0;

	$SLOTA_TABLE="";
	$SLOTB_TABLE="";


	$Alabel="Slot A";
	$Blabel="Slot B";

	$statusA="-";
	$statusB="-";

	$NL=0;
	$FR=0;
	$EN=0;



	//------------------------------------------------------------------
	// END OF PART 1
	//------------------------------------------------------------------
	// NOW GET INFO ABOUT A & B SLOTS
	//------------------------------------------------------------------

	$slotcount=1;



	foreach ($timeslots as $k=>$timeslot)
	{

		//-----MAKE SLOT CODES A & B
		$slotcode=$myBrewery[0].$mydate."t".$slotcount;
		$slotcodeA=$slotcode."A";
		$slotcodeB=$slotcode."B";

		//echo "k=$k(k)<BR>";

		//---------------------------------------
		//-----AND GET INFO ABOUT THAT A SLOT ---
		//---------------------------------------

		$slots[$slotcodeA]=getonerow ("select * from slots where SLOT_CODE='$slotcodeA'",0);

		//-----------------------------------------------------------------------------------
		//-----IF SLOT HAS DETAILS THEN OPEN NEIGHBOUR SLOTS UNLESS WE ARE IN LASTMINUTE MODE
		//-----------------------------------------------------------------------------------
		if(!empty($slots[$slotcodeA]))
		{




			if(!$lastminute)   // ONLY OPEN SLOTS WHERE SPACES ARE AVAILABLE AND CONFIRMED
			{
				$possibleA[$slotcount]=1;

				$possibleA[$slotcount-1]=1;  //ACTIVATE NEIGHBOURS IF RESERVATION EXISTS, EVEN IF IT IS STILL OPTIONS, i.e. LESS THAN 15 PEOPLE
				$possibleA[$slotcount+1]=1;

				$bookingsA_today++;
				$slots[$slotcodeA][PEOPLE_SOFAR]=slot_count($slotcodeA,0);//HOW MANY PEOPLE ??
			}
			else
			{

				$current_slot_count=slot_count($slotcodeA);

				//if(($current_slot_count >=15)||(SlotHasBigGroup($slotcodeA)))
				if(($current_slot_count >=5)||(SlotHasBigGroup($slotcodeA)))
				{

				$new_size_of_group=$size_of_group+$current_slot_count;

				// THIS WAS THE OLDER VERSION I'M KEEPING THIS FOR HISTORICAL REASONS
				//if(slot_count($slotcodeA)<=$MINCONF)  // TEST IF (CURRENT + NEW) COUNT FITS INTO SLOT

					if($new_size_of_group<=30)  // TEST IF (CURRENT + NEW) COUNT FITS INTO SLOT
					{
						$possibleA[$slotcount]=1;
						$bookingsA_today++;
						$slots[$slotcodeA][PEOPLE_SOFAR]=slot_count($slotcodeA,0);   //HOW MANY PEOPLE ??
					}
				}
			}
		}

		//---------------------------------------
		//-----AND GET INFO ABOUT THAT B SLOT ---
		//---------------------------------------

		$slots[$slotcodeB]=getonerow ("select * from slots where SLOT_CODE='$slotcodeB'",0);

		//-----------------------------------------------------------------------------------
		//-----IF SLOT HAS DETAILS THEN OPEN NEIGHBOUR SLOTS UNLESS WE ARE IN LASTMINUTE MODE
		//-----------------------------------------------------------------------------------

		if(!empty($slots[$slotcodeB]))
		{
			if(!$lastminute)   // ONLY OPEN SLOTS WHERE SPACES ARE AVAILABLE AND CONFIRMED
			{

			$possibleB[$slotcount]=1;

			$possibleB[$slotcount-1]=1;  			//ACTIVATE NEIGHBOURS IF RESERVATION EXISTS, EVEN IF IT IS STILL OPTIONS, i.e. LESS THAN 15 PEOPLE
			$possibleB[$slotcount+1]=1;
			$bookingsB_today++;
			$slots[$slotcodeB][PEOPLE_SOFAR]=slot_count($slotcodeB,0);//HOW MANY PEOPLE ??
			}
			else
			{

				$current_slot_count=slot_count($slotcodeB);
				//if($current_slot_count >=15)

				if($current_slot_count >=5)
				{

					$new_size_of_group=$size_of_group+$current_slot_count;

				// THIS WAS THE OLDER VERSION I'M KEEPING THIS FOR HISTORICAL REASONS
				// if(slot_count($slotcodeB)<=$MINCONF)

					if($new_size_of_group<=30)  // TEST IF (CURRENT + NEW) COUNT FITS INTO SLOT
					{
						$possibleB[$slotcount]=1;
						$bookingsB_today++;
						$slots[$slotcodeB][PEOPLE_SOFAR]=slot_count($slotcodeB,0);   //HOW MANY PEOPLE ??
					}
				}
			}
		}

		$slotcount++;
	}

	$infoA="Slot A has $bookingsA_today of $maxslots bookings - ";
	$infoB="Slot B has $bookingsB_today of $maxslots bookings - ";

	//-------------------------------------
	//-----IF WE HAVE NO BOOKINGS THEN ALL SLOT ARE POSSIBLE-----
	//-------------------------------------
	if($bookingsA_today==0)
	{

		//-------------------------------------
		// UNLESS OF COURSE WE ARE NOT OPEN ON THIS DAY  I ASSUME IF WE CAN'T OPEN A THEN B IS SAFE
		//-------------------------------------

		$dow=date("w",mktime(0,0,0,$m,$d,$y));

		if($dow==0)  // SUN IN DATE(W) HAS 0 AND I HAVE MON AS 0 SO THIS IS THE TRANSLATION - MIGHT BE ABLE TO ELEGANTLY MOD THIS SOMEHOW
			$dow=6;
		else
			$dow--;

		if(!$mydays[$dow])
			$possibleA=initarray($maxslots,0);
		else
		{
			if(!$lastminute)    // OPEN ALL BECAUSE NONE SET UNLESS WE ARE LAST MINUTE
			{
				unset($possibleA);
				$possibleA=initarray($maxslots,1);
			}

		}
	}


	if($bookingsB_today==0)
	{
		if(!$lastminute)  // OPEN ALL BECAUSE NONE SET UNLESS WE ARE LAST MINUTE
		{
			unset($possibleB);
			$possibleB=initarray($maxslots,1);
		}
	}




	// FOR MORE THAN 30 PEOPLE, B MUST BE EMTPY AND A HAVE AT LEAST AS MANY SPACES AS THE GROUP > 30 IS


	//----------------------------------------------------------
	// ----- THIS SECTION DEALS WITH BIG GROUPS-----------------
	//----------------------------------------------------------


	if($_SESSION['SSGROUP_COUNT']>30)
	{
		echo "Checking for slots available in A & B.<BR>";

		$remainder=$_SESSION['SSGROUP_COUNT']-30;

		$slotcount=1;
		foreach ($timeslots as $k=>$timeslot)
		{

			//-----MAKE SLOT CODES A & B
			$slotcode=$myBrewery[0].$mydate."t".$slotcount;
			$slotcodeA=$slotcode."A";
			$slotcodeB=$slotcode."B";

			$acount=$slots[$slotcodeA][PEOPLE_SOFAR];
			$bcount=$slots[$slotcodeB][PEOPLE_SOFAR];

			$booklink="<a class=\"dayopen\" href=\"reservation.htm?slotcodeAB=$slotcodeA";

			$langAB[$slotcount]="";
			$infoAB[$slotcount].="";

			if($bcount!=0)
				$infoAB[$slotcount].="slot B is not available";

			if(($acount+$remainder)>31)
				$infoAB[$slotcount].=" people in A $acount + (group-30) $remainder, is too big";


			if(($bcount==0)&&(($acount+$remainder)<31))
			{
				$ABcount=$acount+$remainder;

				if($acount==0)
				{
					$infoAB[$slotcount]="t$slotcount available, A & B are empty. Lang=ANY";
					//$langAB[$slotcount]="ANY";


					if($possibleA[$slotcount]==1)
					{
						$langAB[$slotcount]=$booklink."&setlang=nl\">".$nl_flag."</a>&nbsp;";
						$langAB[$slotcount].=$booklink."&setlang=fr\">".$fr_flag."</a>&nbsp;";
						$langAB[$slotcount].=$booklink."&setlang=en\">".$en_flag."</a>&nbsp;";
					}
					else
						$langAB[$slotcount]="";


				}
				else
				{
					$langAB[$slotcount]=$booklink."&setlang=".$slots[$slotcodeA][SLOT_LANG]."\">".getword($slots[$slotcodeA][SLOT_LANG])."</a>&nbsp;";
					$infoAB[$slotcount]="t$slotcount possible Slot B is empty and  A=$acount + Rest of group=$remainder ($ABcount) is smaller than or equal to 30. But Lang is restricted.";
				}




				// NOW TEST IF IT IS A SLOT IS POSSIBLE TO ENSURE SEQUENTIAL BOOKINGS



				if(($possibleA[$slotcount]==1)||($bookingsA_today==0))
				{
					$possibleAB[$slotcount]=1;
				}
				else
				{
					$possibleAB[$slotcount]=A;
					$infoAB[$slotcount].=" But this A slot is not possible.";
					$langAB[$slotcount].="";
				}

			}
			else
			{
				//echo "t$slotcount NOT possible.<BR>";
				$possibleAB[$slotcount]=0;
			}
			$slotcount++;
		}


	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	//foreach($possibleAB as $k=>$v)
	{
		//echo "t$slotcount possible=".$possibleAB[$slotcount]."  Lang=".$langAB[$slotcount];
		$ainfo=info($infoAB[$slotcount],0);

		if($possibleAB[$slotcount]==1)
			$slot_status="slotopen";
		else
			$slot_status="slotclosed";


		$dayrow=str_replace("AX_DAYCLASS","class=\"$slot_status\"",$dayrow_clean);
		$dayrow=str_replace("<AX_SLOTLINK>",$langAB[$slotcount] ,$dayrow);

		$dayrow=str_replace("<AX_TIMESLOT_START>",$timeslot[0],$dayrow);
		$dayrow=str_replace("<AX_TIMESLOT_STOP>",$timeslot[1],$dayrow);
		$dayrow=str_replace("<AX_DEBUG>",$ainfo ,$dayrow);
		$dayrows.=$dayrow;

		$slotcount++;
	}


	$SLOTAB_TABLE=str_replace("<AX_TIMESLOTS>",$dayrows,$daytable);


}
else
{

	//-------------------------------------
	//-----NOW WORK OUT IF WE CAN OPEN SLOT B
	//-------------------------------------
	//1. ARE ALL OF SLOTS A FULL OR IS IT LAST MINUTE
	//-------------------------------------



	if(($bookingsA_today==$maxslots)||($lastminute))
	{
		$statusA="";
		$statusA.="Status: A has bookings in all slots";
		$statusA.="Checking to see if the lang is available for the group size.";


		// GET LANGS HERE BUT I THINK WE CAN MOVE THIS UP LATER.
		$slotcount=1;

		foreach ($timeslots as $k=>$timeslot)
		{
				$slotcode=$myBrewery[0].$mydate."t".$slotcount;
				$slotcodeA=$slotcode."A";

				//echo ("TOTAL=".$slots[$slotcodeA][PEOPLE_SOFAR]."+".$_SESSION['SSGROUP_COUNT']."<BR>");

				$new_group_total=$slots[$slotcodeA][PEOPLE_SOFAR]+$_SESSION['SSGROUP_COUNT'];

				//---ONLY IF LANG IS NOT POSSIBLE IN SLOT A

				if($new_group_total<=30)
					$$slots[$slotcodeA][SLOT_LANG]++;  // BEWARE, IT CREATES THE VARS EN,FR,NL
				//else
				//	echo "<BR><B>t".$slotcount." $slotcodeA people in group (".$slots[$slotcodeA][PEOPLE_SOFAR].") + groupcount (".$_SESSION['SSGROUP_COUNT'].")= ($new_group_total) too big.</b>";

				//echo $slots[$slotcodeA][SLOT_LANG];


			$slotcount++;
		}

		//echo "nl=$NL<BR>";
		//echo "fr=$FR<BR>";
		//echo "en=$EN<BR>";

		$Bsearch="";
		if($NL==0)
			$Bsearch.="Checking Slot B for Dutch spaces.<BR><BR>";
		if($FR==0)
			$Bsearch.="Checking Slot B for French spaces.<BR><BR>";
		if($EN==0)
			$Bsearch.="Checking Slot B for English spaces.<BR><BR>";


		//----------------------------------------------------
		//LET'S JUST SHOW SLOT B
		//----------------------------------------------------
		$slotcount=1;

		foreach ($timeslots as $k=>$timeslot)
		{
			$slotcode=$myBrewery[0].$mydate."t".$slotcount;
			$slotcodeB=$slotcode."B";
		}



		//----------------------------------------------------
	}
	else
	{
		$infoA.="Status: A Still has spaces.";
		$EN=A;
		$NL=A;
		$FR=A;
	}



	//-----CHECK SLOT A COLOURS-----
	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	{
		$slotcode=$myBrewery[0].$mydate."t".$slotcount;
		$slotcodeA=$slotcode."A";
		$slotcodeB=$slotcode."B";

		$spaces_left=0;
		$peoplecountA=0;
		$peoplecountB=0;

		//get slot info  //CHECK FOR SPACES AVAILABLE

		$lang="";
		//echo "$slotcount => $possible[$slotcount]<BR>";
		//	$peoplecount=slot_count($slotcode);//HOW MANY PEOPLE ??

		$peoplecountA=slot_count($slotcodeA);//HOW MANY PEOPLE ??
		$peoplecountB=slot_count($slotcodeB);//HOW MANY PEOPLE ??

		$new_group_total=$slots[$slotcodeA][PEOPLE_SOFAR]+$_SESSION['SSGROUP_COUNT'];



		if(!$possibleA[$slotcount])
		{
			$slot_status="slotclosed";
			$lang="";
			$message="Slot is not yet open.";
		}
		elseif($new_group_total>30)
		{
			$slot_status="slotfull";
			$message="+".$_SESSION['SSGROUP_COUNT']."=$new_group_total Not possible, not enough room in slot. (NOT ENOUGH SPACES)";
		}
		elseif($peoplecountA==30)
		{
			$slot_status="slotfull";
			$message="Slot is fully booked (SLOT FULL)";
		}
		else
		{
			if(($new_group_total<15)&&(!SlotHasBigGroup($slotcodeA)))
			{
				$slot_status="slotoption";
				$optional=" * ";

				// added because we changes the min num for last minute to less than 15
				if($lastminute)
				{
					$slot_status="slotlastminute";
					$optional="";
				}

			}
			else
			{
				if($lastminute)
					$slot_status="slotlastminute";
				else
					$slot_status="slotopen";

				$optional="";
			}

			$booklink="<a class=\"dayopen\" href=\"reservation.htm?slotcode=$slotcodeA";

			if(!empty($slots[$slotcodeA]))  // IF OPEN GET LANGUAGE IF DEFINED
			{
				$lang=$slots[$slotcodeA]["SLOT_LANG"];
				$fvar=strtolower($lang)."_flag";
				$lang=$optional.$booklink."\">".$$fvar."</a>";  // MAKE VARIABLE NAME WHICH CONTAINS THE FLAG

				$spaces_left=$maxgroupsize-$peoplecountA;
				$message="Slot is open and has a booking, $spaces_left spaces ($maxgroupsize-$peoplecountA) are available in ".$slots[$slotcodeA]["SLOT_LANG"].".";

			}
			else
			{
				$message="Slot is open. 30 spaces are available in all languages";
				//$lang=$allflags;
				$lang="$optional";
				$lang.=$booklink."&setlang=nl\">".$nl_flag."</a>&nbsp;";
				$lang.=$booklink."&setlang=fr\">".$fr_flag."</a>&nbsp;";
				$lang.=$booklink."&setlang=en\">".$en_flag."</a>&nbsp;";


				//$lang=$allflags;

			}
		}
		$dayrow=str_replace("AX_DAYCLASS","class=\"$slot_status\"",$dayrow_clean);
		$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);

		//if($slot_status=="slotopen")
		//{
		//	//$booklink=$booklink."\">BOOK</a>";
		//	//$dayrow=str_replace("<AX_SLOTLINK>",$booklink." | ".$slotcode." | ".$lang ,$dayrow);
		//	$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);
		//}
		//else
		//	$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);

		$gcountA=groups_in_slot($slotcodeA);

		$dayrow=str_replace("<AX_TIMESLOT_START>",$timeslot[0],$dayrow);
		$dayrow=str_replace("<AX_TIMESLOT_STOP>",$timeslot[1],$dayrow);
		$dayrow=str_replace("<AX_DEBUG>",info($slotcodeA."->".$peoplecountA." people in slot, $gcountA groups in slot, ".$message) ,$dayrow);
		$dayrows.=$dayrow;
		$slotcount++;
	}

	//echo "</td><td>";

	$SLOTA_TABLE=str_replace("<AX_TIMESLOTS>",$dayrows,$daytable);




//-------------------------------------
//----SLOT B
//-------------------------------------

//echo "nl=$NL<BR>";
//echo "fr=$FR<BR>";
//echo "en=$EN<BR>";


if(($NL=="0")||($FR=="0")||($EN=="0"))
{

	$dayrow_clean=getTemplate(table_day_rowB);
	$dayrow="";
	$dayrows="";
	$statusB="";
	$statusB.="Status: B, Slot B can be opened.";
	$statusB.=$Bsearch;

	//-----CHECK SLOT B COLOURS-----
	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	{
		$slotcode=$myBrewery[0].$mydate."t".$slotcount;
		$slotcodeA=$slotcode."A";
		$slotcodeB=$slotcode."B";

		$spaces_left=0;
		$peoplecountA=0;
		$peoplecountB=0;

		//get slot info  //CHECK FOR SPACES AVAILABLE

		$lang="";
		//echo "$slotcount => $possible[$slotcount]<BR>";
		//	$peoplecount=slot_count($slotcode);//HOW MANY PEOPLE ??

		$peoplecountA=slot_count($slotcodeA);//HOW MANY PEOPLE ??
		$peoplecountB=slot_count($slotcodeB);//HOW MANY PEOPLE ??

		$new_group_total=$slots[$slotcodeB][PEOPLE_SOFAR]+$_SESSION['SSGROUP_COUNT'];



		if(!$possibleB[$slotcount])
		{
			$slot_status="slotclosed";
			$lang="";
			$message="Slot is not yet open.";
		}
		elseif($new_group_total>30)
		{
			$slot_status="slotfull";
			$message="+".$_SESSION['SSGROUP_COUNT']."=$new_group_total Not possible, not enough room in slot.";
		}
		elseif($peoplecountB==30)
		{
			$slot_status="slotfull";
			$message="Slot is fully booked";
		}
		else
		{
			if($new_group_total<15)
			{
				$slot_status="slotoption";
				$optional="*";


				// added because we changes the min num for last minute to less than 15
				if($lastminute)
				{
					$slot_status="slotlastminute";
					$optional="";
				}


			}
			else
			{
				if($lastminute)
					$slot_status="slotlastminute";
				else
					$slot_status="slotopen";

				$optional="";
			}

			$booklink="<a class=\"dayopen\" href=\"reservation.htm?slotcode=$slotcodeB";

			if(!empty($slots[$slotcodeB]))  // IF OPEN GET LANGUAGE IF DEFINED
			{
				$lang=$slots[$slotcodeB]["SLOT_LANG"];

				//$lang=$booklink."\"><img src=\"/images/".strtolower($lang).".gif\" alt=\"".getword($lang)."\" border=0>";
				$lang=$optional.$booklink."\">".getword($lang)."</a>";

				$spaces_left=$maxgroupsize-$peoplecountB;
				$message="Slot is open and has a booking, $spaces_left spaces ($maxgroupsize-$peoplecountA) are available in ".$slots[$slotcodeB]["SLOT_LANG"].".";

			}
			else
			{
				$message="Slot is open. 30 spaces are available in all languages";
				//$lang=$allflags;

				$lang="$optional";


				if($NL==0)
					$lang.=$booklink."&setlang=nl\">".$nl_flag."</a>&nbsp;";

				if($FR==0)
					$lang.=$booklink."&setlang=fr\">".$fr_flag."</a>&nbsp;";

				if($EN==0)
					$lang.=$booklink."&setlang=en\">".$en_flag."</a>&nbsp;";


				//$lang=$allflags;

			}
		}

		$dayrow=str_replace("AX_DAYCLASS","class=\"$slot_status\"",$dayrow_clean);

		if($slot_status=="slotopen")
		{
			$booklink=$booklink."\">BOOK</a>";
			//$dayrow=str_replace("<AX_SLOTLINK>",$booklink." | ".$slotcode." | ".$lang ,$dayrow);
			$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);
		}
		else
			$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);


		$gcountB=groups_in_slot($slotcodeB);

		$dayrow=str_replace("<AX_TIMESLOT_START>",$timeslot[0],$dayrow);
		$dayrow=str_replace("<AX_TIMESLOT_STOP>",$timeslot[1],$dayrow);

		$dayrow=str_replace("<AX_DEBUG>",info($slotcodeB."->".$peoplecountB." people in slot, $gcountB groups in slot, ".$message) ,$dayrow);

		$dayrows.=$dayrow;
		$slotcount++;
	}

	$SLOTB_TABLE=str_replace("<AX_TIMESLOTS>",$dayrows,$daytable);
}
else
{
	$infoB.="Slot B cannot be opened yet.";
	$Blabel="";

}  // EIF SLOT B


	echo'<table border="0" cellpadding="0" cellspacing="0" >';

	//echo "<tr><th align=right>$Alabel&nbsp;&nbsp;</td><th align=left>$Blabel</td></tr>";

	echo "<tr><td valign=top>$SLOTA_TABLE &nbsp;</td><td valign=top>$SLOTB_TABLE &nbsp;</td></tr>";
	echo "</table>";

	info($infoA,$infoecho);
	info($infoB,$infoecho);


	echo "<BR>";

	info($statusA,$infoecho);
	info($statusB,$infoecho);

} // EIF GROUP > 30

echo $SLOTAB_TABLE;
echo "<BR>";
echo(getword(optional));


} //EIF GET GROUPCOUNT


?>


