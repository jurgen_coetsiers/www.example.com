
<div class="res">
<?
$d=substr($slotcode,7,2);
$m=substr($slotcode,5,2);
$y=substr($slotcode,1,4);



//if(!checkdate($m,$d,$y))
//{
//	echo(gw(bad_date));
//	finito();
//}


$realbookcount=$bookcount;		 	// NEED THIS FOR PERSON COUNT CHECK AGE TO GROUPSIZE FOR LARGE GROUPS

if(special())
	$realbookcount=$_SESSION['SSGROUP_SPECIAL_COUNT'];
else
	$realbookcount=$bookcount;		 	// NEED THIS FOR PERSON COUNT CHECK AGE TO GROUPSIZE FOR LARGE GROUPS

$mydate=$_SESSION['SSMYDATE'];  	// NEED THIS FOR THE CANCEL BUTTON


//--------------------------------------------------------------------------------------------------------
//  THIS CODE DEALS WITH BIG GROUPS AND SMALL GROUPS  (MIGHT BE GOOD TO SEPERATE THESE LATER)
//--------------------------------------------------------------------------------------------------------


if(isset($slotcodeAB))   // SLOTCODEAB COMES FROM SHOWDAY
{
	$slotcode=substr(trim($slotcodeAB),0,strlen($slotcode)-1);

	$slotcodeA=$slotcode."A";

	$slotcodeB=$slotcode."B";


	if($reservation==1)
		$slotcode=$slotcodeA;
	else
		$slotcode=$slotcodeAB;


	$origcount=$bookcount;
	$bookcount=$bookcount-30;
	$biggroup=1;
}
else
{
	$slotcodeAB=0;
	$biggroup=0;

}

//echo "res=$reservation";

if($reservation==1)
{

	$emessage="";  // INITIALISE ERROR MESSAGE

	if($bookfname=="dc")
	{
		$bookfname="Darren";
		$booksname="Cooper";
		$bookstreet="Kaiser Friedrich Promenade";
		$bookhnum="89";
		$bookzip="61348";
		$bookcity="Bad Homburg";
		$bookemail="dc7590@gmail.com";
		$bookemail2="dc7590@gmail.com";
		$bookmobile="0171 12345";
		$bookgname="Test Group";
		//$booksenior=$realbookcount;

		$book_inv_name="inv_name";
		$book_inv_street="inv_str";
		$book_inv_hnum="inv_num";
		$book_inv_zip="inv_zip";
		$book_inv_city="inv_city";
		$book_inv_vat="inv_vat";



	}


	if($bookfname=="")$emessage.=gw(fname).", ";
	if($booksname=="")$emessage.=gw(sname).", ";
	if($bookstreet=="")$emessage.=gw(street).", ";
	if($bookhnum=="")$emessage.=gw(num).", ";
	if($bookzip=="")$emessage.=gw(zip).", ";
	if($bookcity=="")$emessage.=gw(city).", ";
	if($bookemail=="")$emessage.=gw(email).", ";
	if($bookmobile=="")$emessage.=gw(mobile).", ";
	if($bookgname=="")$emessage.=gw(gname).", ";
	//if($bookdept=="")$emessage.=gw(dept).", ";


	if(!isset($bookstudents))  // FOR BREWERIES OTHER THAN STELLA
		$bookstudents=0;

	$total=$bookjunior+$booksenior+$bookstudents;
	//echo "$bookjunior bs=$booksenior $bookstudents";

	if($emessage)
	{
		$emessage[strlen(trim($emessage))-1]=".";  // REMOVE LAST COMMA
		$emessage=gw(warn).gw(fields_required).$emessage;
	}

	if(($bookemail!="")&&(!email_valid($bookemail)))   // SYNTAX CHECK EMAIL
		$emessage.=$br.str_replace("<AX_EMAIL>",$bookemail,gw(email_notok));

	if($realbookcount<>$total)
		$emessage.=$br.gw(warn).gw(nomatch_members)." ($total/$realbookcount)";

	if($total==$bookjunior)
		$emessage.=$br.gw(warn).gw(noadult);

	if($bookemail!=$bookemail2)
		$emessage.=$br.gw(warn).gw(emails_nomatch);

	if($bookpayment=="invoice")
	{

		if(!special())  // CHECK PRICE IF INVOICE SELECTED
		{
			$gprice=$booksenior*6;
			if($slotcode[0]=="S")
			{
				$students=$bookstudents*5;
				$gprice=$gprice+$students;
			}

			if($gprice<25)
				$emessage.="<BR>".gw(warn).$gprice.gw(not25);

		}

		if($book_inv_name=="")$imessage.=gw(gname).", ";
		if($book_inv_street=="")$imessage.=gw(street).", ";
		if($book_inv_hnum=="")$imessage.=gw(num).", ";
		if($book_inv_zip=="")$imessage.=gw(zip).", ";
		if($book_inv_city=="")$imessage.=gw(city).", ";
		if($book_inv_vat=="")$imessage.=gw(vat).", ";

		//echo $imessage;
		if($imessage)
		{
			$imessage="<BR>".gw(warn).gw(invoice_fields)."&nbsp;".$imessage;
			$emessage.=$imessage;
		}

	}

	//$emessage='';  // disable error message

	if(!$emessage)
	{
		echo("<H1>".gw(saving)."</h1>");


		$_SESSION['SSGROUP_SLOTCODE']=$slotcode;
		$tmp=$_POST;  // DO THIS TO PRESERVE POST VAR FOR SLOT B, SEEM TO LOOSE THEM IN ADD_GROUP FUNCTION

		$group_ident=make_gid($slotcode);								// DETERMINE WHICH GROUP AND MAKE GROUP_IDENT ALSO CHECK IF IT ALREADY EXISTS
		$tslot="t".$group_ident[(strpos($group_ident,"t")+1)];

		$starttime=$timeslots[$tslot][0];
		$stoptime=$timeslots[$tslot][1];

		make_slot($slotcode, $booklang, 'guide', 'comment',$starttime,$stoptime,0);			// CHECK IF DATA EXISITS FOR THIS SLOT

		$md5pass=add_group($group_ident,A,$biggroup,$starttime,$stoptime,0);			// CHECK IF SPACE IS STILL AVAILABLE AND INSERT GROUP

		$_SESSION['SSGROUP_GROUP_IDENT']=$group_ident;

		if($origcount>30)  // BOOK B  GROUP IS LARGER THAN 30
		{
			$_POST=$tmp;
			$_POST["slotcode"]=$slotcodeB;

			make_slot($slotcodeB, $booklang, 'guide', 'comment',$starttime,$stoptime,0);		// CHECK IF DATA EXISITS FOR THIS SLOT
			$group_ident=make_gid($slotcodeB);								// DETERMINE WHICH GROUP AND MAKE GROUP_IDENT ALSO CHECK IF IT ALREADY EXISTS
			add_group($group_ident,B,$biggroup,$starttime,$stoptime,0);							// CHECK IF SPACE IS STILL AVAILABLE AND INSERT GROUP
		}

		grouppeople($bookpayment,$md5pass);
	}
	else
	{
		$SLOTCODE=$slotcode;  // PUT THIS IN THE GLOBAL VERSION WHICH IS USED BY THE FUNCTION WHICH DISPLAYS THE BOOKING FORM
		echo $emessage;
		getTemplate(booking_form,1,1);
	}


}
elseif($reservation==2)
{
		echo("<H1>".gw(saving)."</h1>");

		$gid=$_SESSION['SSGROUP_GROUP_IDENT'];

		save_members($gid,0);

		$row=getonerow("select * from groups where GROUP_IDENT='$gid'",0);

		foreach($row as $k=>$v)
		{
			//echo "$k --->$v<BR>";
			$$k=$v;
		}


		//showpost();
		// SHOW CONFIRM MESSAGE AND SEND CONFIRM LINK EMAIL


			echo(str_replace("<AX_EMAIL>",$GROUP_BOOKEMAIL,getword(please_confirm)));
			$mailfile=$mybrewery[0]."_".$mylang."_"."confirm";
	
	
			$mailbody=gettemplate($mailfile);

			// GET THE FIRST LINE OF THE MAILBODY TO USE AS SUBJECT
			$maillines=explode("\n",$mailbody,2);
			$mailsubject=$maillines[0];

			$mailbody=str_replace($mailsubject,"",$mailbody);

			//$mailsubject=getword(link_email_subject);
			//--------------------------------------------------------
			// -- GET VARS READY FOR EMAIL
			//--------------------------------------------------------
			$bookdate=substr($gid,7,2).".".substr($gid,5,2).".".substr($gid,1,4);
			$tslot="t".$GROUP_IDENT[(strpos($GROUP_IDENT,"t")+1)];

			if($GROUP_SPECIAL_TYPE)
				$gsize=$_SESSION['SSGROUP_SPECIAL_COUNT'];
			else
				$gsize=$_SESSION['SSGROUP_COUNT'];

			// IF PAYMENT GOES VIA COST CENTER OR INVOICE, REMOVE "PAY UPON ARRIVAL" LINE FROM EMAIL TEXT

			if(($GROUP_SPECIAL_TYPE=="ITW")||($GROUP_BOOKPAYMENT=="invoice"))  // REMOVE PRICE INFORMATION
			{
				$mailbody=remove_prices($mailbody);
			}


			if($gsize==1)  // REMOVE PLURAL OF PEOPLE IF ONLY ONE PERSON
			{
				$mailbody=remove_plural($mailbody);
			}

			//--------------------------------------------------------
			// -- SWOP VARS IN EMAIL TEMPLATE
			//--------------------------------------------------------
			$mailbody=str_replace("<AX_DATE>",$bookdate,$mailbody);


			$mailbody=str_replace("<AX_TIME_START>",$timeslots[$tslot][0],$mailbody);
			$mailbody=str_replace("<AX_TIME_END>",$timeslots[$tslot][1],$mailbody);
			$mailbody=str_replace("<AX_GROUP_COUNT>",$gsize,$mailbody);

			//echo "SPEC=>".$GROUP_SPECIAL_TYPE;

			if(($GROUP_SPECIAL_TYPE)||($gsize>14))
				$mailbody=str_replace("\n<AX_OPTION>\n","",$mailbody);
			else
				$mailbody=str_replace("<AX_OPTION>",getword(email_optional),$mailbody);
				
			$mailbody=str_replace("<AX_TOTAL_PRICE>",$GROUP_PRICE.".00",$mailbody);


			// ADD CONFIRM LINK

			//$confirm_link="http://$HTTP_HOST/web/$mylang/$mybrewery/mybooking.html?ID=$link";

			$confirm_link="http://$HTTP_HOST/web/$mylang/$mybrewery/mybooking.html?ID=".code_id("$GROUP_ID");
			$confirm_link=shrink_link($confirm_link);


			$mailbody=str_replace("<AX_CONFIRMLINK>",gw(email_link_conf,nl).$confirm_link."\n".gw(email_link_del,nl),$mailbody);

			if(fromxs())
			{

				echo '<BR><BR><table border="1" cellpadding="0" cellspacing="0" bgcolor="#c8c8c8"><TR><TD><B>Visible for XS only:</b></td></tr>';
				echo "<a href=\"$confirm_link\" target=\"_blank\">$confirm_link</a><BR>";
				echo "<TR><TD>gid=$gid<BR>";
				echo 	nl2br($mailbody);
				echo "</TD></TR><TR><TD>";
				echo(day_link());
				echo(cal_link());
				echo "</td></tr></table>";


			}
	          
	        
	        
			mail($GROUP_BOOKEMAIL, utf8_decode('FMS-'.$mailsubject), utf8_decode($mailbody), $email_headers);
			mail("dc7590@gmail.com", utf8_decode('Copy-FMS2-'."RESERVATION_".$mailsubject), utf8_decode($GROUP_BOOKEMAIL."\n".$mailbody), $email_headers);
			mail("Carole.Kindt@demonstr8.com", utf8_decode('Copy-FMS-'."RESERVATION_".$mailsubject), utf8_decode($GROUP_BOOKEMAIL."\n".$mailbody), $email_headers);
			mail("Bram.Willemen@demonstr8.com", utf8_decode('Copy-FMS-'."RESERVATION_".$mailsubject), utf8_decode($GROUP_BOOKEMAIL."\n".$mailbody), $email_headers);
			
			mail("info@breweryvisits.com", utf8_decode('Copy-FMS-'."RESERVATION_".$mailsubject), utf8_decode($GROUP_BOOKEMAIL."\n".$mailbody), $email_headers);
			mail("breweryvisitsbelgium@ab-inbev.com", utf8_decode('Copy-FMS2-'."RESERVATION_".$mailsubject), utf8_decode($GROUP_BOOKEMAIL."\n".$mailbody), $email_headers);
			
		
}
else
{

	if(!isset($slotcode))
		echo $TEXT[error].$TEXT[nodata];
	else
	{
		global $SLOTCODE;

		$SLOTCODE=$slotcode;

		$slotcode_brew=$slotcode[0];

		$slotcode_date=substr($slotcode,1,8);
		$slotcode_slot=substr($slotcode,9,2);
		$slotcode_lang=substr($slotcode,11,2);

		list($y, $m, $d) =explode_date($slotcode_date);

		//title(gw(Reservation));
		echo "$myBrewery ".gw(onthe)."$d.$m.$y.<BR>";
		if($_SESSION['SSGROUP_SPECIAL_COUNT']==0)
			echo (gw(reservation).gw(forr)." <strong>".$_SESSION['SSGROUP_COUNT']."</strong> ".gw(people)."<BR>");
		else
			echo (gw(reservation).gw(forr)." <strong>".$_SESSION['SSGROUP_SPECIAL_COUNT']."</strong> ".gw(people)."<BR>");


		echo (gw(startat).'<strong>'.$timeslots[$slotcode_slot][0]."</strong>,".strtolower(gw(endat)).'<strong>'.$timeslots[$slotcode_slot][1]."</strong>.<BR><BR>");
		$slotlang=slot_lang($slotcode);

		if(!$slotlang==0)
			echo gw(tourlang).gw(slot_lang($slotcode));

		getTemplate(booking_form,1,1);
	}

}
?>

</div>