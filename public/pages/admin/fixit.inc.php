
<BR><font color=red>*</font>1.	- You can not reserve for more than 30 persons in the admin.

<BR><font color=red>*</font>2.	- For admin is must be possible to reserve for more than 60 person in slots C & D

<BR><b>LANG</b> 3.	- The cancellation email is in English only and has as sender demonstr@demonstr8.com

<BR><font color=red>*</font>4.	- Website does not have to mention how many members have been saved

<BR><b>LANG</b>5.	- When you confirm for reservation, you get following text in dutch and french

<BR><font color=red>*</font>6.	- In Jupiler with ITW booking I got (require 1 slot) after selecting date in the form. No info for consumer.

<BR><font color=red>*</font>7.	- In Jupiler the form is not entirely translated in the Dutch version and following error appaers:
<BR><font color=red>*</font>8.	costcenterdetails is NOT_DEFINED

<BR><font color=red>*</font>9.	- Change in each language direct debit with Bancontact

<BR><font color=red>*</font>10.	- Please add next to payment option Invoice:

<BR><font color=red>*</font>�	- ENG/FR/NL: (min. 25�)

<BR><font color=red>*</font> 11.	- In the reservation form, we need to add spaces between the brewery and the hour: f.e. Bellevueop de22.04.2005. need to be Bellevue op de 22.04.2005.

<BR><font color=red>*</font>  12.	- In the confirmation letter a space needs to be added after the start and endhour

<BR><font color=red>*</font>13.	- Invoice details are not always translated.

<BR><font color=red>*</font>14.	NL: Factuurgegevens

<BR><font color=red>*</font>	FR: donn�es de facturation

<BR><font color=red>*</font>15.	- When you choose invoice, you get the confirmation you have to pay x at arrival. Can we find a way to change this because you don't pay on te spot with an invoice

<BR><font color=red>*</font>16.	- In the reservation info page: change visitor's age into visitor's age

<BR>17.<b>DISCUSS</b>	- If you break the flow in the reservation form, the slot is blocked. Can this be if you run through the entire slot reservation flow

<BR><font color=red>*</font>18.	- In the confirmation letter, we always need to put the �-sign after the amount

<BR><font color=red>*</font>19.	- Change the text "leeftijd 12 of meer" into "12 jaar of ouder" in all dutch forms

<BR><font color=red>*</font>20.	- Change special arrangements text into:

<BR><font color=red>*</font>21.	NL: Exclusieve bezoeken en VIP bezoeken

<BR><font color=red>*</font>22.	FR; Visite exclusive et VIP

<BR><font color=red>*</font>23.	ENG: Exclusive and VIP visits

<BR><font color=red>*</font>24.	- In the Stella Artois reservation form: change following text below:

<BR><font color=red>*</font>25.	NL: Om te reserveren email naar customer care: -> Om te reserveren stuur een mail naar volgend adres:

<BR><font color=red>*</font>26.	FR: Pour r�server �nvoyer un e-mail au customer care: -> Pour r�server �nvoyer un e-mail a l'adresse suivante:

<BR><font color=red>*</font>27.	ENG: For reservation email customer care: -> For reservation email:

<BR><font color=red>*</font>28.	Also change 16.00 into 17.00 and 20 into 25 for the persons.

<BR><font color=red>*</font>29.	- Following link is only available in English in the reservation info form:

<BR><font color=red>*</font>30.	To resend your confirmation email or cancel, please use the following link: My Booking

<BR><font color=red>?????</font>31.	- In the confirmation letters, change ITW with Interbrew

<BR>32.	- In special arrangements on the reservation info page:

<BR><font color=red>*</font>33.	Put     NL: 180� (1-30 personen) - 360� (31-60 personen)

<BR><font color=red>*</font>�	FR 180� (1-30 personnes) - 360� (31-60 personnes)

<BR><font color=red>*</font>�	ENG: 180� (1-30 persons) - 360� (31-60 persons)

<BR><font color=red>*</font>34.	- We need a legend with the calendar

<BR><font color=red>*</font>35.	- Green: NL Open FR Ouvert ENG Open

<BR><font color=red>*</font>36.	- Orange: NL/FR/ENG Last-minute

<BR><font color=red>*</font>37.	- Red: NL Closed FR Ferme ENG: Closed

<BR><font color=red>*</font>38.	- Blue: NL Reservaties afgesloten FR Reservation cl�ture ENG reservations finalised

<BR><b>IQ <10, nothing I can do</b> 39.	- On  08.04.2005 om 10:30 in SA I did a booking for 10 persons. But when I changed the freeze period. The slot was not available in last minute

<BR><b>DISCUSS</b>40.	-  ON 19.04.2005 in SA I could join a group in last-minute but also the sequential slot before and after where there were 0 people in

<BR><b>DISCUSS</b>41.	- Sometimes it is possible to book more than 16 persons in last-minute

<BR><font color=red>*</font>42.	-  In the reservation info page change bent u "minder dan 15 personen" into "bent u met minder dan 15 personen"

<BR><font color=red>*</font>43.	- In the dutch version change "group" into "groep"

<BR><font color=red>*</font>44.	- You can no longer choise you own hours in version 5 in the admin slots C & D

<BR><font color=red>?? tested, cannot replicate</font>45.	- Sometimes I get the confirmation of booking e-mail twice. Once in the native language and one in english

<BR><font color=red>?? tested, cannot replicate</font>46.	- It must be impossible to join a VIP or ITW slot whatever the number of persons booked.

<BR><font color=red>?? tested, cannot replicate</font>47.	-  In admin reservations, costcenter in not obligatory in the reservation form

<BR><font color=red>*</font>48.	-          After reserving in admin, you get back to reservation. If you click , you go back to under construction

<BR><font color=red>*</font>49.	-          You can�t see slots C&D reservations in the calendars

<BR><font color=red>?? EXPLAIN</font>50.	-          We need to add remarks from the slots c&d somewhere

<BR><font color=red>*</font>51.	-          Calendar BUG: multiple bookings: alle the slots get the same starting hour???

<BR><font color=red>*</font> 52.	-          For the guide report, some changes:

<BR>�<font color=red>*</font> 	Fields to enter amount:

<BR>�<font color=red>*</font> 	VAT number:

<BR>�<font color=red>*</font> 	# Free coupons + amount

<BR>

<HR>
<font color=red>*</font> = Done

<HR><h1>LANG Related</h1>
<BR>

<BR><b>LANG</b> 3.	- The cancellation email is in English only and has as sender demonstr@demonstr8.com

<BR><font color=red>*</font><b>LANG</b>5.	- When you confirm for reservation, you get following text in dutch and french

<HR><h1>DISCUSS</h1>
<BR>3. sender demonstr@demonstr8.com  will change when we have a domain name, it currently uses it's own domain which is demonstr8.
<BR>17.	- If you break the flow in the reservation form, the slot is blocked. Can this be if you run through the entire slot reservation flow

<BR>[<b>DC</b>:need to reserver in case of multiple bookings, we could have a complete flag which is set after part 2 or just delete any slots that are not confirmed within x days.]

<BR><b>DISCUSS</b>40.	-  ON 19.04.2005 in SA I could join a group in last-minute but also the sequential slot before and after where there were 0 people in

<BR>[<b>DC</b>: After several time consuming attempts and checks, I simply cannot recreate this, there is <a href="http://itw5.demonstr8.com/web/en/admin/showgroup.htm?gid=S20050419t3A01&nomenu=3">one group</a> on that day, so joining should is allowed]

<BR>b>DISCUSS</b>41.	- Sometimes it is possible to book more than 16 persons in last-minute
<BR>[<b>DC</b>: essentially this if possible if slot A has part of a BIG group which means that more than 15 spaces available <B>BUT</b>
I cannot physically insert > 15 people ???


<b>Note:</b><BR>
last minute check fro link seems to look if slot is open and not is spaces are availaible.
Hence if 3 slots are open but full, the link is still shown which is a not desired action.
