<?
	if(isset($slot))
	{
		$showbrew=strtoupper($slot[0]);
		$showmonth=substr($slot,5,2);
		$showday=substr($slot,7,2);
		$comm="adminshow";

	//	echo 	"$showbrew - $showmonth - $showday";
	}

	title(Calendar);
	$showgroups=1;

	if(!$nomenu)
		include("includes/admin_cal_form.inc.php");

	if($comm=="adminshow")
	{

		$quicklink=trim($quicklink);

		if((isset($quicklink))&&($quicklink!="quicklink")&&($quicklink!=""))
		{
			echo "Using quicklink:<b>$quicklink</b>";
			$showbrew=$quicklink[0];
			$showyear=substr($quicklink,1,4);
			$showmonth=substr($quicklink,5,2);
			$showday=substr($quicklink,7,2);
			$showview="day";

		}


		if($showview=="week")
			echo "<h1>Bookings for ".$brew_names[$showbrew]." - ".$month_names[$showmonth-1]." Week $showweek $showyear - $showguide</h1>";
		else
			echo "<h1>Bookings for ".$brew_names[$showbrew]." - ".$month_names[$showmonth-1]." $showday $showyear - $showguide</h1>";

	//-----------------------------------------------
	// SET UP ALL THE VARIABLES WE NEED TO SHOW DAY
	//-----------------------------------------------

		$y=date("Y");
		$m=twodigit($showmonth);
		$d=twodigit($showday);

		$mydate=$y.$m.$d;

	//---------------------
	//----NOW SHOW THE DAY
	//---------------------


	$mybrewery=$brew_paths[$showbrew];
	$myBrewery=$mybrewery;

	include("includes/brewery_data.inc.php");  // GET BREWERY DATA


	list($y, $m, $d) =explode_date($mydate);

	$day_open=1;  // ALWAYS OPEN
	if((!$yeardays[$m][$d])&&($showview=="day"))
	{
		//echo gw(warn)."Brewery is not officially open on this day $d.$m.$y.<BR>";
	}

	//list($y, $m, $d) =explode_date($mydate);

	$lastminute=0;

	//echo(getword(optional)."<BR><BR>");

	$daytable=getTemplate(admin_table_day);
	$dayrow_clean=getTemplate(admin_table_day_row);

	$slotstub=$showbrew;

	//----------------------------------------------------------------
	// MAKE QUERY
	//----------------------------------------------------------------



	if($showview=="week")
	{

		$adate=datefromweeknr($y, $showweek, 01);
		$start=	date('Y-m-d',$adate);
		$date_msg='<b>Date: '.date('D d.m.Y',$adate);

		$adate=datefromweeknr($y, $showweek, 07);
		$stop=	date('Y-m-d',$adate);
		$date_msg.=' - '.date('D d.m.Y',$adate).'</b><BR>';

		echo $date_msg;

		$kw=" `SLOT_DATE` >= '$start' AND `SLOT_DATE`<='$stop' ";

		$slotstub=$slotstub[0];

	}
	else
		$kw=" `SLOT_DATE`='$showyear-$showmonth-$showday'";


	if($showguide!="Administrator")
		$guide_check=" SLOT_GUIDE='$showguide' ";
	else
		$guide_check="";




	if($showbrew!="A")  // ALL BREWERIES
	{

		if($guide_check)
			$guide_check=$guide_check." AND ";   // ADD AND IF GUIDE CRITERIA IS NOT EMPTY
			//$guide_check=" AND ".$guide_check;   // ADD AND IF GUIDE CRITERIA IS NOT EMPTY

		$query="select * from slots where SLOT_CODE like '$slotstub%' AND $guide_check $kw order by SLOT_CODE";
	}
	else

	{
		if($guide_check)    // ADD AND IF GUIDE CRITERIA IS NOT EMPTY
			$kw=" AND ".$kw;

		$query="select * from slots where  $guide_check $kw order by SLOT_CODE";
	}

	$result=dosql($query,0);

	$slotcodes=array();
	$slotdata=array();


	if(mysql_num_rows($result))   // GO THROUGH SLOTS IF ANY EXIST
	{
		echo "<table border=0>";
		$slotsopen=mysql_num_rows($result);  // HOW MANY  SLOTS

		while ($row = mysql_fetch_assoc($result))  // GET CODES & DATA
		{
			$mycode=$row["SLOT_CODE"];
			$slotcodes[]=$row["SLOT_CODE"];

			foreach($row as $k => $v)   // GET FIELD VALUES IN VARS WHICH ARE FIELD NAMES
				$$k=$v;


			$myclass=' class="adminslotheader" ';
			if($SLOT_GUIDE=="")
				$SLOT_GUIDE="Assign Guide";




		//	list($y, $m, $d) =explode_date($mydate);
		//	$testday=date("D",mktime(0,0,0,$m,$d,$y));

			$new_date=date_from_sc($SLOT_DATE,1);
			list($y, $m, $d) =explode_date(str_replace("-","",$SLOT_DATE));
			$dayofweek=date("D",mktime(0,0,0,$m,$d,$y));

			$myspaces="";
			$mysspacescount=30-$SLOT_COUNT;
			$myspaces="$mysspacescount Spaces";   // was "People: $SLOT_COUNT"

			if((strstr($SLOT_CODE,"C"))||(strstr($SLOT_CODE,"D")))
				$myspaces="C/D Slot";



			$slotdata[$mycode]="<TR><TD $myclass><b>".$brew_names[$SLOT_CODE[0]]."&nbsp;$dayofweek-$new_date-<font color=red>".substr($SLOT_CODE,9,3)."</font></b></TD><TD $myclass><a href=\"javascript:makepopupnamed('showslot.htm?sid=$SLOT_CODE&nomenu=3',250,480,'slot')\">".admininfo($SLOT_CODE)."</a></TD><TD $myclass><a href=\"javascript:makepopupnamed('guide_report.htm?sid=$SLOT_CODE&nomenu=3',450,560,'report')\"><img src=\"/images/report.gif\" border=0></a></TD><TD $myclass>&nbsp;</TD><TD $myclass>$SLOT_LANG</TD><TD $myclass>$myspaces</TD><TD $myclass><a href=\"javascript:makepopupnamed('assign_guide.htm?sid=$SLOT_CODE&nomenu=3&name=$SLOT_GUIDE',250,250,'guide')\">$SLOT_GUIDE&nbsp;</TD><TD $myclass>&nbsp;</TD>";


		}

		foreach($slotcodes as $k=>$slotcode)  // GO THROUGH SLOTS AND GET GROUPS CODES AND DATA
		{

			$groups_result=dosql("select * from groups where GROUP_CODE like '$slotcode%' order by GROUP_CODE",0 );
			$groups_in_slot=mysql_num_rows($groups_result);

			while ($groups = mysql_fetch_assoc($groups_result))
			{
				$gid=$groups["GROUP_IDENT"];
				$group_idents[]=$groups["GROUP_IDENT"];


				foreach($groups as $d => $c)   // GET FIELD VALUES IN VARS WHICH ARE FIELD NAMES
					$$d=$c;

				//$gcount=$GROUP_COUNT-$GROUP_SPECIAL_DELTA;
				$this_slotcode=$groups["GROUP_CODE"];
				$gcount=$groups["GROUP_COUNT"];

				$gspecial=$groups[GROUP_SPECIAL_TYPE ];
				if($gspecial)
					$gname.="&nbsp;<B>($gspecial)</B>";

				if($GROUP_BOOKPAYMENT!="invoice")
					$price="$GROUP_PRICE &euro;";
				else
					$price="-";

				if($GROUP_SPECIAL_TYPE=="ITW")
					$price="-";

				if(($GROUP_BIG=="1")&&(strstr($GROUP_CODE,"B")))  // PRICE IS ALREADY IN THE A SLOT SO
						$price="-";

				if($GROUP_BIG=="1")
				{

					$big="<b>B</b>";

					if($this_slotcode[strlen($this_slotcode)-1]=="A") // GET A or B
					{
						if($GROUP_REALCOUNT>30)
						{
							$mytmp=$GROUP_REALCOUNT-30;
							$gcount="$mytmp of $GROUP_REALCOUNT";
						}
						else
							$gcount=$GROUP_REALCOUNT;
						//$gcount=$GROUP_BOOKJUN_COUNT+$GROUP_BOOKSEN_COUNT +$GROUP_BOOKSTU_COUNT;
					}
					else
					{
						if($GROUP_REALCOUNT>30)
						{
							$gcount="30 of $GROUP_REALCOUNT";
						}
						else
						 $gcount=0;
					}

				}
				else
					$big="";


				if($GROUP_SPECIAL_TYPE)
					$GROUP_SPECIAL_TYPE="(".$GROUP_SPECIAL_TYPE.")";


				if(trim($_SESSION['SSUSER_ROLE'])=="ADMIN")
					$del_link="<a href=\"javascript:makepopupnamed('admin_delgroup.htm?gid=$gid&nomenu=3',400,300,'delgroup')\">$icon_del</a>";
				else
					$del_link="";

				if($_SESSION['SSUSER_NAME']=="security")  // VIEW ONLY
					$del_link="";

				$GROUP_COMMENT=trim($GROUP_COMMENT);

				if(($GROUP_COMMENT)&&($GROUP_COMMENT!="Admin:"))
					$mygroup_comments="<BR>".str_replace("Admin:","",$GROUP_COMMENT);
				else
					$mygroup_comments="";

				$g_row="<TR bgcolor=\"#FFFFFF\"><TD>&nbsp;$del_link</TD><TD>$GROUP_STARTTIME&nbsp;~&nbsp;$GROUP_STOPTIME</TD><TD>$big</TD><TD>$GROUP_SPECIAL_TYPE</td><TD>$gcount people</td><td>$GROUP_BOOKFNAME $GROUP_BOOKSNAME, $GROUP_BOOKGNAME ($GROUP_BOOKMOBILE) $mygroup_comments</TD><TD>$GROUP_BOOKPAYMENT</TD><TD>$price</TD><TD><a href=\"javascript:makepopupnamed('showgroup.htm?gid=$gid&nomenu=3',700,500,'group')\"> ".admininfo($gid)."</a></td></TR>\n";
				$groupdata[$gid].=$g_row;  // GET GROUP DATA IN ROW
			}

			echo $slotdata[$slotcode]."<TD $myclass>$groups_in_slot Group(s)</TD></TR>\n";  // SHOW SLOT HEADER + NUMBER OF GROUPS

			if(!is_array($group_idents))
			{
				echo "NOT ARRAY: a slot <b>$slotcode</b> exists which has no groups, please report this message to $service<BR>";
			}
			else
			{
				foreach($group_idents as $k=>$groupcode)
				{
					if($showgroups)
						echo $groupdata[$groupcode];
				}
			}
			$group_idents="";
		}


		echo "</table>";

	}
	else
	{
		echo(gw(warn)."<b>No slots open.</b>");
	}








	echo $js_closewin;
	"<BR><BR><center><a href=\"javascript:window.close()\">Close this Window</a></center>";


















	exit;



















































	$slots="";
	$bookings_today=0;
	$maxslots=count($timeslots);

	$possible=array();
	$possible=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$possibleA=array();
	$possibleA=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$possibleB=array();
	$possibleB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE


	$possibleAB=array();
	$possibleAB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$langAB=array();
	$langAB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$infoAB=array();
	$infoAB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$bookingsA_today=0;
	$bookingsB_today=0;

	$SLOTA_TABLE="";
	$SLOTB_TABLE="";


	$Alabel="Slot A";
	$Blabel="Slot B";

	$statusA="-";
	$statusB="-";

	$NL=0;
	$FR=0;
	$EN=0;



	//------------------------------------------------------------------
	// END OF PART 1
	//------------------------------------------------------------------
	// NOW GET INFO ABOUT A & B SLOTS
	//------------------------------------------------------------------


	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	{

		//-----MAKE SLOT CODES A & B
		$slotcode=$myBrewery[0].$mydate."t".$slotcount;
		$slotcodeA=$slotcode."A";
		$slotcodeB=$slotcode."B";

		//echo "k=$k<BR>";

		//---------------------------------------
		//-----AND GET INFO ABOUT THAT A SLOT ---
		//---------------------------------------

		$slots[$slotcodeA]=getonerow ("select * from slots where SLOT_CODE='$slotcodeA'",0);

		//-----------------------------------------------------------------------------------
		//-----IF SLOT HAS DETAILS THEN OPEN NEIGHBOUR SLOTS UNLESS WE ARE IN LASTMINUTE MODE
		//-----------------------------------------------------------------------------------
		if(!empty($slots[$slotcodeA]))
		{
			if(!$lastminute)   // ONLY OPEN SLOTS WHERE SPACES ARE AVAILABLE AND CONFIRMED
			{
				$possibleA[$slotcount]=1;

				$possibleA[$slotcount-1]=1;  //ACTIVATE NEIGHBOURS IF RESERVATION EXISTS, EVEN IF IT IS STILL OPTIONS, i.e. LESS THAN 15 PEOPLE
				$possibleA[$slotcount+1]=1;

				$bookingsA_today++;
				$slots[$slotcodeA][PEOPLE_SOFAR]=slot_count($slotcodeA,0);//HOW MANY PEOPLE ??
			}
			else
			{
				if(slot_count($slotcodeA)>=$MINCONF)
				{
					$possibleA[$slotcount]=1;
					$bookingsA_today++;
					$slots[$slotcodeA][PEOPLE_SOFAR]=slot_count($slotcodeA,0);   //HOW MANY PEOPLE ??
				}
			}
		}

		//---------------------------------------
		//-----AND GET INFO ABOUT THAT B SLOT ---
		//---------------------------------------

		$slots[$slotcodeB]=getonerow ("select * from slots where SLOT_CODE='$slotcodeB'",0);

		//-----------------------------------------------------------------------------------
		//-----IF SLOT HAS DETAILS THEN OPEN NEIGHBOUR SLOTS UNLESS WE ARE IN LASTMINUTE MODE
		//-----------------------------------------------------------------------------------

		if(!empty($slots[$slotcodeB]))
		{
			if(!$lastminute)   // ONLY OPEN SLOTS WHERE SPACES ARE AVAILABLE AND CONFIRMED
			{

			$possibleB[$slotcount]=1;

			$possibleB[$slotcount-1]=1;  			//ACTIVATE NEIGHBOURS IF RESERVATION EXISTS, EVEN IF IT IS STILL OPTIONS, i.e. LESS THAN 15 PEOPLE
			$possibleB[$slotcount+1]=1;
			$bookingsB_today++;
			$slots[$slotcodeB][PEOPLE_SOFAR]=slot_count($slotcodeB,0);//HOW MANY PEOPLE ??
		}
		else
			{
				if(slot_count($slotcodeB)>=$MINCONF)
				{
					$possibleB[$slotcount]=1;
					$bookingsB_today++;
					$slots[$slotcodeB][PEOPLE_SOFAR]=slot_count($slotcodeB,0);   //HOW MANY PEOPLE ??
				}
			}
		}

		$slotcount++;
	}

	$infoA="Slot A has $bookingsA_today of $maxslots bookings - ";
	$infoB="Slot B has $bookingsB_today of $maxslots bookings - ";

	//-------------------------------------
	//-----IF WE HAVE NO BOOKINGS THEN ALL SLOT ARE POSSIBLE-----
	//-------------------------------------
	if($bookingsA_today==0)
	{

		//-------------------------------------
		// UNLESS OF COURSE WE ARE NOT OPEN ON THIS DAY  I ASSUME IF WE CAN'T OPEN A THEN B IS SAFE
		//-------------------------------------

		//$dow=date("w",mktime(0,0,0,$m,$d,$y));



		//if($dow==0)  // SUN IN DATE(W) HAS 0 AND I HAVE MON AS 0 SO THIS IS THE TRANSLATION - MIGHT BE ABLE TO ELEGANTLY MOD THIS SOMEHOW
		//	$dow=6;
		//else
		//	$dow--;

		//if(!$mydays[$dow])
		//	$possibleA=initarray($maxslots,0);
		//else
		//{
		//	if(!$lastminute)    // OPEN ALL BECAUSE NONE SET UNLESS WE ARE LAST MINUTE
		//	{
		//		unset($possibleA);
				$possibleA=initarray($maxslots,1);

				if(!$day_open)
					$possibleA=initarray($maxslots,0);
		//	}

		//}
	}




	if($bookingsB_today==0)
	{
		if(!$lastminute)  // OPEN ALL BECAUSE NONE SET UNLESS WE ARE LAST MINUTE
		{
			unset($possibleB);
			$possibleB=initarray($maxslots,1);
		}
	}



	// IF BREWERY IS CLOSED SHUT B TOO

	if(!$day_open)
		$possibleB=initarray($maxslots,0);


	// FOR MORE THAN 30 PEOPLE, B MUST BE EMTPY AND A HAVE AT LEAST AS MANY SPACES AS THE GROUP > 30 IS


	//----------------------------------------------------------
	// ----- THIS SECTION DEALS WITH BIG GROUPS-----------------
	//----------------------------------------------------------


	if($_SESSION['SSGROUP_COUNT']>30)
	{
		echo "Checking for slots available in A & B.<BR>";

		$remainder=$_SESSION['SSGROUP_COUNT']-30;

		$slotcount=1;
		foreach ($timeslots as $k=>$timeslot)
		{

			//-----MAKE SLOT CODES A & B
			$slotcode=$myBrewery[0].$mydate."t".$slotcount;
			$slotcodeA=$slotcode."A";
			$slotcodeB=$slotcode."B";

			$acount=$slots[$slotcodeA][PEOPLE_SOFAR];
			$bcount=$slots[$slotcodeB][PEOPLE_SOFAR];

			$booklink="<a class=\"dayopen\" href=\"admin_reservation.htm?slotcodeAB=$slotcodeA";

			$langAB[$slotcount]="";
			$infoAB[$slotcount].="";

			if($bcount!=0)
				$infoAB[$slotcount].="slot B is not available";

			if(($acount+$remainder)>31)
				$infoAB[$slotcount].=" people in A $acount + (group-30) $remainder, is too big";


			if(($bcount==0)&&(($acount+$remainder)<31))
			{
				$ABcount=$acount+$remainder;

				if($acount==0)
				{
					$infoAB[$slotcount]="t$slotcount available, A & B are empty. Lang=ANY";
					//$langAB[$slotcount]="ANY";


					if($possibleA[$slotcount]==1)
					{
						$langAB[$slotcount]=$booklink."&setlang=nl\">".$nl_flag."</a>&nbsp;";
						$langAB[$slotcount].=$booklink."&setlang=fr\">".$fr_flag."</a>&nbsp;";
						$langAB[$slotcount].=$booklink."&setlang=en\">".$en_flag."</a>&nbsp;";
					}
					else
						$langAB[$slotcount]="";


				}
				else
				{
					$langAB[$slotcount]=$booklink."&setlang=".$slots[$slotcodeA][SLOT_LANG]."\">".getword($slots[$slotcodeA][SLOT_LANG])."</a>&nbsp;";
					$infoAB[$slotcount]="t$slotcount possible Slot B is empty and  A=$acount + Rest of group=$remainder ($ABcount) is smaller than or equal to 30. But Lang is restricted.";
				}




				// NOW TEST IF IT IS A SLOT IS POSSIBLE TO ENSURE SEQUENTIAL BOOKINGS



				if(($possibleA[$slotcount]==1)||($bookingsA_today==0))
				{
					$possibleAB[$slotcount]=1;
				}
				else
				{
					$possibleAB[$slotcount]=A;
					$infoAB[$slotcount].=" But this A slot is not possible.";
					$langAB[$slotcount].="";
				}

			}
			else
			{
				//echo "t$slotcount NOT possible.<BR>";
				$possibleAB[$slotcount]=0;
			}
			$slotcount++;
		}


	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	//foreach($possibleAB as $k=>$v)
	{
		//echo "t$slotcount possible=".$possibleAB[$slotcount]."  Lang=".$langAB[$slotcount];
		$ainfo=info($infoAB[$slotcount],0);

		if($possibleAB[$slotcount]==1)
			$slot_status="slotopen";
		else
			$slot_status="slotclosed";


		$dayrow=str_replace("AX_DAYCLASS","class=\"$slot_status\"",$dayrow_clean);
		$dayrow=str_replace("<AX_SLOTLINK>",$langAB[$slotcount] ,$dayrow);

		$dayrow=str_replace("<AX_TIMESLOT_START>",$timeslot[0],$dayrow);
		$dayrow=str_replace("<AX_TIMESLOT_STOP>",$timeslot[1],$dayrow);
		$dayrow=str_replace("<AX_DEBUG>",$ainfo ,$dayrow);
		$dayrows.=$dayrow;

		$slotcount++;
	}


	$SLOTAB_TABLE=str_replace("<AX_TIMESLOTS>",$dayrows,$daytable);


}
else
{

	//-------------------------------------
	//-----NOW WORK OUT IF WE CAN OPEN SLOT B
	//-------------------------------------
	//1. ARE ALL OF SLOTS A FULL OR IS IT LAST MINUTE
	//-------------------------------------


	if(($bookingsA_today==$maxslots)||($lastminute)||isloggedin())
	{
		$statusA="";
		$statusA.="Status: A has bookings in all slots";
		$statusA.=" Checking to see if the lang is available for the group size.";


		// GET LANGS HERE BUT I THINK WE CAN MOVE THIS UP LATER.
		$slotcount=1;

		foreach ($timeslots as $k=>$timeslot)
		{
				$slotcode=$myBrewery[0].$mydate."t".$slotcount;
				$slotcodeA=$slotcode."A";

				//echo ("TOTAL=".$slots[$slotcodeA][PEOPLE_SOFAR]."+".$_SESSION['SSGROUP_COUNT']."<BR>");

				$new_group_total=$slots[$slotcodeA][PEOPLE_SOFAR]+$_SESSION['SSGROUP_COUNT'];

				//---ONLY IF LANG IS NOT POSSIBLE IN SLOT A

				if($new_group_total<=30)
					$$slots[$slotcodeA][SLOT_LANG]++;  // BEWARE, IT CREATES THE VARS EN,FR,NL
				//else
				//	echo "<BR><B>t".$slotcount." $slotcodeA people in group (".$slots[$slotcodeA][PEOPLE_SOFAR].") + groupcount (".$_SESSION['SSGROUP_COUNT'].")= ($new_group_total) too big.</b>";

				//echo $slots[$slotcodeA][SLOT_LANG];


			$slotcount++;
		}

		//echo "nl=$NL<BR>";
		//echo "fr=$FR<BR>";
		//echo "en=$EN<BR>";

		$Bsearch="";
		if($NL==0)
			$Bsearch.=" Checking Slot B for $nl_flag spaces.";
		if($FR==0)
			$Bsearch.=" Checking Slot B for $fr_flag spaces.";
		if($EN==0)
			$Bsearch.=" Checking Slot B for $en_flag spaces.";


		//----------------------------------------------------
		//LET'S JUST SHOW SLOT B
		//----------------------------------------------------
		$slotcount=1;

		foreach ($timeslots as $k=>$timeslot)
		{
			$slotcode=$myBrewery[0].$mydate."t".$slotcount;
			$slotcodeB=$slotcode."B";
		}



		//----------------------------------------------------
	}
	else
	{
		$infoA.="Status: A Still has spaces.";
		$EN=A;
		$NL=A;
		$FR=A;
	}



	//-----CHECK SLOT A COLOURS-----
	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	{
		$slotcode=$myBrewery[0].$mydate."t".$slotcount;
		$slotcodeA=$slotcode."A";
		$slotcodeB=$slotcode."B";

		$spaces_left=0;
		$peoplecountA=0;
		$peoplecountB=0;

		//get slot info  //CHECK FOR SPACES AVAILABLE

		$lang="";
		//echo "$slotcount => $possible[$slotcount]<BR>";
		//	$peoplecount=slot_count($slotcode);//HOW MANY PEOPLE ??

		$peoplecountA=slot_count($slotcodeA);//HOW MANY PEOPLE ??
		$peoplecountB=slot_count($slotcodeB);//HOW MANY PEOPLE ??

		$new_group_total=$slots[$slotcodeA][PEOPLE_SOFAR]+$_SESSION['SSGROUP_COUNT'];


		if(!$possibleA[$slotcount])
		{
			$slot_status="slotclosed";
			$lang="";
			$message="Slot is not yet open.";
		}
		elseif($new_group_total>30)
		{
			$spaces_left=$maxgroupsize-$peoplecountA;
			$slot_status="slotfull";
			$lang=$slots[$slotcodeA]["SLOT_LANG"];
			$message="+".$_SESSION['SSGROUP_COUNT']."=$new_group_total Not possible, not enough room in slot. (NOT ENOUGH SPACES)";
		}
		elseif($peoplecountA==30)
		{
			$slot_status="slotfull";
			$message="Slot is fully booked (SLOT FULL)";
		}
		else
		{
			if($new_group_total<15)
			{
				$slot_status="adminslotoption";
				$optional="*";
			}
			else
			{
				if($lastminute)
					$slot_status="slotlastminute";
				else
					$slot_status="slotopen";

				$optional="";
			}

			$booklink="<a class=\"dayopen\" href=\"admin_reservation.htm?slotcode=$slotcodeA";

			if(!empty($slots[$slotcodeA]))  // IF OPEN GET LANGUAGE IF DEFINED
			{
				$lang=$slots[$slotcodeA]["SLOT_LANG"];
				$lang=$optional.$booklink."\">".getword($lang)."</a>";

				$spaces_left=$maxgroupsize-$peoplecountA;
				$message="Slot is open and has a booking, $spaces_left spaces ($maxgroupsize-$peoplecountA) are available in ".$slots[$slotcodeA]["SLOT_LANG"].".";

			}
			else
			{

				$message="Slot is open. 30 spaces are available in all languages";
				//$lang=$allflags;
				$lang="$optional";
				$lang.=$booklink."&setlang=nl\">".$nl_flag."</a>&nbsp;";
				$lang.=$booklink."&setlang=fr\">".$fr_flag."</a>&nbsp;";
				$lang.=$booklink."&setlang=en\">".$en_flag."</a>&nbsp;";


				//$lang=$allflags;

			}
		}
		$dayrow=str_replace("AX_DAYCLASS","class=\"$slot_status\"",$dayrow_clean);
		$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);

		$gcountA=groups_in_slot($slotcodeA);

		$dayrow=str_replace("<AX_TIMESLOT_START>",$timeslot[0],$dayrow);
		$dayrow=str_replace("<AX_TIMESLOT_STOP>",$timeslot[1],$dayrow);
		$dayrow=str_replace("<AX_SLOTCODE>",$slotcodeA,$dayrow);
		$dayrow=str_replace("<AX_SLOTCOUNT>",$peoplecountA,$dayrow);
		$dayrow=str_replace("<AX_GROUPCOUNT>",$gcountA,$dayrow);
		$dayrow=str_replace("<AX_SLOTSPACES>",$spaces_left,$dayrow);
		$dayrow=str_replace("<AX_GUIDE>","Pascal",$dayrow);
		$dayrow=str_replace("<AX_DEBUG>",info($slotcodeA."->".$peoplecountA." people in slot, $gcountA groups in slot, ".$message,0,1) ,$dayrow);

		$dayrows.=$dayrow;
		$slotcount++;
	}

	//echo "</td><td>";

	$SLOTA_TABLE=str_replace("<AX_TIMESLOTS>",$dayrows,$daytable);




//-------------------------------------
//----SLOT B
//-------------------------------------

//echo "nl=$NL<BR>";
//echo "fr=$FR<BR>";
//echo "en=$EN<BR>";


if(($NL=="0")||($FR=="0")||($EN=="0")||isloggedin())   // THIS OPENS SLOT B FOR LOGGED IN USERS!!
{

	//$dayrow_clean=getTemplate(admin_table_day_rowB);
	$dayrow_clean=getTemplate(admin_table_day_row);
	$dayrow="";
	$dayrows="";
	$statusB="";
	$statusB.="Status: B, Slot B can be opened.";
	$statusB.=$Bsearch;

	//-----CHECK SLOT B COLOURS-----
	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	{
		$slotcode=$myBrewery[0].$mydate."t".$slotcount;
		$slotcodeA=$slotcode."A";
		$slotcodeB=$slotcode."B";

		$spaces_left=0;
		$peoplecountA=0;
		$peoplecountB=0;

		//get slot info  //CHECK FOR SPACES AVAILABLE

		$lang="";
		//echo "$slotcount => $possible[$slotcount]<BR>";
		//	$peoplecount=slot_count($slotcode);//HOW MANY PEOPLE ??

		$peoplecountA=slot_count($slotcodeA);//HOW MANY PEOPLE ??
		$peoplecountB=slot_count($slotcodeB);//HOW MANY PEOPLE ??

		$new_group_total=$slots[$slotcodeB][PEOPLE_SOFAR]+$_SESSION['SSGROUP_COUNT'];



		if(!$possibleB[$slotcount])
		{
			$slot_status="slotclosed";
			$lang="";
			$message="Slot is not yet open.";
		}
		elseif($new_group_total>30)
		{
			$slot_status="slotfull";
			$message="+".$_SESSION['SSGROUP_COUNT']."=$new_group_total Not possible, not enough room in slot.";
			$spaces_left=$maxgroupsize-$peoplecountB;
			$lang=$slots[$slotcodeB]["SLOT_LANG"];
		}
		elseif($peoplecountB==30)
		{
			$slot_status="slotfull";
			$message="Slot is fully booked";
		}
		else
		{
			if($new_group_total<15)
			{
				$slot_status="adminslotoption";
				$optional="*";
			}
			else
			{
				if($lastminute)
					$slot_status="slotlastminute";
				else
					$slot_status="slotopen";

				$optional="";
			}

			$booklink="<a class=\"dayopen\" href=\"admin_reservation.htm?slotcode=$slotcodeB";

			if(!empty($slots[$slotcodeB]))  // IF OPEN GET LANGUAGE IF DEFINED
			{
				$lang=$slots[$slotcodeB]["SLOT_LANG"];

				//$lang=$booklink."\"><img src=\"/images/".strtolower($lang).".gif\" alt=\"".getword($lang)."\" border=0>";
				$lang=$optional.$booklink."\">".getword($lang)."</a>";

				$spaces_left=$maxgroupsize-$peoplecountB;
				$message="Slot is open and has a booking, $spaces_left spaces ($maxgroupsize-$peoplecountA) are available in ".$slots[$slotcodeB]["SLOT_LANG"].".";

			}
			else
			{
				$message="Slot is open. 30 spaces are available in all languages";
				//$lang=$allflags;

				$lang="$optional";


				if($NL==0)
					$lang.=$booklink."&setlang=nl\">".$nl_flag."</a>&nbsp;";

				if($FR==0)
					$lang.=$booklink."&setlang=fr\">".$fr_flag."</a>&nbsp;";

				if($EN==0)
					$lang.=$booklink."&setlang=en\">".$en_flag."</a>&nbsp;";


				//$lang=$allflags;

			}
		}

		$dayrow=str_replace("AX_DAYCLASS","class=\"$slot_status\"",$dayrow_clean);

		if($slot_status=="slotopen")
		{
			$booklink=$booklink."\">BOOK</a>";
			//$dayrow=str_replace("<AX_SLOTLINK>",$booklink." | ".$slotcode." | ".$lang ,$dayrow);
			$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);
		}
		else
			$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);


		$gcountB=groups_in_slot($slotcodeB);


		$dayrow=str_replace("<AX_TIMESLOT_START>",$timeslot[0],$dayrow);
		$dayrow=str_replace("<AX_TIMESLOT_STOP>",$timeslot[1],$dayrow);
		$dayrow=str_replace("<AX_SLOTCODE>",$slotcodeB,$dayrow);
		$dayrow=str_replace("<AX_SLOTCOUNT>",$peoplecountB,$dayrow);
		$dayrow=str_replace("<AX_GROUPCOUNT>",$gcountB,$dayrow);
		$dayrow=str_replace("<AX_SLOTSPACES>",$spaces_left,$dayrow);
		$dayrow=str_replace("<AX_GUIDE>","Pascal",$dayrow);
		$dayrow=str_replace("<AX_DEBUG>",info($slotcodeB.$peoplecountB." people in slot, $gcountB groups in slot, ".$message,0,1) ,$dayrow);

		//$dayrow=str_replace("<AX_DEBUG>",info($slotcodeB."</b><br>".$peoplecountB." people in slot, $gcountB groups in slot, ".$message) ,$dayrow);

		$dayrows.=$dayrow;
		$slotcount++;
	}

	$SLOTB_TABLE=str_replace("<AX_TIMESLOTS>",$dayrows,$daytable);
}
else
{
	$infoB.="Slot B cannot be opened yet.";
	$Blabel="";

}  // EIF SLOT B


	//info($infoA,$infoecho);
	//info($infoB,$infoecho);

	$infoA.=info($statusA,0,1);
	$infoB.=info($statusB,0,1);

	echo'<table border="1" cellpadding="10" cellspacing="0" width=100%>';
	echo "<tr><td class=\"adminheader\">$infoA</td><td class=\"adminheader\">$infoB</td></tr>\n";

	echo "<tr><td valign=top>$SLOTA_TABLE &nbsp;</td><td valign=top>$SLOTB_TABLE &nbsp;</td></tr>\n";
	echo "</table><BR>";

	$slotcodeC=strtoupper($myBrewery[0]).$mydate."C";
	$slotcodeD=strtoupper($myBrewery[0]).$mydate."D";

	$cslots=getextra($slotcodeC,$mydate);
	$dslots=getextra($slotcodeD,$mydate);

	$result=dosql("select * from groups where GROUP_CODE like '$slotcodeC%'",0);
	$Cnums=mysql_num_rows($result)+1;

	$result=dosql("select * from groups where GROUP_CODE like '$slotcodeD%'",0);
	$Dnums=mysql_num_rows($result)+1;

	$slotcodeC.=$Cnums;
	$slotcodeD.=$Dnums;

	$Clink="<a href=\"admin_reservation.htm?slotcode=$slotcodeC&bookcount=$showcount\">";
	$Dlink="<a href=\"admin_reservation.htm?slotcode=$slotcodeD&bookcount=$showcount\">";



	if(is_array($cslots))   // IF WE GET AN ARRAY BACK, MAKE A TABLE OUT OF IT OTHERWISE USE THE "NO BOOKING MESSAGE RETURNED FROM FUNCTION"
	{
		$crows="<table border=0 width=100%>";
		$crows.="<TR><TH>Code</TH><TH>Lang</TH><TH>Guide</TH><TH>People</TH><TH>Comment</TH></TR>";
		foreach($cslots as $k=>$v)
		{
			$crows.="<tr><TD>".$v[SLOT_CODE]."</td><td>".$v[SLOT_LANG]."</td><td>".$v[SLOT_GUIDE]."</td><td>".$v[SLOT_COUNT]."</td><td>".$v[SLOT_EXTRA]."</td></tr>\n";
		}
		$crows.="</table>";
	}
	else
		$crows=$cslots;

	if(is_array($dslots))   // IF WE GET AN ARRAY BACK, MAKE A TABLE OUT OF IT OTHERWISE USE THE "NO BOOKING MESSAGE RETURNED FROM FUNCTION"
	{
		$drows="<table border=0 width=100%>";
		$drows.="<TR><TH>Code</TH><TH>Lang</TH><TH>Guide</TH><TH>People</TH><TH>Comment</TH></TR>\n";
		foreach($dslots as $k=>$v)
		{
			$drows.="<tr><TD>".$v[SLOT_CODE]."</td><td>".$v[SLOT_LANG]."</td><td>".$v[SLOT_GUIDE]."</td><td>".$v[SLOT_COUNT]."</td><td>".$v[SLOT_EXTRA]."</td></tr>\n";

		}
		$drows.="</table>";
	}
	else
		$drows=$dslots;


	echo '<table border="1" cellpadding="0" cellspacing="0" >';
	echo "<tr><td width=50% class=\"adminheader\">Slot C has $Cnums bookings</td><td width=50% class=\"adminheader\">Slot D has $Dnums bookings</td></tr>";
	echo "<tr><td valign=top>$crows</td><td valign=top>$drows</td></tr>";
	echo "<tr><td align=center><B>$Clink Make New C Slot Booking</a></td>";
	echo "<td align=center><b>$Dlink Make New D Slot Booking</a></td></tr>";
	echo "</table>";


}


}




?>



