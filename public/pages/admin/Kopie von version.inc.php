<u><B>Version Information</b></u><BR>
Current version is <b>0.7</b><BR>

Last change to this file: 22.03.05 (09:00)


<HR><HR>
<!--------------------------------------------------------->
<!-----------------------OPEN TASKS------------------------>
<u><b>Open Tasks</b></u><BR><BR>
<LI>Reports and Statistics
<LI>Guide reports
<LI>overview for non admin users
<LI>list of all bookings
<LI>guide reports
<LI>save all of admin and consumer reservation data to database
<LI>auto cancel program

<HR>
<LI>if slot and slot B are open with same lang, focus on filling optional
<LI>if group > 30, i.e. 35 then 30 go to B and 5 go to A, block A from being optional and cancelled.
<LI>check admin interface for booking large groups in ABCD slots.
<LI>add days of month to cal edit
<HR>
<HR>
<!--------------------------------------------------------->
<!--------------------------------------------------------->
<!--------------------------------------------------------->
<!--------------------------------------------------------->



<!-----------------------CHANGE LOG------------------------>
<u><b>Change Log</b></u><BR><BR>
<!-----------------------CHANGE LOG------------------------>


<!-----------------------------REPORT---------------------->
<b>Date:</b> 22.03.2005 (v0.6)<BR>
<b>Comments:</b><BR>
<LI> Studentcard wording in the admin reservation system appears for Stella reservation
<LI> admin reservation form is complete
<LI> admin reservation form is validated
<LI> admin reservation form -  invoice detail are mandatory if you choose payment = invoice
<LI> all mobile fiels are numbers only
<LI> all emails are checked for syntax
<LI> the reset databases option has a confirm message to go through before resetting the databases
<LI> all cost cener fields are shown for ITW employees in the consumer site
<LI> if you login in as admin or guide, special arrangement status VIP or ITW is reset
<LI> email now contains "confirm or cancel" (need translation)
<LI> red mandatory star in consumer and admin booking forms
<LI> next 2 day from now are absolutely locked and cannot be booked by a consumer even in last minute.
<LI> cancellations with 2 weeks of of the scheduled reservation are no longer possible  (How are these dealt with by Admin ?? Do we need a cancel Group option)
<LI> ages of specials slot now match true group size not "virtual" groupsize of 30 or 60
<LI> if the last group in a slot cancels, the "slot" session is deleted so all languages are available again
<LI> My Booking send the user a repeat confirm email for all not deleted, valid bookings.
<LI> fixed calendar view of hoegaarden.

<!-----------------------------REPORT---------------------->
<b>Date:</b> 22.03.2005 (v0.6)<BR>
<b>Comments:</b><BR>
<LI>implement Admin interface

<!-----------------------------REPORT---------------------->
<b>Date:</b> 21.03.2005 (v0.6)<BR>
<b>Comments:</b><BR>
<LI>Cancellation
<LI> Slot C / D form
<HR>

<!-----------------------------REPORT---------------------->
<b>Date:</b> 18.03.2005 (v0.6)<BR>
<b>Comments:</b><BR>
<LI>user autentication
<LI>implement Admin interface
<HR>


<!-----------------------------REPORT---------------------->
<b>Date:</b> 17.03.2005 (v0.6)<BR>
<b>Comments:</b><BR>
<LI>implement Admin interface
<HR>

<!-----------------------------REPORT---------------------->
<b>Date:</b> 16.03.2005 (v0.6)<BR>
<b>Comments:</b><BR>
<LI>add national holiday and extra days to monthly calendar
<LI>implemented day check for all breweries
<LI>added "days open" editor in admin
<LI>inserted all days open data for all breweries
<LI>started booking admin interface
<HR>

<!-----------------------------REPORT---------------------->
<b>Date:</b> 15.03.2005 (v0.5)<BR>
<b>Comments:</b><BR>
<LI>show last minutes bookings for options
<HR>
<!-----------------------------REPORT---------------------->

<b>Date:</b> 14.03.2005 (v0.5)<BR>
<b>Comments:</b><BR>
<LI>save group member info into members database  (name or email)
<LI>form validation
<HR>
<!-----------------------------REPORT---------------------->


<b>Date:</b> 11.03.2005 (v0.5)<BR>
<b>Comments:</b><BR>
<LI>tidy info page
<LI>added Leffe info page
<LI>tidy booking form + invoice & group member forms
<LI>changed month view with previous and next links
<LI>added page under construction message
<LI>moved stella discount message
<LI>added D8 only info option
<LI>added peole in booking form and removed dummy data
<LI>added privacy clause
<LI>1999 special buf fixed
<LI>freeze new bookings within next 2 weeks in month view
<LI>added closed if date has passed month view
<LI>removed specials from month view
<LI>added cost center minus payment for ITW employees
<HR>
<!-----------------------------REPORT---------------------->

<b>Date:</b> 10.03.2005 (v0.4)<BR>
<b>Comments:</b><BR>
<LI>groupcount =0 error fixed
<LI>Confirmation mail now contain correct data fields
<LI>Confirmation mails now available in all languages for all breweries
<LI>group price calculates and saved with group data
<LI>moved invoice data field to next form page
<LI>Added info page before reservation
<LI>Added FR & NL words
<LI>Added Under Construction option
<LI>days and month added for NL & FR calendar
<HR>


<!-----------------------------REPORT---------------------->

<b>Date:</b> 09.03.2005 (v0.4)<BR>
<b>Comments:</b><BR>
<LI>Reservation is  module is ready to be tested
<LI>group data is saved into database
<LI>confirmation emails are sent
<LI>group size limited to 1-60 people
<LI>new groups database structure for booking informationgroup size limited to 1-60 people
<LI>set up admin area
<LI>initiated this change.log for project documentation and version control
<HR>

<!-----------------------------REPORT---------------------->
<b>Date:</b> 08.03.2005 (v0.3)<BR>
<b>Comments:</b><BR>
<LI>Timeslot management module in progress
<LI>Booking for large groups possible
<LI>special reservations for ITW & VIP possible
<HR>

<!-----------------------------REPORT---------------------->
<b>Date:</b> 04.03.2005 (v0.2)<BR>
<b>Comments:</b><BR>
<LI>Month view is finished
<LI>Timeslot management module working for slot A and B
<HR>

<!-----------------------------REPORT---------------------->
<b>Date:</b> 18.02.2005 (v0.1)<BR>
<b>Comments:</b><BR>
<LI>First visuals & a starting-page should

<!-----------------------------END---------------------->
<HR>
<HR>
END
