<?
//UPDATE `groups` SET `GROUP_CONFIRMED` = 'Y',GROUP_CONFIRMED_DATE='29.04.2005 17:22:36' WHERE `GROUP_IDENT` ='S20050429T2A01'

//--------------------------------------
// /web/en/admin/admin_cancel.htm?cronjob=1
// THIS IS THE URL FOR THE CRONJOB TO CALL THIS SCRIPT
//--------------------------------------



global $email_headers;


//--------------------------------------
// WORK OUT THE DATES
//--------------------------------------

	$nowtime=mktime(0, 0, 0, date("m")  , date("d"), date("Y"));

	$nowtime=$nowtime;   // +2days = NEXT 2 DAYS WHICH ARE COMPLETELY BLOCKED.
	$opentime=$nowtime+$FREEZE_SECONDS;  // ADD 2 WEEKS
	$opentime=$opentime-$spd;

//--------------------------------------
// THIS SETS IT UP SO IT WORKS FROM CRON
//--------------------------------------

if(!isset($cronjob))
	$cronjob=0;

if($cronjob==1)
{

	$comm=cancelok;
	$delete=1;
	$email_msg='';
	$email_msg.="Today's Date is ".date("M-d-Y",$nowtime).".\n\n";
	$email_msg.="Today + freeze period (less 1 day) is ".date("M-d-Y",$opentime).".\n\n";

	$email_subject="Cancellations for ".date("M-d-Y",$opentime);

	$showyear=date("Y",$opentime);

	if(!isset($showday))
		$showday=date("d",$opentime);

	$showmonth=date("m",$opentime);

	$email_subject="Cancellations for ".date("M-d-Y",$opentime);
}


title("Cancel Overview Module");

	if($comm=="delone")
	{
		echo "checking $code<BR>";

		$code=trim($code);
		if((strstr($code,"C"))||(strstr($code,"D")))
			$code=$code."01";

		//echo "<BR>select GROUP_MD5IDENT from groups where GROUP_IDENT='$code'";

		$ID=getonewert("select * from groups where GROUP_IDENT='$code'",'GROUP_MD5IDENT',0);

		if(!isset($sendmail))
			$sendmail=0;

		cancel_group($ID,$sendmail,0);
		finito();

	}
	elseif($comm=="cancelok")
	{
		if($delete)
			echo "<b>Cancelling Groups.</b><BR>";
		else
			echo "<b>Showing Groups.</b><BR>";


		$showday=twodigit($showday);
		$showmonth=twodigit($showmonth);

		$msg="Checking slots on <b>$showday. $showmonth. $showyear </b> for non-big, non-cancelled, confirmed groups with less that 15 members.<BR><BR>(Slot/Group)<BR>";
		echo $msg;
		$email_msg.=$msg;

		$mydate="$showyear-".twodigit($showmonth)."-".twodigit($showday);

		$sql="select SLOT_CODE from slots where `SLOT_DATE` = '$mydate' AND `SLOT_COUNT` <15 order by SLOT_CODE";

		$result=dosql($sql,1);
		if(mysql_num_rows($result))   // GO THROUGH SLOTS IF ANY EXIST
		{
			echo("Possible slots to cancel: <B>".mysql_num_rows($result)."</b><BR><BR>");  // HOW MANY SLOTS TO CHECK

			$blue="<font color=\"blue\">";
			$red="<font color=\"red\">";
			echo "<b>$blue Slot</font>.$red Group</font></b><BR>";

			$counter=1;
			$del_total=0;

			while ($row = mysql_fetch_assoc($result))  // GET CODES & DATA FROM SLOTS TABLE
			{
				$mycode=strtoupper($row["SLOT_CODE"]);

				$msg="";

				if((!strstr($mycode,"C"))&&(!strstr($mycode,"D")))  // IGNORE C & D SLOTS
				{

					$sql="select GROUP_CODE, GROUP_BIG, GROUP_SPECIAL_TYPE, GROUP_IDENT, GROUP_CONFIRMED, GROUP_BOOKPAYMENT from groups where GROUP_CODE='$mycode'  and GROUP_DELETED='N'  order by GROUP_CODE";

					$group_result=dosql($sql,0);

					$subcount=1;  												// COUNTER FOR GROUPS PER SLOT

					$del_count=mysql_num_rows($group_result);  // TOTAL GROUP COUNT PER SLOT

					if($del_count==0)
					{
						$msg="<BR>$blue<B>".$counter.".</font>$red".$subcount.".</b></font> $blue $mycode</font>";

						$emsg="\n(".$counter."/".$subcount.") | ".$mycode;

						$thismsg="  Empty slot found. This slot contains no groups.";

						echo $msg.$thismsg;
						$email_msg.=$emsg.$thismsg;

					}

					while ($group_row = mysql_fetch_assoc($group_result))  // GET CODES & DATA FROM GROUP TABLE
					{

						$msg="<BR>$blue<B>".$counter.".</font>$red".$subcount.".</b></font> $blue".$group_row["GROUP_CODE"]."</font>-$red".$group_row["GROUP_IDENT"]."</font>";  // LABEL GROUP
						$emsg="\n(".$counter."/".$subcount.") | ".$group_row["GROUP_CODE"]." | ".$group_row["GROUP_IDENT"];  // LABEL GROUP
						$thismsg="";

						if(($group_row["GROUP_CONFIRMED"]=="N")&&($group_row["GROUP_DELETED"]=="Y"))  // CHECK IS GROUP CONFIRMED  (I GUESS BECAUSE IT'S ALREADY BEEN CANCELLED.)
						{
							$thismsg=" is <B>not</b> confirmed and will not be cancelled.";
							echo $msg.$thismsg;
							$email_msg.=$emsg.$thismsg;

						}
						elseif($group_row["GROUP_BOOKPAYMENT"]=="costcenter")  // CHECK IS GROUP IS COSTCENTRE BOOKING
						{
							$thismsg=" is a costcentre booking and will not be cancelled.";
							echo $msg.$thismsg;
							$email_msg.=$emsg.$thismsg;

						}
						elseif($group_row["GROUP_BIG"]==1)  // CHECK IS GROUP IS PART OF A BIG GROUP
						{
							$thismsg=" is part of a big group and cannot be cancelled.";
							echo $msg.$thismsg;
							$email_msg.=$emsg.$thismsg;
						}
						elseif($group_row["GROUP_SPECIAL_TYPE"]!="")    // CHECK IF SPECIAL BOOKING
						{
							$thismsg="is a special ".$group_row["GROUP_SPECIAL_TYPE"]." reservation and cannot be cancelled.";
							echo $msg.$thismsg;
							$email_msg.=$emsg.$thismsg;
						}
						elseif(biggroups_in_slot($group_row["GROUP_CODE"]))		// CHECK IF ANOTHER GROUP IS IN THE SAME SLOT WHICH IS PART OF A BIG GROUP
						{
							$thismsg=" could be cancelled but this slot also contains a BIG group so it should not be cancelled.";
							echo $msg.$thismsg;
							$email_msg.=$emsg.$thismsg;
						}
						else		// OTHERWISE CANCEL GROUP
						{
							$thismsg=" meets the criteria and can be cancelled. <b>(".$subcount." of ".$del_count.")</b> ";

							echo $msg.$thismsg;

							$email_msg.=$emsg.$thismsg;

							cancel_small_group($group_row["GROUP_IDENT"],$delete,0);
							$del_total++;
						}

						$subcount++;

					}

				}
				else
				{
					$thismsg="<BR>$blue<b>$counter.</b>.$mycode</font> is an extra CD slot and cannot be cancelled.";
					$email_thismsg="($counter) $mycode is an extra CD slot and cannot be cancelled.";
					echo $thismsg;
					$email_msg.=$email_thismsg;

				}

			$counter++;
			}

			if($delete)
			{
				$thismsg="<BR><BR><b>$del_total groups have been cancelled.</b>";
				echo 	$thismsg;
				$email_msg.=$thismsg;
			}
			else
				echo "<BR><BR><b>$del_total groups would be cancelled.</b>";
		}
		else
		{
			$thismsg="There are no slots to cancel.";
			echo 	$thismsg;
			$email_msg.=$thismsg;
		}

		echo "<BR><BR><center><a href=\"$PHP_SELF?sc=$showyear$showmonth$showday\">Back</a></center>";


		if($cronjob)
		{

			echo(table(nl2br($email_msg)));

			$email_msg=str_replace("<BR>","\n",$email_msg);
			$email_msg=str_replace("<b>","",$email_msg);
			$email_msg=str_replace("</b>","",$email_msg);
			$email_msg.="\n\nEnd of cancellation report\n";

			mail("service@breweryvisits.com", $email_subject, $email_html_header.$email_msg, $email_headers);
			mail("dc7590@gmail.com", 'CRON'.$email_subject, $email_html_header.$email_msg, $email_headers);
		}

		finito();
		exit;
	}

	if (isset($sc))   // RETURN AFTER BOOKING
	{

		$showyear=substr($sc,0,4);
		$showmonth=substr($sc,4,2);
		$showday=substr($sc,6,2);
		$comm=adminshow;
	}


	echo "<BR>Today's Date is <b>".date("M-d-Y",$nowtime)."</b>";
	echo "<BR>Today + freeze period (less 1 day) is <b>".date("M-d-Y",$opentime)."</b>";



?>


<form method="GET" action="">
<input type=hidden name=comm value="cancelok">

<table>
<tr>
<td>

<?
if(!isset($showyear))
		$showyear=date("Y",$opentime);

	echo'<select name="showyear">';
	//echo (makeYearOptions($showyear));
	echo "<option value=\"$showyear\" $mysel>$showyear</option>\n";   // can only cancel in this year
	echo'</select>';

?>
</td>
<td>
<select name="showmonth">
<?

	if(!isset($showmonth))
		$showmonth=date("m",$opentime);


	for($loop=1;$loop<=12;$loop++)
	{
		if($loop==$showmonth)
			$mysel=" selected ";
		else
			$mysel="";

		echo "<option value=\"$loop\" $mysel>".$month_names[$loop-1]."</option>\n";
	}
?>
</select>
</td>
<td>
<?
	if(!isset($showday))
		$showday=date("d",$opentime);

?>

<select name="showday">
<!--<option value="all">All</option>-->

<?

	for($loop=1;$loop<=31;$loop++)
	{
		if($loop==$showday)
			$mysel=" selected ";
		else
			$mysel="";

		echo "<option value=\"$loop\" $mysel>$loop</option>\n";
	}

if(!isset($showcount))
	$showcount=0;


		if($usecal==1)
			$use=" selected ";
		else
			$ignore=" selected ";
?>
</select>
</td>
<td valign=top>
<input type="radio" name=delete value=0 checked>Show Groups<BR>
<input type="radio" name=delete value=1>Cancel Groups
<input type="submit" value="Show / Cancel">
</td>
</tr>
</table>
</form>

<BR>
<H1>Cancel single booking</h1>
<b>Note:</b> Group Ident is needed not Group Code<BR>
The Group Ident is a unique identifier containing Brewery, Date, Slot and <b>Group Number</b>.<BR>
The Group Code identifies the Brewery, Date and  Slot.<BR><BR>
<form method=POST>
<input type="hidden" name="comm" value="delone">
<input type="text" name="code" value="Code">
<input type="checkbox" name="sendmail" value="1"> Send email to user<BR><BR>
<input type="submit" value="Cancel this Booking">
</form>


