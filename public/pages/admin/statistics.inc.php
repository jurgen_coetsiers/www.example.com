<?

function show_rows($result,$th="",$cut="",$echo=0, $invoice=0)
{
	$rows="";


	$first=1;
	$odd=0;

	if(mysql_num_rows($result)>0)
		{
			$rows.= "<TABLE border=0>";


			while ($row = mysql_fetch_assoc($result))
			{


				// HEADERS-----------------------------------------
				if($first)
				{
					$rows.= "<TR valign=top>";

					if($th)
						$data=$th;
					else
						$data=$row;

					foreach ($data as $k=> $v)
					{
						$k=str_replace($cut,"",$k);

						if($th)
							$rows.= "<td class=\"adminslotheader\">".ucwords($v)."</td>";    // USE ARRAY VALUES GIVEN
						else
							$rows.= "<td class=\"adminslotheader\">$k</td>";    // DB FIELD NAMES
					}
					if($invoice)
						$rows.= "<td class=\"adminslotheader\">Invoice</td>";

					$rows.= "</TR>\n";
					$first=0;
				}
				// END OF HEADERS-----------------------------------------

				if($odd)
				{
					$rows.= "<TR bgcolor=\"#FFFFFF\">";
					$odd=0;
				}
				else
				{
					$rows.= "<TR bgcolor=\"#FFFFAA\">";
					$odd=1;
				}

				foreach ($row as $k=> $v)
				{
					if($v=="")
						$v="-";

					$rows.= "<td>$v</td>";
				}
				if($invoice)
					$rows.= "<td>$invoice.00</td>";

				$rows.= "</TR>\n";
			}
			$rows.= "</TABLE>";
		}
		else
			$rows.= "No rows to show.";

	if($echo)
		echo $rows;
	else
		return($rows);

}

//-----------------------------------------------------------
// INITIALISE VARIABLES
//-----------------------------------------------------------

	$th="";
	$by="";
	$orderby="";

	$db="";
	$criteria="*";

//-----------------------------------------------------------

	if($cat==1)
	{
		title("Statistics - Age, Language, Country Overviews");
		include("includes/admin_stat_form_overview.inc.php");
	}


	//-----------------------------------------------------------

	elseif($cat==2)
	{
		title("Statistics - Reservations to invoice");
		$th=array("nice date","start time","stop time"," name","e-mail"," size"," over 30","delta","special type","invoice name", "invoice street","invoice nr.","invoice city", "invoice zip","invoice vat","total amount&nbsp;&euro;" );
		$query=invoice_query();

		show_rows(dosql($query,$echo),$th,"_",0);

	}



	//-----------------------------------------------------------

	elseif($cat==3)
	{
		title("Statistics - Internal reservations to invoice (InBev)");
		$th=array("date","start","stop","name","email","count","big","specialdelta","specialtype","invoice name","invoice street","invoice hnum","invoice city","invoice zip","invoice vat ","CC center ","CC personfirst ","CC personlast ","CC affname ","CC name ","CC street ","CC hnum ","CC zip ","CC city ","CC country ","price");

		$query=special_query();


		show_rows(dosql($query,10),$th,"_",0);


	}


	//-----------------------------------------------------------
	elseif($cat==4)
	{
		title("Statistics - Site Income");
		include("includes/admin_stat_form_income.inc.php");

	}


//-----------------------------------------------------------


	elseif($cat=="result1")
	{
		include("statistics_dotype.inc.php");
		//echo $query;
		show_rows(dosql($query,1),$th,$cut,1);
		echo $js_closewin;

	}
//-----------------------------------------------------------

//-----------------------------------------------------------
	elseif($cat=="result4")
	{

		include("statistics_doincome.inc.php");

		$th=array("Cash","Maestro","Bancontact","Mastercard","Visa","Eurocard","Coupons","Coupons Total");

		$invoice_query=str_replace("SLOT_","GROUP_",$invoice_query);
		$invoice=getone($invoice_query,0);

		if(fromxs())
		{

			hr();
			echo "From XS only";
			hr();
			echo $query;
			hr();
			echo $invoice_query;
			hr();
		}

		show_rows(dosql($query,$echo),$th,$cut,1,$invoice);

		echo $js_closewin;
	}

//-----------------------------------------------------------
// SHOW MAIN MENU
//-----------------------------------------------------------
	else
	{
		title("Statistics");
		echo "<LI><a href=\"$PHP_SELF?cat=1\">Age, Language, Country Overviews</a><BR>";
		echo "<LI>Reservations to invoice</a>";
		//echo "<LI><a href=\"$PHP_SELF?cat=2\">Reservations to invoice</a>";

		$rnum=rand(1,300);
		echo "&nbsp;(<a target =\"_blank\"href=\"$PHP_SELF?cat=2&comm=export&rnum=$rnum\">Excel</a>)<BR>";

		//echo "<LI><a href=\"$PHP_SELF?cat=3\">Internal reservations to invoice (InBev)</a>";
		echo "<LI>Internal reservations to invoice (InBev)</a>";
		echo "&nbsp;(<a target =\"_blank\"href=\"$PHP_SELF?cat=3&comm=export\">Excel</a>)<BR>";

		echo "<LI><a href=\"$PHP_SELF?cat=4\">Site Income</a><BR>";

	}


?>