<?

title("Extra Reservation");

$group_ident=$slotcode;

	if(($close_admin_reservations)&&($_SERVER["REMOTE_ADDR"]!="195.243.238.234"))
	{
		echo "Updating admin pages.";
		finito();
	}


$d=substr($group_ident,7,2);
$m=substr($group_ident,5,2);
$y=substr($group_ident,1,4);
$CDslot=0;

if(strtoupper($slotcode[0])=="S")   // STUDENTCARD EXCEPTION FOR STELLA
		$TEXT["student"]="Studentcard";


if(!isset($bookstarttime))$_POST["bookstarttime"]="12:00";  // SET AS DEFAULT C & D SLOTS
if(!isset($bookstoptime))$_POST["bookstoptime"]="14:00";



if($reservation==1)
{

	$realbookcount=$bookcount;

	$emessage="";  // INITIALISE ERROR MESSAGE

	$em1="";
	$em2="";
	$em3="";
	$em4="";

	if($bookfname=="dc")
	{
		$bookfname="Darren";
		$booksname="Cooper";
		$bookstreet="Kaiser Friedrich Promenade";
		$bookhnum="89";
		$bookzip="61348";
		$bookcity="Bad Homburg";
		$bookemail="d.cooper@axess.de";
		$bookmobile="0171 12345";
		$bookgname="Test Group";
		$book_cost_center="dc";
		$book_cost_personfirst="dc";
		$book_cost_personlast="dc";
		$book_cost_affname="dc";
		$book_cost_name="dc";
		$book_cost_street="dc";
		$book_cost_hnum="dc";
		$book_cost_zip="dc";
		$book_cost_city="dc";
		//$booksenior=$bookcount;
		//$bookstarttime="12:00";
		//$bookstoptime="16:00";
	}
	$bookfname=str_replace("'",'',$bookfname);
    $booksname=str_replace("'",'',$booksname);
	$bookstreet=str_replace("'",'',$bookstreet);
	$bookcity=str_replace("'",'',$bookcity);
	$bookemail=str_replace("'",'',$bookemail);
	$bookgname=str_replace("'",'',$bookgname);

	if($bookfname=="")$em1.=gw(fname).", ";
	if($booksname=="")$em1.=gw(sname).", ";
	if($bookstreet=="")$em1.=gw(street).", ";
	if($bookhnum=="")$em1.=gw(num).", ";
	if($bookzip=="")$em1.=gw(zip).", ";
	if($bookcity=="")$em1.=gw(city).", ";
	if($bookemail=="")$em1.=gw(email).", ";
	if($bookmobile=="")$em1.=gw(mobile).", ";
	if($bookgname=="")$em1.=gw(gname).", ";




	if((strstr($slotcode,"C"))||(strstr($slotcode,"D")))   // ONLY CHECK IF SLOT C OR D
	{
		if($bookstarttime=="")$em1.="Starttime, ";
		if($bookstoptime=="")$em1.="Stoptime, ";
	}

	if($em1!="")
	{
		$em1[strlen(trim($em1))-1]=".";  			 // REMOVE LAST COMMA
		$em1="<b>Group:</b> $em1 <BR>";
	}


	/*-----------------------------------------
	// NOT MANDATORY FOR ADMIN
	/*-----------------------------------------
	if($book_payment=="invoice")
	{
		if($book_inv_name=="")$em2.="Company Name, ";
		if($book_inv_street=="")$em2.="Company Street, ";
		if($book_inv_hnum=="")$em2.="Company Nr, ";
		if($book_inv_zip=="")$em2.="Company Postcode, ";
		if($book_inv_city=="")$em2.="Company City, ";
		if($book_inv_vat=="")$em2.="Company VAT, ";

		if($em2!="")
		{
			$em2[strlen(trim($em2))-1]=".";  			 // REMOVE LAST COMMA
			$em2="<b>Invoice:</b> $em2 <BR>";
		}

	}


	if($book_cost_center=="")$em3.="Cost Center Number, ";
	if($book_cost_personfirst=="")$em3.="Cost Center Forename , ";
	if($book_cost_personlast=="")$em3.="Cost Center Name Surname, ";
	if($book_cost_affname=="")$em3.="Cost Center Affiliate Name, ";
	if($book_cost_name=="")$em3.="Cost Center Company, ";
	if($book_cost_street=="")$em3.="Cost Center Street, ";
	if($book_cost_hnum=="")$em3.="Cost Center House Num, ";
	if($book_cost_zip=="")$em3.="Cost Center Postcode, ";
	if($book_cost_city=="")$em3.="Cost Center City, ";

	if($em3!="")
	{
		$em3[strlen(trim($em3))-1]=".";	 // REMOVE LAST COMMA
		$em3="<b>Cost Center:</b> $em3 <BR>";
	}

	//----------------------------------------------------*/

	if(!isset($bookstudents))  // FOR BREWERIES OTHER THAN STELLA
		$bookstudents=0;

	$total=$bookjunior+$booksenior+$bookstudents;

	if($realbookcount<>$total)
		$em4.=gw(nomatch_members)." ($total/$realbookcount)";

	if($total==$bookjunior)
		$em4.=" ".gw(noadult);

	if(($bookemail!="")&&(!email_valid($bookemail))) 								  // SYNTAX CHECK EMAIL
	$em4.="<BR>Email syntax of <B>$bookemail</b> does not appear to be valid.";



	$emessage=$em1.$em2.$em3.$em4;  // CONCAT ALL MESSAGES

	if($emessage)
	{
		$emessage=gw(warn).gw(fields_required).$emessage;   // ADD MESSAGE
	}



	if(!$emessage)
	{
		echo "Saving Data<BR>";


		if($slotcodeAB!="") // BIG GROUP RESERVATION
		{

			$slotcode=substr(trim($slotcodeAB),0,strlen($slotcode)-1);

			$slotcodeA=$slotcode."A";
			$slotcodeB=$slotcode."B";

			if($reservation==1)
			{
				$slotcode=$slotcodeA;
				//$_POST["slotcode"]=$slotcodeA;
			}
			else
			{
				$slotcode=$slotcodeAB;
				//$_POST["slotcode"]=$slotcodeAB;
			}

			$origcount=$bookcount;
			$bookcount=$bookcount-30;
			$biggroup=1;
			$myslot="AB";
		}
		else
		{

			$myslot=$slotcode[strlen($slotcode)-1]; // GET A or B
			$slotcode=trim($slotcode.$nums); //?? NUMS
		}

		$comments="$starttime-$stoptime - ".$bookcomments;

		// WAS HERE FOR AB + CD BUT BMOVED DOWN
		//make_slot($slotcode, $booklang, 'guide', $comments,$bookstarttime,$bookstoptime,0);			// CHECK IF DATA EXISITS FOR THIS SLOT

		if(($myslot=="A")or($myslot=="B")or($myslot=="AB"))
		{

				//make_slot($slotcode, $booklang, 'guide', $comments,$bookstarttime,$bookstoptime,0);			// CHECK IF DATA EXISITS FOR THIS SLOT

				$group_ident=make_gid($slotcode);								// DETERMINE WHICH GROUP AND MAKE GROUP_IDENT ALSO CHECK IF IT ALREADY EXISTS

				// HAVE TO RE-INCLUDE TO GET BREWERY TIMES
				$key=strtoupper($slotcode[0]);  // BREWERY KEY

				$mybrewery=$brew_paths[$key];						// SET VAR TO BREWERY TO GET THE CORRECT TIMES FROM THE INCLUDE FILE
				include("includes/brewery_data.inc.php"); 	// GET BREWERY DATA

				$tslot="t".$group_ident[(strpos(strtolower($group_ident),"t")+1)];

				$starttime=$timeslots[$tslot][0];
				$stoptime=$timeslots[$tslot][1];

				make_slot($slotcode, $booklang, 'guide', $comments,$starttime,$stoptime,0);			// CHECK IF DATA EXISITS FOR THIS SLOT

				$mybrewery="admin";						// SET VAR BACK TO ADMIN IN CASE WE USE IT AGAIN

				$tmp=$_POST;  // SAVE POST VARS FOR LATER

				$myMD5pass=extra_add_group($group_ident,A,$biggroup,$starttime,$stoptime,0);							// CHECK IF SPACE IS STILL AVAILABLE AND INSERT GROUP

				$_SESSION['SSGROUP_GROUP_IDENT']=$group_ident;



				if($origcount>30)  // BOOK B  GROUP IS LARGER THAN 30
				{
					$_POST=$tmp;
					$_POST["slotcode"]=$slotcodeB;
					make_slot($slotcodeB, $booklang, 'guide', $comments,$starttime,$stoptime,0);		// CHECK IF DATA EXISITS FOR THIS SLOT
					$group_ident=make_gid($slotcodeB);								// DETERMINE WHICH GROUP AND MAKE GROUP_IDENT ALSO CHECK IF IT ALREADY EXISTS
					extra_add_group($group_ident,B,$biggroup,$starttime,$stoptime,0);			// CHECK IF SPACE IS STILL AVAILABLE AND INSERT GROUP
				}


		}
		else  // CD SLOT
		{
			make_slot($slotcode, $booklang, 'guide', $comments,$bookstarttime,$bookstoptime,0);			// CHECK IF DATA EXISITS FOR THIS SLOT
			$group_ident=make_gid($slotcode);

			$mailslot_start=$bookstarttime;   // STORE THE TIMES FOR THE EMAIL
			$mailslot_end=$bookstoptime;
			$myMD5pass=add_CD_group($group_ident,$slotcode,$slotcode,0,0);							// THIS VERSION SKIPS ALL THE NONSENSE

			$CDslot=1;


		}

		echo "<BR><a href=\"admin_showday.htm?showcount=$bookcount&sc=$slotcode\">Back to reservations</a>";

	//------------------------------------------------------------------------
	//------------------------------------------------------------------------
	// ADD MAIL GENERATOR


			//$gid=$_SESSION['SSGROUP_GROUP_IDENT'];
			//save_members($gid,0);

			$row=getonerow("select * from groups where GROUP_IDENT='$group_ident'",0);

			foreach($row as $k=>$v)
			{
				//echo "$k --->$v<BR>";
				$$k=$v;
			}

			// SHOW CONFIRM MESSAGE AND SEND CONFIRM LINK EMAIL
				echo(str_replace("<AX_EMAIL>",$GROUP_BOOKEMAIL,getword(please_confirm)));

				//$mailfile=strtolower($slotcode[0])."_".$mylang."_"."confirm";


				$mailfile=strtolower($slotcode[0])."_".strtolower($booklang)."_"."confirm";

				$mailbody=gettemplate($mailfile);




				// GET THE FIRST LINE OF THE MAILBODY TO USE AS SUBJECT
				$maillines=explode("\n",$mailbody,2);
				$mailsubject=$maillines[0];

				$mailbody=str_replace($mailsubject,"",$mailbody);

				//$mailsubject=getword(link_email_subject);
				//--------------------------------------------------------
				// -- GET VARS READY FOR EMAIL
				//--------------------------------------------------------
				$bookdate=substr($slotcode,7,2).".".substr($slotcode,5,2).".".substr($slotcode,1,4);
				$tslot="t".$group_ident[(strpos(strtolower($group_ident),"t")+1)];

				if($GROUP_SPECIAL_TYPE)
					$gsize=$_SESSION['SSGROUP_SPECIAL_COUNT'];
				else
					$gsize=$_SESSION['SSGROUP_COUNT'];

				// IF PAYMENT GOES VIA COST CENTER OR INVOICE, REMOVE "PAY UPON ARRIVAL" LINE FROM EMAIL TEXT

				if(($GROUP_SPECIAL_TYPE=="ITW")||($GROUP_BOOKPAYMENT=="invoice"))  // REMOVE PRICE INFORMATION
				{
					$mailbody=remove_prices($mailbody);
				}


				if($gsize==1)  // REMOVE PLURAL OF PEOPLE IF ONLY ONE PERSON
				{
					$mailbody=remove_plural($mailbody);
				}

				//--------------------------------------------------------
				// -- SWOP VARS IN EMAIL TEMPLATE
				//--------------------------------------------------------
				$mailbody=str_replace("<AX_DATE>",$bookdate,$mailbody);
				$blang=strtolower($booklang);

				if(!$CDslot)
				{
					$mailslot_start=$timeslots[$tslot][0];
					$mailslot_end=$timeslots[$tslot][1];

					if($gsize>14)
						$mailbody=str_replace("\n<AX_OPTION>","",$mailbody);
					else
						$mailbody=str_replace("<AX_OPTION>",getword2(email_optional,$blang),$mailbody);
				}
				else
					$mailbody=str_replace("\n<AX_OPTION>\n","",$mailbody);  // REMOVE OPTIONAL FOR CD SLOTS

				$mailbody=str_replace("<AX_TIME_START>",$mailslot_start,$mailbody);
				$mailbody=str_replace("<AX_TIME_END>",$mailslot_end,$mailbody);
				$mailbody=str_replace("<AX_GROUP_COUNT>",$gsize,$mailbody);
				$mailbody=str_replace("<AX_TOTAL_PRICE>",$GROUP_PRICE.".00",$mailbody);


				// MAKE CONFIRM LINK
				$mykey=strtoupper( $slotcode[0]);
				$realpath=$brew_paths[$mykey];


				// ADD CONFIRM LINK
				//$confirm_link="http://$HTTP_HOST/web/$mylang/$realpath/mybooking.html?ID=$myMD5pass";

				$confirm_link="http://$HTTP_HOST/web/$mylang/$realpath/mybooking.html?ID=".code_id("$GROUP_ID");
				$confirm_link=shrink_link($confirm_link);



			if($CDslot)
				$mailbody=str_replace("<AX_CONFIRMLINK>\n","",$mailbody);  // NO LINK NEEDED, AUTO CONFIRM FOR CD SLOTS
			else
				$mailbody=str_replace("<AX_CONFIRMLINK>",getword2(email_link_conf,$blang,nl).$confirm_link."\n".getword2(email_link_del,$blang,nl),$mailbody);

			if(xs())
				xs(mail);


			if($CDslot)
			{
				mail("service@breweryvisits.com", "CD Booking".$mailsubject, $mailbody, $email_headers);
				echo "<BR>CD Slot <b>saved and confirmed</b>, mail sent to <b>service@breweryvisits.com</b><BR>";
			}
			else
			{
				mail($GROUP_BOOKEMAIL, $mailsubject, $mailbody, $email_headers);
				mail("d.cooper@axess.de", "AB Booking".$mailsubject, $mailbody, $email_headers);
			}


	//------------------------------------------------------------------------
	//------------------------------------------------------------------------

			if(fromxs())
			{

				echo '<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>';
				echo "CDslot value set to $CDslot <BR>";
				echo' <table border="1" cellpadding="0" cellspacing="0" bgcolor="#c8c8c8"><TR><TD><B>Visible for XS only:</b></td></tr>';
				echo "<a href=\"$confirm_link\" target=\"_blank\">$confirm_link</a><BR>";
				echo "<TR><TD>gid=$group_ident<BR>";
				echo 	nl2br($mailbody);
				echo "</TD></TR><TR><TD>";
				echo "</td></tr></table>";
			}

	}
	else  // SHOW ERROR MESSAGE AND FORM
	{
		echo $emessage;
		getTemplate(extra_booking_form,1,1);
	}
}

else // SHOW EMPTY FORM
{
		global $SLOTCODE;

		$SLOTCODE=$slotcode;
		getTemplate(extra_booking_form,1,1);
}



?>