<?
	title("Reset System Databases");

	if(($comm=="delOK")&&($sure=="on"))
	{
			echo "Databases SLOTS and GROUPS have been reset.<BR>";
			dosql("delete from slots",1);
			dosql("delete from groups",1);
			//dosql("delete from members",1);
			finito();
	}
	else
	{
		echo(gw(warn)."Are you sure you want to reset the following table from <b>$dbname</b> and loose all data: <b>SLOTS</b> and <b>GROUPS</b> databases?<BR>");
		echo "<BR>The <b>USERS</b> and <b>DAYS</b> databases will remain untouched.<BR>";
		echo "<form method=\"POST\">";
		echo "<input type=\"hidden\" name=\"comm\" value=\"delOK\">";
		echo "<input type=\"hidden\" name=\"ID\" value=\"$ID\">";
		echo "<input name=\"sure\" type=checkbox> Yes please reset the system databases loosing all the data contained within.<BR><BR>";
		echo "<input type=submit value=\"Reset System Databases\">";
		echo "</form>";
	}