<H1>Reservations Overview Module</h1>
<b>Please select Brewery and Date to view.<BR></b>
<form method="GET" action="admin_showday.htm">
<input type=hidden name=comm value="adminshow">
<table><tr><th>Brewery</th><th>Year</th><th>Month</th><th>Day</th><th>Calendar</th><th>Override</th><th>Groupsize</th><th>&nbsp;</th></tr>
<tr><td>
<?
	if (isset($sc))   // RETURN AFTER BOOKING
	{
		$showbrew=$sc[0];
		$showyear=substr($sc,1,4);
		$showmonth=substr($sc,5,2);
		$showday=substr($sc,7,2);
		$comm=adminshow;

	}


if(!isset($showyear))
	$showyear=date("Y");

?>
<select name="showbrew">
<option value="S"<?if($showbrew=="S") echo "selected"?>>Stella</option>
<option value="J"<?if($showbrew=="J") echo "selected"?>>Jupiler</option>
<option value="B"<?if($showbrew=="B") echo "selected"?>>Belle-Vue</option>
<option value="H"<?if($showbrew=="H") echo "selected"?>>Hoegaarden</option>
</select>
</td><td>

<select name="showyear">
<? echo (makeYearOptions($showyear));?></select>
</td><td>

<select name="showmonth">
<?


	if(!isset($showmonth))
		$showmonth=date("m");


	for($loop=1;$loop<=12;$loop++)
	{
		if($loop==$showmonth)
			$mysel=" selected ";
		else
			$mysel="";

		echo "<option value=\"$loop\" $mysel>".$month_names[$loop-1]."</option>\n";
	}
?>
</select>
</td>
<td>
<?
	if(!isset($showday))
		$showday=date("d");

?>

<select name="showday">
<!--<option value="all">All</option>-->

<?

	for($loop=1;$loop<=31;$loop++)
	{
		if($loop==$showday)
			$mysel=" selected ";
		else
			$mysel="";

		echo "<option value=\"$loop\" $mysel>$loop</option>\n";
	}

if(!isset($showcount))
	$showcount=0;


		if($usecal==1)
			$use=" selected ";
		else
			$ignore=" selected ";
?>
</select>

</td>
<td>
	<select name="usecal">
	<option value=1 <?=$use?>>Use Calendar</option>
	<option value=0 <?=$ignore?>>Ignore Calendar</option>
	</select>
</td>
<td>
	<?
		if($override)
			$ov="checked";
		else
			$ov="";
	?>

	<input type=checkbox name=override value=1 <?=$ov?>>
</td>

<td>
<input  name="showcount" onkeydown="f(this)" onkeyup="f(this)" onblur="f(this)" onclick="f(this)"  maxlength="3"  style="width: 30px" value="<?=$showcount?>"><BR>

</td>
<td>
<input type="submit" value="Show">
</td>
</tr>
</table>
</form>




<?


	if($comm=="adminshow")
	{

		echo "<h1>Bookings for ".$brew_names[$showbrew]." - ".$month_names[$showmonth-1]." $showday $showyear</h1>";

		$y=date("Y");


//-----------------------------------------------------------
/*
		if($showday=="all")
			$admincode=$showbrew.$showyear.twodigit($showmonth);
		else
			$admincode=$showbrew.$showyear.twodigit($showmonth).twodigit($showday);

		echo "<table border=1>";

		$result=dosql("select * from slots where SLOT_CODE like '$admincode%'",1);

		echo "<TR>";
		for($loop=0; $loop<mysql_num_fields($result);$loop++)
			echo ("<TH>".mysql_field_name($result,$loop)."</TH>");
		echo "</TR>";

		while ($row = mysql_fetch_row($result))
		{
			echo "<tr>";

			foreach($row as $k=>$v)
			{
				echo "<td>$v</td>";
			}
			echo "</tr>";

		}
		echo "</table>";

*/
//-----------------------------------------------------------

	//-----------------------------------------------
	// SET UP ALL THE VARIABLES WE NEED TO SHOW DAY
	//-----------------------------------------------



		$m=twodigit($showmonth);
		$d=twodigit($showday);


		$mydate=$showyear.$m.$d;
		$infoecho="5";


		//echo "$y.$m.$d";

	//---------------------
	//----NOW SHOW THE DAY
	//---------------------


	$mybrewery=$brew_paths[$showbrew];
	$myBrewery=$mybrewery;

	include("includes/brewery_data.inc.php");  // GET BREWERY DATA


		if(!$yeardays[$m][$d])
		{
			$day_open=0;
			echo gw(warn)."Brewery is not officially open on this day.<BR>";
		}
		else
			$day_open=1;


		if(!$usecal)  // OVERRIDES CALENDAR RESTRICTIONS
			$day_open=1;






	list($y, $m, $d) =explode_date($mydate);


	$_SESSION['SSGROUP_COUNT']=$showcount;


	$lastminute=0;

	//---------------------------------------------------
	// CHECK IF SPECIAL ARRANGEMENT
	//---------------------------------------------------
	$spec=special();
	$spec_slot="";

	if($spec)
	{
		echo "<b>$spec Special </b><BR>";

		if($_SESSION['SSGROUP_COUNT']==30)
			$spec_slot="(require 1 slot)";
		else
			$spec_slot="(require 2 slots)";

		$lookingfor=$_SESSION['SSGROUP_SPECIAL_COUNT'];
	}
	else
		$lookingfor=$_SESSION['SSGROUP_COUNT'];


	echo "(".gw(freeslots).gw(onthe)."$d.$m.$y".gw(forr).$lookingfor.gw(people)."&nbsp;$spec_slot)<BR>";
	echo(getword(optional)."<BR><BR>");

	$daytable=getTemplate(admin_table_day);
	$dayrow_clean=getTemplate(admin_table_day_row);

	$slots="";
	$bookings_today=0;
	$maxslots=count($timeslots);

	$possible=array();
	$possible=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$possibleA=array();
	$possibleA=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$possibleB=array();
	$possibleB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE


	$possibleAB=array();
	$possibleAB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$langAB=array();
	$langAB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$infoAB=array();
	$infoAB=initarray($maxslots,0);  // START WITH 0 SLOTS POSSIBLE

	$bookingsA_today=0;
	$bookingsB_today=0;

	$SLOTA_TABLE="";
	$SLOTB_TABLE="";


	$Alabel="Slot A";
	$Blabel="Slot B";

	$statusA="-";
	$statusB="-";

	$NL=0;
	$FR=0;
	$EN=0;



	//------------------------------------------------------------------
	// END OF PART 1
	//------------------------------------------------------------------
	// NOW GET INFO ABOUT A & B SLOTS
	//------------------------------------------------------------------


	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	{

		//-----MAKE SLOT CODES A & B
		$slotcode=$myBrewery[0].$mydate."t".$slotcount;
		$slotcodeA=$slotcode."A";
		$slotcodeB=$slotcode."B";

		//echo "k=$k<BR>";

		//---------------------------------------
		//-----AND GET INFO ABOUT THAT A SLOT ---
		//---------------------------------------

		$slots[$slotcodeA]=getonerow ("select * from slots where SLOT_CODE='$slotcodeA'",0);

		//-----------------------------------------------------------------------------------
		//-----IF SLOT HAS DETAILS THEN OPEN NEIGHBOUR SLOTS UNLESS WE ARE IN LASTMINUTE MODE
		//-----------------------------------------------------------------------------------
		if(!empty($slots[$slotcodeA]))
		{
			if(!$lastminute)   // ONLY OPEN SLOTS WHERE SPACES ARE AVAILABLE AND CONFIRMED
			{
				$possibleA[$slotcount]=1;

				$possibleA[$slotcount-1]=1;  //ACTIVATE NEIGHBOURS IF RESERVATION EXISTS, EVEN IF IT IS STILL OPTIONS, i.e. LESS THAN 15 PEOPLE
				$possibleA[$slotcount+1]=1;

				$bookingsA_today++;
				$slots[$slotcodeA][PEOPLE_SOFAR]=slot_count($slotcodeA,0);//HOW MANY PEOPLE ??
			}
			else
			{
				if(slot_count($slotcodeA)>=$MINCONF)
				{
					$possibleA[$slotcount]=1;
					$bookingsA_today++;
					$slots[$slotcodeA][PEOPLE_SOFAR]=slot_count($slotcodeA,0);   //HOW MANY PEOPLE ??
				}
			}
		}

		//---------------------------------------
		//-----AND GET INFO ABOUT THAT B SLOT ---
		//---------------------------------------

		$slots[$slotcodeB]=getonerow ("select * from slots where SLOT_CODE='$slotcodeB'",0);

		//-----------------------------------------------------------------------------------
		//-----IF SLOT HAS DETAILS THEN OPEN NEIGHBOUR SLOTS UNLESS WE ARE IN LASTMINUTE MODE
		//-----------------------------------------------------------------------------------

		if(!empty($slots[$slotcodeB]))
		{
			if(!$lastminute)   // ONLY OPEN SLOTS WHERE SPACES ARE AVAILABLE AND CONFIRMED
			{

			$possibleB[$slotcount]=1;

			$possibleB[$slotcount-1]=1;  			//ACTIVATE NEIGHBOURS IF RESERVATION EXISTS, EVEN IF IT IS STILL OPTIONS, i.e. LESS THAN 15 PEOPLE
			$possibleB[$slotcount+1]=1;
			$bookingsB_today++;
			$slots[$slotcodeB][PEOPLE_SOFAR]=slot_count($slotcodeB,0);//HOW MANY PEOPLE ??
		}
		else
			{
				if(slot_count($slotcodeB)>=$MINCONF)
				{
					$possibleB[$slotcount]=1;
					$bookingsB_today++;
					$slots[$slotcodeB][PEOPLE_SOFAR]=slot_count($slotcodeB,0);   //HOW MANY PEOPLE ??
				}
			}
		}

		$slotcount++;
	}

	$infoA="Slot A has $bookingsA_today of $maxslots bookings - ";
	$infoB="Slot B has $bookingsB_today of $maxslots bookings - ";

	//-------------------------------------
	//-----IF WE HAVE NO BOOKINGS THEN ALL SLOT ARE POSSIBLE-----
	//-------------------------------------
	if($bookingsA_today==0)
	{

		//-------------------------------------
		// UNLESS OF COURSE WE ARE NOT OPEN ON THIS DAY  I ASSUME IF WE CAN'T OPEN A THEN B IS SAFE
		//-------------------------------------

		//$dow=date("w",mktime(0,0,0,$m,$d,$y));



		//if($dow==0)  // SUN IN DATE(W) HAS 0 AND I HAVE MON AS 0 SO THIS IS THE TRANSLATION - MIGHT BE ABLE TO ELEGANTLY MOD THIS SOMEHOW
		//	$dow=6;
		//else
		//	$dow--;

		//if(!$mydays[$dow])
		//	$possibleA=initarray($maxslots,0);
		//else
		//{
		//	if(!$lastminute)    // OPEN ALL BECAUSE NONE SET UNLESS WE ARE LAST MINUTE
		//	{
		//		unset($possibleA);
				$possibleA=initarray($maxslots,1);

				if(!$day_open)
					$possibleA=initarray($maxslots,0);
		//	}

		//}
	}




	if($bookingsB_today==0)
	{
		if(!$lastminute)  // OPEN ALL BECAUSE NONE SET UNLESS WE ARE LAST MINUTE
		{
			unset($possibleB);
			$possibleB=initarray($maxslots,1);
		}
	}



	// IF BREWERY IS CLOSED SHUT B TOO

	if(!$day_open)
		$possibleB=initarray($maxslots,0);


	// FOR MORE THAN 30 PEOPLE, B MUST BE EMTPY AND A HAVE AT LEAST AS MANY SPACES AS THE GROUP > 30 IS


	//----------------------------------------------------------
	// ----- THIS SECTION DEALS WITH BIG GROUPS-----------------
	//----------------------------------------------------------

	//echo "BIG GROUPS CHECK";
	//showpost();


	//if($_SESSION['SSGROUP_COUNT']>30)



	$SLOTAB_TABLE="";

	if($showcount>30)
	{
		$_SESSION['SSGROUP_COUNT']=$showcount;

		echo "Checking for slots available in A & B.<BR>";

		$remainder=$_SESSION['SSGROUP_COUNT']-30;

		$slotcount=1;
		foreach ($timeslots as $k=>$timeslot)
		{

			//-----MAKE SLOT CODES A & B
			$slotcode=$myBrewery[0].$mydate."t".$slotcount;
			$slotcodeA=$slotcode."A";
			$slotcodeB=$slotcode."B";

			$acount=$slots[$slotcodeA][PEOPLE_SOFAR];
			$bcount=$slots[$slotcodeB][PEOPLE_SOFAR];

			$booklink="<a class=\"dayopen\" href=\"admin_reservation.htm?slotcodeAB=".strtoupper($slotcodeA);

			$langAB[$slotcount]="";
			$infoAB[$slotcount].="";

			if($bcount!=0)
				$infoAB[$slotcount].="slot B is not available";

			if(($acount+$remainder)>31)
				$infoAB[$slotcount].=" people in A $acount + (group-30) $remainder, is too big";


			if(($bcount==0)&&(($acount+$remainder)<31))
			{
				$ABcount=$acount+$remainder;

				if($acount==0)
				{
					$infoAB[$slotcount]="t$slotcount available, A & B are empty. Lang=ANY";
					//$langAB[$slotcount]="ANY";


					if($possibleA[$slotcount]==1)
					{
						$langAB[$slotcount]=$booklink."&setlang=nl\">".$nl_flag."</a>&nbsp;";
						$langAB[$slotcount].=$booklink."&setlang=fr\">".$fr_flag."</a>&nbsp;";
						$langAB[$slotcount].=$booklink."&setlang=en\">".$en_flag."</a>&nbsp;";
					}
					else
						$langAB[$slotcount]="";


				}
				else
				{
					$langAB[$slotcount]=$booklink."&setlang=".$slots[$slotcodeA][SLOT_LANG]."\">".getword($slots[$slotcodeA][SLOT_LANG])."</a>&nbsp;";
					$infoAB[$slotcount]="t$slotcount possible Slot B is empty and  A=$acount + Rest of group=$remainder ($ABcount) is smaller than or equal to 30. But Lang is restricted.";
				}




				// NOW TEST IF IT IS A SLOT IS POSSIBLE TO ENSURE SEQUENTIAL BOOKINGS



				if(($possibleA[$slotcount]==1)||($bookingsA_today==0))
				{
					$possibleAB[$slotcount]=1;
				}
				else
				{
					$possibleAB[$slotcount]=A;
					$infoAB[$slotcount].=" But this A slot is not possible or not open.";
					$langAB[$slotcount].="";
				}

			}
			else
			{
				//echo "t$slotcount NOT possible.<BR>";
				$possibleAB[$slotcount]=0;
			}
			$slotcount++;
		}


	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	//foreach($possibleAB as $k=>$v)
	{
		//echo "t$slotcount possible=".$possibleAB[$slotcount]."  Lang=".$langAB[$slotcount];
		$ainfo=info($infoAB[$slotcount],0,1);

		if($possibleAB[$slotcount]==1)
			$slot_status="slotopen";
		else
			$slot_status="slotclosed";


		$dayrow=str_replace("AX_DAYCLASS","class=\"$slot_status\"",$dayrow_clean);

		$dayrow=str_replace("<AX_SLOTLINK>",$langAB[$slotcount] ,$dayrow);

		$dayrow=str_replace("<AX_TIMESLOT_START>",$timeslot[0],$dayrow);
		$dayrow=str_replace("<AX_TIMESLOT_STOP>",$timeslot[1],$dayrow);
		$dayrow=str_replace("<AX_DEBUG>",$ainfo ,$dayrow);
		$dayrows.=$dayrow;

		$slotcount++;
	}


	$SLOTAB_TABLE=str_replace("<AX_TIMESLOTS>",$dayrows,$daytable);
//	echo $SLOTAB_TABLE;

}
else
{
	//-------------------------------------
	//-----NOW WORK OUT IF WE CAN OPEN SLOT B
	//-------------------------------------
	//1. ARE ALL OF SLOTS A FULL OR IS IT LAST MINUTE
	//-------------------------------------


	if(($bookingsA_today==$maxslots)||($lastminute)||isloggedin())
	{
		$statusA="";
		$statusA.="Status: A has bookings in all slots";
		$statusA.=" Checking to see if the lang is available for the group size.";


		// GET LANGS HERE BUT I THINK WE CAN MOVE THIS UP LATER.
		$slotcount=1;

		foreach ($timeslots as $k=>$timeslot)
		{
				$slotcode=$myBrewery[0].$mydate."t".$slotcount;
				$slotcodeA=$slotcode."A";

				//echo ("TOTAL=".$slots[$slotcodeA][PEOPLE_SOFAR]."+".$_SESSION['SSGROUP_COUNT']."<BR>");

				$new_group_total=$slots[$slotcodeA][PEOPLE_SOFAR]+$_SESSION['SSGROUP_COUNT'];

				//---ONLY IF LANG IS NOT POSSIBLE IN SLOT A

				if($new_group_total<=30)
					$$slots[$slotcodeA][SLOT_LANG]++;  // BEWARE, IT CREATES THE VARS EN,FR,NL
				//else
				//	echo "<BR><B>t".$slotcount." $slotcodeA people in group (".$slots[$slotcodeA][PEOPLE_SOFAR].") + groupcount (".$_SESSION['SSGROUP_COUNT'].")= ($new_group_total) too big.</b>";

				//echo $slots[$slotcodeA][SLOT_LANG];


			$slotcount++;
		}

		//echo "nl=$NL<BR>";
		//echo "fr=$FR<BR>";
		//echo "en=$EN<BR>";

		$Bsearch="";
		if($NL==0)
			$Bsearch.=" Checking Slot B for Dutch spaces.";
		if($FR==0)
			$Bsearch.=" Checking Slot B for French spaces.";
		if($EN==0)
			$Bsearch.=" Checking Slot B for English spaces.";


		//----------------------------------------------------
		//LET'S JUST SHOW SLOT B
		//----------------------------------------------------
		$slotcount=1;

		foreach ($timeslots as $k=>$timeslot)
		{
			$slotcode=$myBrewery[0].$mydate."t".$slotcount;
			$slotcodeB=$slotcode."B";
		}



		//----------------------------------------------------
	}
	else
	{
		$infoA.="Status: A Still has spaces.";
		$EN=A;
		$NL=A;
		$FR=A;
	}



	//-----CHECK SLOT A COLOURS-----
	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	{
		$slotcode=$myBrewery[0].$mydate."t".$slotcount;
		$slotcodeA=$slotcode."A";
		$slotcodeB=$slotcode."B";

		$spaces_left=0;
		$peoplecountA=0;
		$peoplecountB=0;

		//get slot info  //CHECK FOR SPACES AVAILABLE

		$lang="";
		//echo "$slotcount => $possible[$slotcount]<BR>";
		//	$peoplecount=slot_count($slotcode);//HOW MANY PEOPLE ??

		$peoplecountA=slot_count($slotcodeA);//HOW MANY PEOPLE ??
		$peoplecountB=slot_count($slotcodeB);//HOW MANY PEOPLE ??

		$new_group_total=$slots[$slotcodeA][PEOPLE_SOFAR]+$_SESSION['SSGROUP_COUNT'];


		if(!$possibleA[$slotcount])
		{
			$slot_status="slotclosed";
			$lang="";
			$message="Slot is not yet open.";
		}
		elseif($new_group_total>30)
		{
			$spaces_left=$maxgroupsize-$peoplecountA;
			$slot_status="slotfull";
			$lang=$slots[$slotcodeA]["SLOT_LANG"];
			$message="+".$_SESSION['SSGROUP_COUNT']."=$new_group_total Not possible, not enough room in slot. (NOT ENOUGH SPACES)";
		}
		elseif($peoplecountA==30)
		{
			$slot_status="slotfull";
			$message="Slot is fully booked (SLOT FULL)";
		}
		else
		{
			if($new_group_total<15)
			{
				$slot_status="adminslotoption";
				$optional="*";
			}
			else
			{
				if($lastminute)
					$slot_status="slotlastminute";
				else
					$slot_status="slotopen";

				$optional="";
			}

			$booklink="<a class=\"dayopen\" href=\"admin_reservation.htm?slotcode=".strtoupper($slotcodeA);


			if(!empty($slots[$slotcodeA]))  // IF OPEN GET LANGUAGE IF DEFINED
			{
				$lang=$slots[$slotcodeA]["SLOT_LANG"];
				$lang=$optional.$booklink."\">".getword($lang)."</a>";

				$spaces_left=$maxgroupsize-$peoplecountA;
				$message="Slot is open and has a booking, $spaces_left spaces ($maxgroupsize-$peoplecountA) are available in ".$slots[$slotcodeA]["SLOT_LANG"].".";

			}
			else
			{

				$message="Slot is open. 30 spaces are available in all languages";
				//$lang=$allflags;
				$lang="$optional";
				$lang.=$booklink."&setlang=nl\">".$nl_flag."</a>&nbsp;";
				$lang.=$booklink."&setlang=fr\">".$fr_flag."</a>&nbsp;";
				$lang.=$booklink."&setlang=en\">".$en_flag."</a>&nbsp;";

				//$lang=$allflags;
			}



			if($showcount==0)  // BOOKING NOT POSSIBLE IF 0 GROUPSIZE
				$lang=str_replace("href","",$lang);
		}
		$dayrow=str_replace("AX_DAYCLASS","class=\"$slot_status\"",$dayrow_clean);
		$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);

		$gcountA=groups_in_slot($slotcodeA);

		$dayrow=str_replace("<AX_TIMESLOT_START>",$timeslot[0],$dayrow);
		$dayrow=str_replace("<AX_TIMESLOT_STOP>",$timeslot[1],$dayrow);

		if($override)
		{
			$over_link="<a href=\"admin_reservation.htm?slotcode=$slotcodeA";

			$over_nl=$over_link."&setlang=nl\">".$nl_flag."</a>&nbsp;";
			$over_fr=$over_link."&setlang=fr\">".$fr_flag."</a>&nbsp;";
			$over_en=$over_link."&setlang=en\">".$en_flag."</a>&nbsp;";

			if((slot_count($slotcodeA)==0)&&($showcount==30)) // OVERRIDE IS ONLY POSSIBLE IF THE SLOT IS EMPTY AND GROUPSIZE IS NOT 0
				$override_link="</td ><td class=\"adminheader\">".$over_nl.$over_fr.$over_en;
			else
				$override_link="</td ><td class=\"adminheader\">&nbsp";


			if(!strstr($daytable,"Override"))  // ONLY REPLACE HEADER ONCE
				$daytable=str_replace("Code","Code</th><th>Override",$daytable);

		}
		else
			$override_link="";






		$dayrow=str_replace("<AX_SLOTCODE>",$slotcodeA.$override_link."</a>",$dayrow);

		$dayrow=str_replace("<AX_SLOTCOUNT>",$peoplecountA,$dayrow);
		$dayrow=str_replace("<AX_GROUPCOUNT>",$gcountA,$dayrow);
		$dayrow=str_replace("<AX_SLOTSPACES>",$spaces_left,$dayrow);
		$dayrow=str_replace("<AX_GUIDE>","Pascal",$dayrow);
		$dayrow=str_replace("<AX_DEBUG>",info($slotcodeA."->".$peoplecountA." people in slot, $gcountA groups in slot, ".$message,0,1) ,$dayrow);

		$dayrows.=$dayrow;
		$slotcount++;
	}

	//echo "</td><td>";

	$SLOTA_TABLE=str_replace("<AX_TIMESLOTS>",$dayrows,$daytable);




//-------------------------------------
//----SLOT B
//-------------------------------------

//echo "nl=$NL<BR>";
//echo "fr=$FR<BR>";
//echo "en=$EN<BR>";


if(($NL=="0")||($FR=="0")||($EN=="0")||isloggedin())   // THIS OPENS SLOT B FOR LOGGED IN USERS!!
{

	//$dayrow_clean=getTemplate(admin_table_day_rowB);
	$dayrow_clean=getTemplate(admin_table_day_row);
	$dayrow="";
	$dayrows="";
	$statusB="";
	$statusB.="Status: B, Slot B can be opened.";
	$statusB.=$Bsearch;

	//-----CHECK SLOT B COLOURS-----
	$slotcount=1;
	foreach ($timeslots as $k=>$timeslot)
	{
		$slotcode=$myBrewery[0].$mydate."t".$slotcount;
		$slotcodeA=$slotcode."A";
		$slotcodeB=$slotcode."B";

		$spaces_left=0;
		$peoplecountA=0;
		$peoplecountB=0;

		//get slot info  //CHECK FOR SPACES AVAILABLE

		$lang="";
		//echo "$slotcount => $possible[$slotcount]<BR>";
		//	$peoplecount=slot_count($slotcode);//HOW MANY PEOPLE ??

		$peoplecountA=slot_count($slotcodeA);//HOW MANY PEOPLE ??
		$peoplecountB=slot_count($slotcodeB);//HOW MANY PEOPLE ??

		$new_group_total=$slots[$slotcodeB][PEOPLE_SOFAR]+$_SESSION['SSGROUP_COUNT'];



		if(!$possibleB[$slotcount])
		{
			$slot_status="slotclosed";
			$lang="";
			$message="Slot is not yet open.";
		}
		elseif($new_group_total>30)
		{
			$slot_status="slotfull";
			$message="+".$_SESSION['SSGROUP_COUNT']."=$new_group_total Not possible, not enough room in slot.";
			$spaces_left=$maxgroupsize-$peoplecountB;
			$lang=$slots[$slotcodeB]["SLOT_LANG"];
		}
		elseif($peoplecountB==30)
		{
			$slot_status="slotfull";
			$message="Slot is fully booked";
		}
		else
		{
			if($new_group_total<15)
			{
				$slot_status="adminslotoption";
				$optional="*";
			}
			else
			{
				if($lastminute)
					$slot_status="slotlastminute";
				else
					$slot_status="slotopen";

				$optional="";
			}

			$booklink="<a class=\"dayopen\" href=\"admin_reservation.htm?slotcode=".strtoupper($slotcodeB);

			if(!empty($slots[$slotcodeB]))  // IF OPEN GET LANGUAGE IF DEFINED
			{
				$lang=$slots[$slotcodeB]["SLOT_LANG"];

				//$lang=$booklink."\"><img src=\"/images/".strtolower($lang).".gif\" alt=\"".getword($lang)."\" border=0>";
				$lang=$optional.$booklink."\">".getword($lang)."</a>";

				$spaces_left=$maxgroupsize-$peoplecountB;
				$message="Slot is open and has a booking, $spaces_left spaces ($maxgroupsize-$peoplecountA) are available in ".$slots[$slotcodeB]["SLOT_LANG"].".";

			}
			else
			{
				$message="Slot is open. 30 spaces are available in all languages";
				//$lang=$allflags;

				$lang="$optional";


				if($NL==0)
					$lang.=$booklink."&setlang=nl\">".$nl_flag."</a>&nbsp;";

				if($FR==0)
					$lang.=$booklink."&setlang=fr\">".$fr_flag."</a>&nbsp;";

				if($EN==0)
					$lang.=$booklink."&setlang=en\">".$en_flag."</a>&nbsp;";


				//$lang=$allflags;

			}
			if($showcount==0)  // BOOKING NOT POSSIBLE IF 0 GROUPSIZE
				$lang=str_replace("href","",$lang);
		}

		$dayrow=str_replace("AX_DAYCLASS","class=\"$slot_status\"",$dayrow_clean);

		if($slot_status=="slotopen")
		{
			$booklink=$booklink."\">BOOK</a>";
			//$dayrow=str_replace("<AX_SLOTLINK>",$booklink." | ".$slotcode." | ".$lang ,$dayrow);
			$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);
		}
		else
			$dayrow=str_replace("<AX_SLOTLINK>","$lang" ,$dayrow);


		$gcountB=groups_in_slot($slotcodeB);


		$dayrow=str_replace("<AX_TIMESLOT_START>",$timeslot[0],$dayrow);
		$dayrow=str_replace("<AX_TIMESLOT_STOP>",$timeslot[1],$dayrow);


		if($override)
		{
			$over_link="<a href=\"admin_reservation.htm?slotcode=$slotcodeB";

			$over_nl=$over_link."&setlang=nl\">".$nl_flag."</a>&nbsp;";
			$over_fr=$over_link."&setlang=fr\">".$fr_flag."</a>&nbsp;";
			$over_en=$over_link."&setlang=en\">".$en_flag."</a>&nbsp;";

			if((slot_count($slotcodeB)==0)&&($showcount==30)) // OVERRIDE IS ONLY POSSIBLE IF THE SLOT IS EMPTY AND GROUPSIZE IS NOT 30
				$override_link="</td ><td class=\"adminheader\">".$over_nl.$over_fr.$over_en;
			else
				$override_link="</td ><td class=\"adminheader\">&nbsp";


			if(!strstr($daytable,"Override"))  // ONLY REPLACE HEADER ONCE
				$daytable=str_replace("Code","Code</th><th>Override",$daytable);

		}
		else
			$override_link="";





		$dayrow=str_replace("<AX_SLOTCODE>",$slotcodeB.$override_link."</a>",$dayrow);
		$dayrow=str_replace("<AX_SLOTCOUNT>",$peoplecountB,$dayrow);
		$dayrow=str_replace("<AX_GROUPCOUNT>",$gcountB,$dayrow);
		$dayrow=str_replace("<AX_SLOTSPACES>",$spaces_left,$dayrow);
		$dayrow=str_replace("<AX_GUIDE>","Pascal",$dayrow);
		$dayrow=str_replace("<AX_DEBUG>",info($slotcodeB.$peoplecountB." people in slot, $gcountB groups in slot, ".$message,0,1) ,$dayrow);

		//$dayrow=str_replace("<AX_DEBUG>",info($slotcodeB."</b><br>".$peoplecountB." people in slot, $gcountB groups in slot, ".$message) ,$dayrow);

		$dayrows.=$dayrow;
		$slotcount++;
	}

	$SLOTB_TABLE=str_replace("<AX_TIMESLOTS>",$dayrows,$daytable);
}
else
{
	$infoB.="Slot B cannot be opened yet.";
	$Blabel="";

}  // EIF SLOT B


	//info($infoA,$infoecho);
	//info($infoB,$infoecho);

	$infoA.=info($statusA,0,1);
	$infoB.=info($statusB,0,1);

	// SHOW SLOTS A & B
	echo'<table border="1" cellpadding="10" cellspacing="0" width=100%>';
	echo "<tr><td class=\"adminheader\">$infoA</td><td class=\"adminheader\">$infoB</td></tr>";
	echo "<tr><td valign=top>$SLOTA_TABLE &nbsp;</td><td valign=top>$SLOTB_TABLE &nbsp;</td></tr>";
	echo "</table><BR>";




}

// SHOW SLOTS AB  FOR BIG GROUPS

if	($SLOTAB_TABLE)
{
		echo'<table border="1" cellpadding="10" cellspacing="0" width=100%>';
		echo "<tr><td class=\"adminheader\">Availabilities for large Groups</td></tr>";
		echo "<tr><td valign=top>$SLOTAB_TABLE &nbsp;</td></tr>";
		echo "</table><BR>";

}


	// SHOW SLOT C&D



	$slotcodeC=strtoupper($myBrewery[0]).$mydate."C";
	$slotcodeD=strtoupper($myBrewery[0]).$mydate."D";

	$cslots=getextra($slotcodeC,$mydate);
	$dslots=getextra($slotcodeD,$mydate);

	$result=dosql("select * from groups where GROUP_CODE like '%$slotcodeC%'",0);
	$Cnums=mysql_num_rows($result);


	$result=dosql("select * from groups where GROUP_CODE like '%$slotcodeD%'",0);
	$Dnums=mysql_num_rows($result);

	$slotcodeC.=$Cnums+1;
	$slotcodeD.=$Dnums+1;

	if($showcount==0)
		$Clink="";
	else

		$Clink="<a href=\"admin_reservation.htm?slotcode=$slotcodeC&bookcount=$showcount\">";


	if($showcount==0)
		$Dlink="";
	else
		$Dlink="<a href=\"admin_reservation.htm?slotcode=$slotcodeD&bookcount=$showcount\">";





	if(is_array($cslots))   // IF WE GET AN ARRAY BACK, MAKE A TABLE OUT OF IT OTHERWISE USE THE "NO BOOKING MESSAGE RETURNED FROM FUNCTION"
	{
		$crows="<table border=0 width=100%>";
		$crows.="<TR><TH>Code</TH><TH>Lang</TH><TH>Guide</TH><TH>People</TH><TH>Comment</TH></TR>";
		foreach($cslots as $k=>$v)
		{
			$crows.="<tr><TD>".$v[SLOT_CODE]."</td><td>".$v[SLOT_LANG]."</td><td>".$v[SLOT_GUIDE]."</td><td>".$v[SLOT_COUNT]."</td><td>".$v[SLOT_EXTRA]."</td></tr>\n";
		}
		$crows.="</table>";
	}
	else
		$crows=$cslots;

	if(is_array($dslots))   // IF WE GET AN ARRAY BACK, MAKE A TABLE OUT OF IT OTHERWISE USE THE "NO BOOKING MESSAGE RETURNED FROM FUNCTION"
	{
		$drows="<table border=0 width=100%>";
		$drows.="<TR><TH>Code</TH><TH>Lang</TH><TH>Guide</TH><TH>People</TH><TH>Comment</TH></TR>";
		foreach($dslots as $k=>$v)
		{
			$drows.="<tr><TD>".$v[SLOT_CODE]."</td><td>".$v[SLOT_LANG]."</td><td>".$v[SLOT_GUIDE]."</td><td>".$v[SLOT_COUNT]."</td><td>".$v[SLOT_EXTRA]."</td></tr>\n";

		}
		$drows.="</table>";
	}
	else
		$drows=$dslots;


	echo '<table border="1" cellpadding="0" cellspacing="0" >';
	echo "<tr><td width=50% class=\"adminheader\">Slot C has $Cnums bookings</td><td width=50% class=\"adminheader\">Slot D has $Dnums bookings</td></tr>";
	echo "<tr><td valign=top>$crows</td><td valign=top>$drows</td></tr>";
	echo "<tr><td align=center><B>$Clink New C Slot Booking</a></td>";
	echo "<td align=center><b>$Dlink New D Slot Booking</a></td></tr>";
	echo "</table>";

}




?>



