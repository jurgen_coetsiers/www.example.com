<?
	if($comm=="delconfirm")
	{
		title(getword(cancellation));

		echo "<form method=\"POST\">";
		echo "<input type=\"hidden\" name=\"comm\" value=\"delOK\">";
		echo "<input type=\"hidden\" name=\"ID\" value=\"$ID\">";
		echo "<input name=\"sure\" type=checkbox> ".gw(cancelbooking_yes)."<BR><BR>";
		echo "<input type=submit value=\"".gw(del_booking_button)."\">";
		echo "<input onclick=\"javascript:location.href='$PHP_SELF?ID=$ID'\" type=\"button\" value=\"".gw(FORMCANCEL)."\" style=\"width: 160px\">";
		echo "</form>";
	}
	elseif(($comm=="delOK")&&($sure=="on"))
	{
		cancel_group($ID);
	}
	elseif($comm=="resendemail")
	{
		title(getword(confirmation));

		$bookemail=trim($bookemail);
		$result=dosql("select * from groups where GROUP_EMAIL='$bookemail' and `GROUP_DATE` >= now() and GROUP_DELETED='N'",0);
		if(mysql_num_rows($result)>0)
		{
			while ($row = mysql_fetch_array($result))
			{
				makemail($row);
			}
		}
		else
		{
			echo str_replace("<EMAIL>",$bookemail,gw(email_notfound)."<BR><BR>");

			echo(gw(lostemail)."<BR>");
			echo(gettemplate(lost_email,1,0));

		}



	}
	else  // CONFIRM
	{
		$group_row=md5exists($ID,0);

		if(is_array($group_row))
		{
			if ($group_row["GROUP_CONFIRMED"]=="N")
			{
				title(getword(confirmation));
				$GID=$group_row["GROUP_IDENT"];
				$EMAIL=$group_row["GROUP_EMAIL"];

				$row=confirmgroup($GID,0);

				if($group_row["GROUP_BIG"])      // CONFIRM SLOT B TOO
				{
					$B_GID=str_replace("A","B",$GID);
					$row=confirmgroup($B_GID,0);
				}

				echo(getword(booking_confirmed)."<BR>");
				$cancel_text=getword(cancelinfo);

				$cancel_text=str_replace("link","<a href=\"$PHP_SELF?comm=delconfirm&ID=$ID\">link</a>",$cancel_text);
				echo $cancel_text;

				$mailsubject=getword(confirmed_email_subject);
				$mailbody=getword(confirmed_email);

				mail($EMAIL, $mailsubject, $mailbody, $email_headers);

			}
			else
			{
				title(getword(cancellation));
				if(cancel_too_late($group_row["GROUP_DATE"]))
					echo(gw(cancel_toolate));
				else
				{
						echo(gw(confirm_OK_askdel));
						echo ("<a href=\"$PHP_SELF?comm=delconfirm&ID=$ID\">".$TEXT["del_booking_button"]."</a>");
				}

			}

		}
		else
		{
			if(isset($ID))
				echo(getword(error_groupcode)."<BR>");
			echo(gw(lostemail)."<BR>");
			echo(gettemplate(lost_email,1,0));

		}

	}


?>
