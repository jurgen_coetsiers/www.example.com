<?
	if($comm=="setfreeze")
	{
		$_SESSION['SS_FREEZE']=$freeze;
		echo gw(warn)."Freeze period set to $freeze for this session only";
	}

?>

<H1>Please select Freeze days.<BR></h1>
<form method="POST">
<input type=hidden name=comm value="setfreeze">
<table><tr><td>
Freeze Days:
<select name="freeze">
<option value="<?=$freeze_default?>">System Default=<?=$freeze_default?></option>
<?
	for($loop=1;$loop<=31;$loop++)
	{
		if($loop==$_SESSION['SS_FREEZE'])
			$mysel=" selected ";
		else
			$mysel="";

		echo "<option value=\"$loop\" $mysel>$loop</option>\n";
	}
?>
</select>
</td>
<td>
<input type="submit" value="Set Freeze">
</td>
</tr>
</table>