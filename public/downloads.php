<?php

//includes + error reporting
include('pager.php');
include('store_admin/amfphp/services/inc/functions.php');
error_reporting(E_ERROR);

//Filters
(!isset($_SESSION['filter']))?$_SESSION['filter']="all":NULL;
if (isset($_GET['filter'])) {
	$_SESSION['filter'] = $_GET['filter'];
}

$filter = $_SESSION['filter'];
$lng = $_GET['lng'];
$thispage = $_SERVER['PHP_SELF'];

if ($filter=="all") {
	$filterQry = "";
} else {
	$filterQry = " AND category='".$filter."'";
}

//language switch
switch($lng) {
	case 0:
		$mainQry = "SELECT id,pName_nl,pDes_nl,price,message,picture FROM products WHERE brand='jupiler' ".$filterQry;
		$lngStr = "nl";
	break;
	case 1:
		$mainQry = "SELECT id,pName_fr,pDes_fr,price,message,picture FROM products WHERE brand='jupiler' ".$filterQry;
		$lngStr = "fr";
	break;
	case 2:
		$mainQry = "SELECT id,pName_en,pDes_en,price,message,picture FROM products WHERE brand='jupiler' ".$filterQry;
		$lngStr = "en";
	break;
}
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>visit store</title>
<link href="css/stella_intra.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); </script> <script type="text/javascript"> try { var pageTracker = _gat._getTracker("UA-4619313-23"); pageTracker._trackPageview(); } catch(err) {}</script>
</head>

<body>
<table align="center">
<tr>
<td>

<div id="store_wrapper">
	<table  border="0" align="center"><tr><td align="left"><div id="horizontal_sep" style="padding-bottom:10px;"></div></td></tr></table>
<?
opendb();
// get the pager input values  
$page = $_GET['page'];  
$limit = 8;  
$result = mysql_query("select count(*) from products WHERE brand='jupiler' ".$filterQry);  
$total = mysql_result($result, 0, 0);  


// work out the pager values  
$pager  = Pager::getPagerData($total, $limit, $page);  
$offset = $pager->offset;  
$limit  = $pager->limit;  
$page   = $pager->page;  

$qMain = mysql_query($mainQry." LIMIT $offset, $limit");
$dMainItems = @mysql_num_rows($qMain);
?>
    
    <div id="products">

        <?php
        $cnt=1;
        while($dMain = @mysql_fetch_assoc($qMain))	{
        ?>
                <div id="itm<?=($cnt%4==0)?'lst':NULL?>">
                     <div id="content">
                         <table cellpadding="0" cellspacing="0" border="0" align="center">
                          <tr>
                            <td class="itm_img" align="left">
                            	<a href="store_admin/library/images/thumb_240x180/<?=$dMain['picture'].".jpg"?>"><img src="store_admin/library/images/thumb_240x180/<?=$dMain['picture'].".jpg"?>" border="0" /></a>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" class="itm_info" align="left"><?=$dMain['pName_'.$lngStr]?></td>
                          </tr>
                          <tr>
                           <td colspan="2" class="itm_price" align="left"><?=$dMain['price']?>&nbsp;&euro;</td>
                          </tr>
                         </table>
                     </div>
                </div>
        <? 
        $cnt++;
        } ?>  
        
        
    </div>
    
    <table  border="0" align="center"><tr><td align="left"><div id="horizontal_sep" style="padding-bottom:10px;"></div></td></tr></table>
    
     
    <!-- PAGING STARTS HERE -->
    
        <table cellpadding="0" cellspacing="0" border="0" align="center" width="545" id="paging">
        
        <tr>
        	<td valign="top" align="left" width="50px">
		<?
		// output paging system (could also do it before we output the page content) 
		if ($page == 1) // this is the first page - there is no previous page  
			echo "<img src=\"img/store_back.gif\" width=\"54\" height=\"20\" border=\"0\" />";  
		else            // not the first page, link to the previous page  
			echo "<a href=\"$thispage?page=" . ($page - 1) . "\"><img src=\"img/store_back.gif\" width=\"54\" height=\"20\" border=\"0\" /></a>";  
		?>   
        	</td>
                
		    <td valign="top" align="center" id="pager">
		<?
		if ($pager->numPages>15) {
			$stPg = $page-7;
			$maxPages=14;
			if ($stPg<1) $stPg = 1;
			$endPg = $stPg+$maxPages;
			if ($endPg>$pager->numPages) {
				$endPg = $pager->numPages;
				$stPg = $endPg-14;
			}
		} else {
			$stPg = 1;
			$maxPages = $pager->numPages;
			$endPg = $maxPages;
		}
		if ($stPg>1) echo '<a href="'.$thispage.'?page='.($pager->page-10).'">...</a>';
	
		for ($i = $stPg; $i <= $endPg; $i++) {  
			if ($i>1) echo " - ";  
			if ($i == $pager->page)  
				echo "$i";  
			else  
				echo "<a href=\"$thispage?page=$i\">$i</a>";  
		}  
		if ($endPg<$pager->numPages&&$maxPages>=14) echo ' - <a href="'.$thispage.'?page='.($pager->page+10).'">...</a>';
		
		?>  </td>
			
            <td valign="top" align="right" width="50px">
		<?
		if ($page == $pager->numPages) // this is the last page - there is no next page  
			echo "<img src=\"img/store_next.gif\" width=\"54\" height=\"20\" border=\"0\" />";  
		else            // not the last page, link to the next page  
			echo "<a href=\"$thispage?page=" . ($page + 1) . "\"><img src=\"img/store_next.gif\" width=\"54\" height=\"20\" border=\"0\" /></a>";  
		?>
        	</td>
        </tr>
        </table>
   
     <!-- PAGING ENDS HERE -->
</div>
</td>
</tr>
</table>
</body>
</html>
<?php closedb(); ?>
