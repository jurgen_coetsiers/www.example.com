<?php
session_start();
$thispage = $_SERVER['PHP_SELF'];
include_once("checklang.php");
// CONTROLEER AGE CHECK

if (isset($_GET['age'])) { 
	$age = $_GET['age'];
	$_SESSION['age'] = $age;	
	
} else {
	if (isset($_SESSION['age'])) $age = $_SESSION['age'];
	else header("Location: index.php");
}
if 	($_SESSION['age'] == 0) {
	header("Location: index.php");
	}
//CONTROLEER TAAL
if (isset($_GET['lng'])) { 
	$lng = $_GET['lng'];
	$_SESSION['lng'] = $lng;	
} else {
	if (isset($_SESSION['lng'])) $lng = $_SESSION['lng'];
	else $lng = 0;
}
$_SESSION['langfile'] = check_lang($lng);
include_once($_SESSION['langfile']); 

//CONTROLEER IMG PATH
if ($lng==0) $imgpath = "nl/";
elseif ($lng==2) $imgpath = "en/";
else $imgpath = "fr/";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="imagetoolbar" content="no" />
<title>index</title>
<link rel="stylesheet" href="css/style_general.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/style_leffe.css" type="text/css" media="screen" />
<script src="js/swfobject.js" type="text/javascript"></script>

<link rel="stylesheet" href="css/SIFRscreen.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/print.css" type="text/css" media="print" />
<script src="js/sifr.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
	sIFR.prefetch({
		src: 'din.swf', 
		highsrc: 'din.swf'
	});

	sIFR.compatMode = true;
	sIFR.activate();
  
	sIFR.replace({
		selector: 'h2', 
		src: 'din.swf', 
		highsrc: 'din.swf',
		css: {
		  '.sIFR-root': { 'color': '#333333' }
		}
	});
	sIFR.replace({
		selector: 'h2.intro', 
		src: 'din.swf', 
		highsrc: 'din.swf',
		css: {
		  '.sIFR-root': { 'color': '#333333' }
		}
	});
	sIFR.replace({
		selector: 'h3', 
		src: 'din.swf', 
		highsrc: 'din.swf',
		css: {
		  '.sIFR-root': { 'color': '#5a5a5a' }
		}
	});
	//]]>
</script>
  <script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); </script> <script type="text/javascript"> try { var pageTracker = _gat._getTracker("UA-4619313-23"); pageTracker._trackPageview(); } catch(err) {}</script>
</head>

<body>
<div id="wrapper">
	<div id="header">
    	<div id="lng">
        	<a href="<?php echo $thispage.'?lng=1' ?>">FR</a> - <a href="<?php echo $thispage.'?lng=0' ?>">NL</a> - <a href="<?php echo $thispage.'?lng=2' ?>">EN</a>
            <h1><a href=""><img src="img/logoBrew.png" height="90" border="0" width="150" alt="Brewery Visit" /></a></h1>
        </div>
        <div id="flashcontent">
			<strong>You need to upgrade your Flash Player</strong>
		</div>
        <img src="img/maakkeuze<?php echo $txt['img_sufix'];?>.png" width="140" height="112" border="0" />

<script type="text/javascript">
	// <![CDATA[
	
	var so = new SWFObject("flash/menu.swf", "menu", "300", "128", "9", "#FFFFFF");
	so.addParam("wmode", "transparent");
	so.addVariable("currentpage", "1"); // this line is optional, but this example uses the variable and displays this text inside the flash movie
	so.write("flashcontent");
	
	// ]]>
</script>
	</div>
	<div id="nav">
		<ul id="navlist">
			<li><img src="img/nav_bezoekdebrouwerij_leffe<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_bezoek_leffe'];?>" border="0" /></li>
            <li><a href="leffe_shop_images.php"><img src="img/nav_fanshop<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_fanshop'];?>" border="0" /></a></li>
            <li><a href="leffe_touristinfo.php"><img src="img/nav_leffeenomg<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_leffeomg'];?>" border="0" /></a></li>
            <li><a href="leffe_route.php"><img src="img/nav_route<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_route'];?>" border="0" /></a></li>
            <li><a href="leffe_reserveren.php"><img src="img/nav_reserveren<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_reserveren'];?>" border="0" /></a></li>
        </ul>
    </div>
    <div id="container">
    	<div id="slideshow">
      		<strong>You need to upgrade your Flash Player</strong>
        </div>
             <script type="text/javascript">
				// <![CDATA[		
				var so2 = new SWFObject("flash/imageplayer.swf", "imageplayer", "370", "390", "9", "#FFFFFF");
				so2.addVariable("brand", "leffe");
				so2.addVariable("i1", "bezoek_img_01.jpg"); 
				so2.addVariable("i2", "bezoek_img_02.jpg"); 
				so2.addParam("wmode", "transparent");
			so2.write("slideshow");	// ]]>
			</script>
    	<div id="cont_t"> </div>
        <div id="content">
        	<div id="innercontent">
            	<?php echo $txt['leffe_museum'];?>
    		</div>
        </div>
        <div id="cont_b"> </div>
    </div>
    <div id="footer">
    	<span class="ftr_l"><?php echo $txt['terms'];?></span><span class="ftr_r"><?php echo $txt['verstand'];?></span>
    </div>
</div>
</body>
</html>
