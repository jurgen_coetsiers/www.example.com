<?php
session_start();
$thispage = $_SERVER['PHP_SELF'];
include_once("checklang.php");
// CONTROLEER AGE CHECK

if (isset($_GET['age'])) { 
	$age = $_GET['age'];
	$_SESSION['age'] = $age;	
	
} else {
	if (isset($_SESSION['age'])) $age = $_SESSION['age'];
	else header("Location: index.php");
}
if 	($_SESSION['age'] == 0) {
	header("Location: index.php");
	}
//CONTROLEER TAAL
if (isset($_GET['lng'])) { 
	$lng = $_GET['lng'];
	$_SESSION['lng'] = $lng;	
} else {
	if (isset($_SESSION['lng'])) $lng = $_SESSION['lng'];
	else $lng = 0;
}
$_SESSION['langfile'] = check_lang($lng);
include_once($_SESSION['langfile']); 

//CONTROLEER IMG PATH
if ($lng==0) $imgpath = "nl/";
elseif ($lng==2) $imgpath = "en/";
else $imgpath = "fr/";

//includes + error reporting
include('pager.php');
include('store_admin/amfphp/services/inc/functions.php');
error_reporting(E_ERROR);

//Filters
(!isset($_SESSION['filter']))?$_SESSION['filter']="5":NULL;
if (isset($_GET['filter'])) {
	$_SESSION['filter'] = $_GET['filter'];
}

$filter = $_SESSION['filter'];
//$lng = $_GET['lng'];
//$thispage = $_SERVER['PHP_SELF'];
$brand = "stella";

if ($filter==5) {
	$filterQry = "";
} else {
	$filterQry = " AND category='".$filter."'";
}

//language switch
switch($lng) {
	case 0:
		$mainQry = "SELECT id,pName_nl,pDes_nl,price,message_nl,picture FROM products WHERE brand='".$brand."' ".$filterQry;
		$lngStr = "nl";
	break;
	case 1:
		$mainQry = "SELECT id,pName_fr,pDes_fr,price,message_fr,picture FROM products WHERE brand='".$brand."' ".$filterQry;
		$lngStr = "fr";
	break;
	case 2:
		$mainQry = "SELECT id,pName_en,pDes_en,price,message_en,picture FROM products WHERE brand='".$brand."' ".$filterQry;
		$lngStr = "en";
	break;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="imagetoolbar" content="no" />
<title>index</title>
<link rel="stylesheet" href="css/style_general.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/style_stella.css" type="text/css" media="screen" />
<link href="css/stella_intra.css" rel="stylesheet" type="text/css" />

<script src="js/swfobject.js" type="text/javascript"></script>

<link rel="stylesheet" href="css/SIFRscreen.css" type="text/css" media="screen">
<script src="js/sifr.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
	sIFR.prefetch({
		src: 'din.swf', 
		highsrc: 'din.swf'
	});

	sIFR.compatMode = true;
	sIFR.activate();
  
	sIFR.replace({
		selector: 'h2', 
		src: 'din.swf', 
		highsrc: 'din.swf',
		css: {
		  '.sIFR-root': { 'color': '#333333' }
		}
	});
	sIFR.replace({
		selector: 'h2.intro', 
		src: 'din.swf', 
		highsrc: 'din.swf',
		css: {
		  '.sIFR-root': { 'color': '#333333' }
		}
	});
	sIFR.replace({
		selector: 'h3', 
		src: 'din.swf', 
		highsrc: 'din.swf',
		css: {
		  '.sIFR-root': { 'color': '#5a5a5a' }
		}
	});
	//]]>
</script>
<!-- PRELOAD THE IMAGES NEEDED FOR THE MENU -->
<script language="JavaScript">
	
	var act = 5;
	var out = ["img/menu_bot_on.jpg","img/menu_glass_on.jpg","img/menu_shirt_on.jpg","img/menu_col_on.jpg","img/menu_var_on.jpg","img/menu_all_on.jpg",];
	var over = ["img/menu_bot_over.jpg","img/menu_glass_over.jpg","img/menu_shirt_over.jpg","img/menu_col_over.jpg","img/menu_var_over.jpg","img/menu_all_over.jpg"];
	
	function mouseOver(ndx) {
		if(act != ndx) {
			document.getElementById(ndx).src = over[ndx];	
		}
	}
	
	function mouseOut(ndx) {
		if(act != ndx) {
			document.getElementById(ndx).src = out[ndx];
		}
	}
	
	function mouseClick(ndx) {
		document.getElementById(ndx).src = over[ndx];	
		act = ndx;
	}
	
	function initMenu(ndx) {
		act = ndx;
		document.getElementById(ndx).src = over[ndx];	
	}
	
</script>

  <script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); </script> <script type="text/javascript"> try { var pageTracker = _gat._getTracker("UA-4619313-23"); pageTracker._trackPageview(); } catch(err) {}</script>
</head>

<body onload=<? echo "initMenu(". $filter . ")" ?>>
<div id="wrapper">
	<div id="header">
    	<div id="lng">
        	<a href="<?php echo $thispage.'?lng=1' ?>">FR</a> - <a href="<?php echo $thispage.'?lng=0' ?>">NL</a> - <a href="<?php echo $thispage.'?lng=2' ?>">EN</a>
            <h1><a href=""><img src="img/logoBrew.png" height="90" border="0" width="150" alt="Brewery Visit" /></a></h1>
        </div>
        <div id="flashcontent">
			<strong>You need to upgrade your Flash Player</strong>
		</div>
        <img src="img/maakkeuze<?php echo $txt['img_sufix'];?>.png" width="140" height="112" border="0" />

		<script type="text/javascript">
            // <![CDATA[
            
            var so = new SWFObject("flash/menu.swf", "menu", "300", "128", "9", "#FFFFFF");
            so.addParam("wmode", "transparent");
            so.addVariable("currentpage", "0"); // this line is optional, but this example uses the variable and displays this text inside the flash movie
            so.write("flashcontent");
            
            // ]]>
        </script>
	</div>
	<div id="nav">
		<ul id="navlist">
			<li><a href="stella_breweryvisit.php"><img src="img/nav_bezoekdebrouwerij<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_bezoek'];?>" border="0" /></a></li>
            <li><img src="img/nav_fanshop<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_fanshop'];?>" border="0" /></li>
            <li><a href="stella_touristinfo.php"><img src="img/nav_stellaomg<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_stellaomg'];?>" border="0" /></a></li>
            <li><a href="stella_route.php"><img src="img/nav_route<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_route'];?>" border="0" /></a></li>
            <li><a href="stella_events.php"><img src="img/nav_events<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_events'];?>" border="0" /></a></li>
            <li><a href="stella_beerfood.php"><img src="img/nav_beerenfood<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_beerenfood'];?>" border="0" /></a></li>
            <li><a href=<?php echo "web/" . $imgpath . "stella/showinfo.htm" ?> ><img src="img/nav_reserveren<?php echo $txt['img_sufix'];?>.png" height="14" width="<?php echo $txt['img_width_reserveren'];?>" border="0" /></a></li>
        </ul>
    </div>
    
    
    
    <div id="container">
    	<div id="slideshow" style="float:right;">
           <strong>You need to upgrade your Flash Player</strong>
        </div>
		<script type="text/javascript">
            // <![CDATA[		
            var so2 = new SWFObject("flash/imageplayer.swf", "imageplayer", "370", "390", "9", "#FFFFFF");
            so2.addVariable("brand", "jupiler");
            so2.addVariable("i1", "shop_img_01.jpg");
            so2.write("slideshow");		
            // ]]>
        </script>
    
                
        <div id="cont_t"></div>
        <div id="content">
        	<div id="innercontent">
               <?php echo $txt['stella_fanshop'];?>
              <table align="center" width="570px">
                    <!-- START MENU -->
                    <tr>
                        <td>
                            <a href="stella_fanshop_images.php?page=1&lng=0&filter=5" target="_self"><img src="img/menu_all_on.jpg" name="5" id="5" onMouseover="mouseOver(5)" onMouseout="mouseOut(5)" onClick="mouseClick(5)" border="0" /></a>
                            <a href="stella_fanshop_images.php?page=1&lng=0&filter=0" target="_self"><img src="img/menu_bot_on.jpg" name="0" id="0" onMouseover="mouseOver(0)" onMouseout="mouseOut(0)" onClick="mouseClick(0)" border="0" /></a>
                            <a href="stella_fanshop_images.php?page=1&lng=0&filter=1" target="_self"><img src="img/menu_glass_on.jpg" name="1" id="1" onMouseover="mouseOver(1)" onMouseout="mouseOut(1)" onClick="mouseClick(1)" border="0" /></a>
                            <a href="stella_fanshop_images.php?page=1&lng=0&filter=2" target="_self"><img src="img/menu_shirt_on.jpg" name="2" id="2" onMouseover="mouseOver(2)" onMouseout="mouseOut(2)" onClick="mouseClick(2)" border="0" /></a>
                            <a href="stella_fanshop_images.php?page=1&lng=0&filter=3" target="_self"><img src="img/menu_col_on.jpg" name="3" id="3" onMouseover="mouseOver(3)" onMouseout="mouseOut(3)" onClick="mouseClick(3)" border="0" /></a>
                            <a href="stella_fanshop_images.php?page=1&lng=0&filter=4" target="_self"><img src="img/menu_var_on.jpg" name="4" id="4" onMouseover="mouseOver(4)" onMouseout="mouseOut(4)" onClick="mouseClick(4)" border="0" /></a>
                        </td>
                    </tr>
                    <!-- END MENU -->


                <tr>
                	<td>
                <!-- START STORE -->
                <div id="store_wrapper">
                    <table  border="0" cellpadding="0" ><tr><td align="left"><div id="horizontal_sep" style="padding-bottom:22px;"></div></td></tr></table>
                <?
                opendb();
                
                // get the pager input values  
                $page = $_GET['page'];  
                $limit = 8;  
                $result = mysql_query("select count(*) from products WHERE brand='".$brand."' ".$filterQry);  
                $total = mysql_result($result, 0, 0);  
                
                // CHECK FOR RESULTS:
                if($total==0) { 
                    echo "No items found, please choose another category.";
                } else {
                
                    // work out the pager values  
                    $pager  = Pager::getPagerData($total, $limit, $page);  
                    $offset = $pager->offset;  
                    $limit  = $pager->limit;  
                    $page   = $pager->page;  
                    
                    $qMain = mysql_query($mainQry." LIMIT $offset, $limit");
                    $dMainItems = @mysql_num_rows($qMain);
                    
                }
                ?>
                    
                    <div id="products">
                        
                        <?php
						$cnt=1;
						while($dMain = @mysql_fetch_assoc($qMain))	{
						?>
    					<div id="itm<?=($cnt%4==0)?'lst':NULL?>">
                                     <div id="store_content">
                          
                                         <table cellpadding="0" cellspacing="0" border="0" align="left">
                                          <tr>
                                            <td class="itm_img" align="left">
                                                <a href="stella_fanshop_detail.php?id=<?=$dMain['id']?>&lng=<? echo $lng ?>&filter=<? echo $filter ?>"><img src="store_admin/library/images/thumb_240x180/<?=$dMain['picture'].".jpg"?>" border="0" /></a>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td colspan="2" class="itm_info" align="left"><?=$dMain['pName_'.$lngStr]?></td>
                                          </tr>
                                          <tr>
                                           <td colspan="2" class="itm_price" align="left"><?=$dMain['price']?>&nbsp;&euro;</td>
                                          </tr>
                                         </table>
                                     </div>
                                </div>
                        <? 
                        $cnt++;
                        } ?>
                        
                        
                    </div>
                    
                    <table  border="0" cellpadding="0" ><tr><td align="left"><div id="horizontal_sep" style="padding-bottom:22px;"></div></td></tr></table>
                    
                     
                    <!-- PAGING STARTS HERE -->
                    
                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="545" id="paging">
                        
                        <tr>
                            <td valign="top" align="left" width="50px">
                        <?
                        // output paging system (could also do it before we output the page content) 
                        if ($page == 1) // this is the first page - there is no previous page  
                            echo "";  
                        else            // not the first page, link to the previous page  
                            echo "<a href=\"$thispage?page=" . ($page - 1) . "\"><img src=\"img/store_back.gif\" width=\"54\" height=\"20\" border=\"0\" /></a>";  
                        ?>   
                            </td>
                                
                            <td valign="top" align="center" id="pager">
                        <?
                        if ($pager->numPages>15) {
                            $stPg = $page-7;
                            $maxPages=14;
                            if ($stPg<1) $stPg = 1;
                            $endPg = $stPg+$maxPages;
                            if ($endPg>$pager->numPages) {
                                $endPg = $pager->numPages;
                                $stPg = $endPg-14;
                            }
                        } else {
                            $stPg = 1;
                            $maxPages = $pager->numPages;
                            $endPg = $maxPages;
                        }
                        if ($stPg>1) echo '<a href="'.$thispage.'?page='.($pager->page-10).'">...</a>';
                    
                        for ($i = $stPg; $i <= $endPg; $i++) {  
                            if ($i>1) echo " - ";  
                            if ($i == $pager->page)  
                                echo "$i";  
                            else  
                                echo "<a href=\"$thispage?page=$i\">$i</a>";  
                        }  
                        if ($endPg<$pager->numPages&&$maxPages>=14) echo ' - <a href="'.$thispage.'?page='.($pager->page+10).'">...</a>';
                        
                        ?>  </td>
                            
                            <td valign="top" align="right" width="50px">
                        <?
                        if ($page == $pager->numPages) // this is the last page - there is no next page  
                            echo "";  
                        else            // not the last page, link to the next page  
                           if ($total!=0) echo "<a href=\"$thispage?page=" . ($page + 1) . "\"><img src=\"img/store_next.gif\" width=\"54\" height=\"20\" border=\"0\" /></a>";  
                        ?>
                            </td>
                        </tr>
                        </table>
                   
                     <!-- PAGING ENDS HERE -->
                </div>
                </td>
                </tr>
              </table>
   		  </div>
        </div>
        <div id="cont_b">
        </div>
    </div>
    <div id="footer">
    	<span class="ftr_l"><?php echo $txt['terms'];?></span><span class="ftr_r"><?php echo $txt['verstand'];?></span>
    </div>
</div>
</body>
</html>
<?php closedb(); ?>