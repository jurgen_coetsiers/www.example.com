<?php

//includes + error reporting
include('pager.php');
include('store_admin/amfphp/services/inc/functions.php');
error_reporting(E_ERROR);

//Filters
(!isset($_SESSION['filter']))?$_SESSION['filter']="5":NULL;
if (isset($_GET['filter'])) {
	$_SESSION['filter'] = $_GET['filter'];
}

$filter = $_SESSION['filter'];
$lng = $_GET['lng'];
$thispage = $_SERVER['PHP_SELF'];
$brand = "jupiler";

if ($filter==5) {
	$filterQry = "";
} else {
	$filterQry = " AND category='".$filter."'";
}

//language switch
switch($lng) {
	case 0:
		$mainQry = "SELECT id,pName_nl,pDes_nl,price,message,picture FROM products WHERE brand='".$brand."' ".$filterQry;
		$lngStr = "nl";
	break;
	case 1:
		$mainQry = "SELECT id,pName_fr,pDes_fr,price,message,picture FROM products WHERE brand='".$brand."' ".$filterQry;
		$lngStr = "fr";
	break;
	case 2:
		$mainQry = "SELECT id,pName_en,pDes_en,price,message,picture FROM products WHERE brand='".$brand."' ".$filterQry;
		$lngStr = "en";
	break;
}
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>visit store</title>
<link href="css/stella_intra.css" rel="stylesheet" type="text/css" />

<!-- PRELOAD THE IMAGES NEEDED FOR THE MENU -->
<script language="JavaScript">
	
	var act = 5;
	var out = ["img/menu_bot_on.jpg","img/menu_glass_on.jpg","img/menu_shirt_on.jpg","img/menu_col_on.jpg","img/menu_var_on.jpg","img/menu_all_on.jpg",];
	var over = ["img/menu_bot_over.jpg","img/menu_glass_over.jpg","img/menu_shirt_over.jpg","img/menu_col_over.jpg","img/menu_var_over.jpg","img/menu_all_over.jpg"];
	
	function mouseOver(ndx) {
		if(act != ndx) {
			document.getElementById(ndx).src = over[ndx];	
		}
	}
	
	function mouseOut(ndx) {
		if(act != ndx) {
			document.getElementById(ndx).src = out[ndx];
		}
	}
	
	function mouseClick(ndx) {
		document.getElementById(ndx).src = over[ndx];	
		act = ndx;
	}
	
	function initMenu(ndx) {
		act = ndx;
		document.getElementById(ndx).src = over[ndx];	
	}
	
</script>

  <script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); </script> <script type="text/javascript"> try { var pageTracker = _gat._getTracker("UA-4619313-23"); pageTracker._trackPageview(); } catch(err) {}</script>
</head>

<body onload=<? echo "initMenu(". $filter . ")" ?>>

<table align="center">
<!-- START MENU -->
<tr>
	<td>
    	<a href="jupiler_store.php?page=1&lng=0&filter=5" target="_self"><img src="img/menu_all_on.jpg" name="5" id="5" onMouseover="mouseOver(5)" onMouseout="mouseOut(5)" onClick="mouseClick(5)" border="0" /></a>
    	<a href="jupiler_store.php?page=1&lng=0&filter=0" target="_self"><img src="img/menu_bot_on.jpg" name="0" id="0" onMouseover="mouseOver(0)" onMouseout="mouseOut(0)" onClick="mouseClick(0)" border="0" /></a>
        <a href="jupiler_store.php?page=1&lng=0&filter=1" target="_self"><img src="img/menu_glass_on.jpg" name="1" id="1" onMouseover="mouseOver(1)" onMouseout="mouseOut(1)" onClick="mouseClick(1)" border="0" /></a>
        <a href="jupiler_store.php?page=1&lng=0&filter=2" target="_self"><img src="img/menu_shirt_on.jpg" name="2" id="2" onMouseover="mouseOver(2)" onMouseout="mouseOut(2)" onClick="mouseClick(2)" border="0" /></a>
        <a href="jupiler_store.php?page=1&lng=0&filter=3" target="_self"><img src="img/menu_col_on.jpg" name="3" id="3" onMouseover="mouseOver(3)" onMouseout="mouseOut(3)" onClick="mouseClick(3)" border="0" /></a>
        <a href="jupiler_store.php?page=1&lng=0&filter=4" target="_self"><img src="img/menu_var_on.jpg" name="4" id="4" onMouseover="mouseOver(4)" onMouseout="mouseOut(4)" onClick="mouseClick(4)" border="0" /></a>
    </td>
</tr>
<!-- END MENU -->


<tr>
<td>
<!-- START STORE -->
<div id="store_wrapper">
	<table  border="0" cellpadding="0" ><tr><td align="left"><div id="horizontal_sep" style="padding-bottom:22px;"></div></td></tr></table>
<?
opendb();

// get the pager input values  
$page = $_GET['page'];  
$limit = 8;  
$result = mysql_query("select count(*) from products WHERE brand='".$brand."' ".$filterQry);  
$total = mysql_result($result, 0, 0);  

// CHECK FOR RESULTS:
if($total==0) echo "No items found, please choose another category.";

// work out the pager values  
$pager  = Pager::getPagerData($total, $limit, $page);  
$offset = $pager->offset;  
$limit  = $pager->limit;  
$page   = $pager->page;  

$qMain = mysql_query($mainQry." LIMIT $offset, $limit");
$dMainItems = @mysql_num_rows($qMain);
	
?>
    
    <div id="products">
		
        <?php
        $cnt=1;
        while($dMain = @mysql_fetch_assoc($qMain))	{
        ?>
        
                <div id="itm<?=($cnt%4==0)?'lst':NULL?>">
                     <div id="content_store">
                         <table cellpadding="0" cellspacing="0" border="0" align="center">
                          <tr>
                            <td class="itm_img" align="left">
                            	<a href="store_detail.php?id=<?=$dMain['id']?>&lng=<? echo $lng ?>&filter=<? echo $filter ?>"><img src="store_admin/library/images/thumb_240x180/<?=$dMain['picture'].".jpg"?>" border="0" /></a>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" class="itm_info" align="left"><?=$dMain['pName_'.$lngStr]?></td>
                          </tr>
                          <tr>
                           <td colspan="2" class="itm_price" align="left"><?=$dMain['price']?>&nbsp;&euro;</td>
                          </tr>
                         </table>
                     </div>
                </div>
        <? 
        $cnt++;
        } ?>
        
        
    </div>
    
    <table  border="0" cellpadding="0" ><tr><td align="left"><div id="horizontal_sep" style="padding-bottom:22px;"></div></td></tr></table>
    
     
    <!-- PAGING STARTS HERE -->
    
        <table cellpadding="0" cellspacing="0" border="0" align="center" width="545" id="paging">
        
        <tr>
        	<td valign="top" align="left" width="50px">
		<?
		// output paging system (could also do it before we output the page content) 
		if ($page == 1) // this is the first page - there is no previous page  
			echo "<img src=\"img/store_back.gif\" width=\"54\" height=\"20\" border=\"0\" />";  
		else            // not the first page, link to the previous page  
			echo "<a href=\"$thispage?page=" . ($page - 1) . "\"><img src=\"img/store_back.gif\" width=\"54\" height=\"20\" border=\"0\" /></a>";  
		?>   
        	</td>
                
		    <td valign="top" align="center" id="pager">
		<?
		if ($pager->numPages>15) {
			$stPg = $page-7;
			$maxPages=14;
			if ($stPg<1) $stPg = 1;
			$endPg = $stPg+$maxPages;
			if ($endPg>$pager->numPages) {
				$endPg = $pager->numPages;
				$stPg = $endPg-14;
			}
		} else {
			$stPg = 1;
			$maxPages = $pager->numPages;
			$endPg = $maxPages;
		}
		if ($stPg>1) echo '<a href="'.$thispage.'?page='.($pager->page-10).'">...</a>';
	
		for ($i = $stPg; $i <= $endPg; $i++) {  
			if ($i>1) echo " - ";  
			if ($i == $pager->page)  
				echo "$i";  
			else  
				echo "<a href=\"$thispage?page=$i\">$i</a>";  
		}  
		if ($endPg<$pager->numPages&&$maxPages>=14) echo ' - <a href="'.$thispage.'?page='.($pager->page+10).'">...</a>';
		
		?>  </td>
			
            <td valign="top" align="right" width="50px">
		<?
		if ($page == $pager->numPages) // this is the last page - there is no next page  
			echo "<img src=\"img/store_next.gif\" width=\"54\" height=\"20\" border=\"0\" />";  
		else            // not the last page, link to the next page  
			echo "<a href=\"$thispage?page=" . ($page + 1) . "\"><img src=\"img/store_next.gif\" width=\"54\" height=\"20\" border=\"0\" /></a>";  
		?>
        	</td>
        </tr>
        </table>
   
     <!-- PAGING ENDS HERE -->
</div>
</td>
</tr>
</table>
</body>
</html>
<?php closedb(); ?>
