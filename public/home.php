<?php
session_start();
$thispage = $_SERVER['PHP_SELF'];
include_once("checklang.php");
// CONTROLEER AGE CHECK

if (isset($_GET['age'])) { 
	$age = $_GET['age'];
	$_SESSION['age'] = $age;	
	
} else {
	if (isset($_SESSION['age'])) $age = $_SESSION['age'];
	else header("Location: index.php");
}
if 	($_SESSION['age'] == 0) {
	header("Location: index.php");
	}
//CONTROLEER TAAL
if (isset($_GET['lng'])) { 
	$lng = $_GET['lng'];
	$_SESSION['lng'] = $lng;	
} else {
	if (isset($_SESSION['lng'])) $lng = $_SESSION['lng'];
	else $lng = 0;
}
$_SESSION['langfile'] = check_lang($lng);
include_once($_SESSION['langfile']); 

//CONTROLEER IMG PATH
if ($lng==0) $imgpath = "nl/";
elseif ($lng==2) $imgpath = "en/";
else $imgpath = "fr/";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="imagetoolbar" content="no">
<title>index</title>
<link rel="stylesheet" href="css/style_general.css" type="text/css" media="screen" />
<script src="js/sifr.js" type="text/javascript"></script>
  <script type="text/javascript">
  //<![CDATA[
  sIFR.prefetch({
    src: 'din.swf', 
    highsrc: 'din.swf'
  });

  sIFR.compatMode = true;
  sIFR.activate();
  
  sIFR.replace({
    selector: 'h2', 
    src: 'dinB.swf', 
    highsrc: 'dinB.swf',
	wmode: 'transparent',
	 css: {
      '.sIFR-root': { 'color': '#2b2b2b' },
	  'a': { 'text-decoration': 'none' },
      'a:link': { 'color': '#2b2b2b' },
      'a:hover': { 'color': '#FFFFFF' }
    }
  });
  //]]>
  </script>
  <script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); </script> <script type="text/javascript"> try { var pageTracker = _gat._getTracker("UA-4619313-23"); pageTracker._trackPageview(); } catch(err) {}</script>
</head>

<body>
<div id="wrapper">
	<div id="header">
    	<div id="lng">
        	<a href="<?php echo $thispage.'?lng=1' ?>">FR</a> - <a href="<?php echo $thispage.'?lng=0' ?>">NL</a> - <a href="<?php echo $thispage.'?lng=2' ?>">EN</a>
            <h1><a href="home.php"><img src="img/logoBrew.png" height="90" border="0" width="150" alt="Brewery Visit" /></a></h1>
        </div>
	</div>
	<div id="nav">
		<ul id="navlist">
			<li><img src="img/nav_index<?php echo $txt['img_sufix'];?>.png" width="141" height="14" border="0" /></li>
        </ul>
    </div>
    <div id="container">
    	<div id="cont_t"> </div>
        <div id="content">
			<a href="stella.php"><img src="img/glas_stella.jpg" width="238" height="360" border="0" alt="STELLA" /></a>
            <a href="jupiler.php"><img src="img/glas_jupiler.jpg" width="198" height="360" border="0" alt="JUPILER" /></a>
            <a href="http://www.hertogjan.nl" target="_blank"><img src="img/glas_hertogjan.jpg" width="235" height="360" border="0" alt="HERTOG JAN" /></a>
            <a href="http://www.twitgebrouw.be/" target="_blank"><img src="img/glas_hoegaarden.jpg" width="229" height="360" border="0" alt="HOEGAARDEN" /></a>
      </div>
        <div id="cont_b"> </div>
    </div>
    <div id="footer">
    	<span class="ftr_l"><?php echo $txt['terms'];?></span><span class="ftr_r"><?php echo $txt['verstand'];?></span>
    </div>
</div>
</body>
</html>
