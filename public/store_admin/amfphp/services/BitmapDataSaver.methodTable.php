<?php 
$this->methodTable = array(
	"getBitmapId" => array(
		"description" => "Get a unique bitmap id so we can implement a lock",
		"arguments" => array("bitDepth", "width", "height"),
		"access" => "remote"
	),
	"saveImagePart" => array(
		"description" => "Save a part of an image onto a bitmap",
		"arguments" => array("bitmapId", "bytes"),
		"access" => "remote"
	),
	"endSaveImage" => array(
		"description" => "Decode rgb file, save as a png",
		"arguments" => array("bitmapId"),
		"access" => "remote"
	),
	"stringToPng" => array(
		"description" => "Saves an image as PNG32 using GD only",
		"arguments" => array("bitmapId", "pixels", "width", "height"),
		"access" => "private"
	),
	"retrieveCompressed" => array(
		"description" => "Retrieve a saved image in whatever format \n Note only PNG24 and PNG32 are supported. Use gif where PNG8 would be useful",
		"arguments" => array("id - The bitmap id", "format - The format (jpg, png, or gif)", "quality - The quality (jpg only)"),
		"access" => "remote"
	),
	
	"sanitize" => array(
		"description" => "Sanitize the name of a bitmap id",
		"arguments" => array("id"),
		"access" => "private"
	)
);
?>