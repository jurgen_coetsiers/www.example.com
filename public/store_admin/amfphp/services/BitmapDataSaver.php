<?php
//define("SAVE_FOLDER", $_SERVER['DOCUMENT_ROOT'] . '/proximus/moviemaker/site_now/library/images/');
//define("SAVE_FOLDER", 'http://www.famous.be/projects/proximus/moviemaker/site_now/library/images/');
define("SAVE_FOLDER", '../../library/images/');

class BitmapDataSaver
{
	function BitmapDataSaver()
	{
		include('BitmapDataSaver.methodTable.php');
	}
	
	/**
	 * Get a unique bitmap id so we can implement a lock
	 * @access remote
	 */
	function getBitmapId($bitDepth, $width, $height, $id)
	{
		//$uniqueID = md5( microtime() . rand() );
		$uniqueID = $id;
		touch(SAVE_FOLDER . 'temp/' . $uniqueID . '.rgb');
		$_SESSION[$uniqueID]['depth'] = $bitDepth;
		$_SESSION[$uniqueID]['width'] = $width;
		$_SESSION[$uniqueID]['height'] = $height;
		$_SESSION[$uniqueID]['lastByte'] = "";
		return $uniqueID;
	}
	
	/**
	 * Save a part of an image onto a bitmap
	 * @access remote
	 */
	function saveImagePart($bitmapId, $bytes)
	{
		$bitmapId = $this->sanitize($bitmapId);
		$chars = array(
				'A','B','C','D','E','F','G','H',
				'I','J','K','L','M','N','O','P',
				'Q','R','S','T','U','V','W','X',
				'Y','Z','a','b','c','d','e','f',
				'g','h','i','j','k','l','m','n',
				'o','p','q','r','s','t','u','v',
				'w','x','y','z','0','1','2','3',
				'4','5','6','7','8','9','+','/');
		$reverse = array_flip($chars);
		
		$out = "";
		$byte = $_SESSION[$bitmapId]['lastByte'];
		$depth = $_SESSION[$bitmapId]['depth'];
		$lastBytes = $_SESSION[$bitmapId]['lastBytes'];
		$firstPass = true;
		
		if($depth == 32)
		{
			$chunkSize = 16;
		}
		elseif($depth == 24)
		{
			$chunkSize = 8;
		}
		elseif($depth == 12)
		{
			$chunkSize = 6;
		}
		
		while(($pos = strpos($bytes, '#')) !== FALSE && $pos !== FALSE)
		{ 
			if($pos >= $chunkSize)
			{
				$byte = substr($bytes, $pos - $chunkSize, $chunkSize);
			}
			elseif($pos > 0)
			{
				$byte = substr($lastBytes, 16 - $chunkSize + $pos) . substr($bytes, 0, $pos);
			}
			elseif($firstPass)
			{
				$byte = $lastBytes;
			}
			$out .= substr($bytes, 0, $pos) .
				   str_repeat($byte, $reverse[ substr($bytes, $pos + 1, 1) ] + 1);
			$bytes = substr($bytes, $pos + 2);
			$firstPass = false;
		}
		

		
		$out .= $bytes;
		$bytes = $out;
		$_SESSION[$bitmapId]['lastByte'] = $byte;
		$_SESSION[$bitmapId]['lastBytes'] = substr($bytes, strlen($bytes) - 16);
		
		file_put_contents(SAVE_FOLDER . 'temp/' . $bitmapId . '.rgb', $bytes, FILE_APPEND);
	}
	
	/**
	 * Decode rgb file, save as a png
	 * @access remote
	 */
	function endSaveImage($bitmapId)
	{
		$bitmapId = $this->sanitize($bitmapId);
		
		$bytes = file_get_contents(SAVE_FOLDER . 'temp/' . $bitmapId . '.rgb');
		$width = $_SESSION[$bitmapId]['width'];
		$height = $_SESSION[$bitmapId]['height'];
		
		$data = base64_decode($bytes);
		$depth = $_SESSION[$bitmapId]['depth'];
		
		
		
		if($depth == 12)
		{
			include("BitmapDataSaver.12bitreplacementTable.php");
			$data = strtr($data, $table);
		}
		
		unlink(SAVE_FOLDER . 'temp/' . $bitmapId . '.rgb');
		
		if($depth == 24 || $depth == 12)
		{
			$data = substr($data, 0, 3*$width*$height);
			$data = chunk_split($data, 3, chr(0));
			
			$data = chr(0) . substr($data, 0, strlen($data) - 1);
			$link = $this->stringToPng($bitmapId, $data, $width, $height, false);
			$_SESSION[$bitmapId]['location'] = $link;
		}
		elseif($depth == 32)
		{
			$data = substr($data, 0, 4*$width*$height);
			$link = $this->stringToPng($bitmapId, $data, $width, $height, true);
			$_SESSION[$bitmapId]['location'] = $link;
		}
		
		return true;
	}
	
	/**
	 * Saves an image as PNG32 using GD only
	 */
	function stringToPng($bitmapId, $pixels, $width, $height, $transparent)
	{
		
		// GD library .gd file header
		$gd_header = "";
		$gd_header .= "\xFF\xFE";
		$gd_header .= pack("n2", $width, $height);
		$gd_header .= "\x01";
		$gd_header .= "\xFF\xFF\xFF\xFF";
		
		// GD library .gd file datas
		$bmp= $gd_header . $pixels;

		// create a .gd file
		$filename = SAVE_FOLDER . 'temp/' . $bitmapId;
		file_put_contents($filename . '.gd', $bmp);
		
		// open the .gd file
		$imggd = imagecreatefromgd($filename.'.gd');
		
		if ($imggd) {
			if($transparent) {
				imageAlphaBlending($imggd, true);
				imageSaveAlpha($imggd, true);
			}
			
			// if successfull create a .png file and delete the .gd
			imagepng($imggd, $filename.'.png');
			
			unlink($filename . '.gd');
			return $filename . '.png';
		}
		
		return false;
	}
	
	
	/**
	 * Retrieve a saved image in whatever format
	 * Note only PNG24 and PNG32 are supported. Use gif where PNG8 would be useful
	 * @param id The bitmap id
	 * @param format The format (jpg, png, or gif)
	 * @param quality The quality (jpg only)
	 * @access remote
	 */
	function retrieveCompressed($id, $format, $quality){
	
		$bitmapId = $this->sanitize($id);
		$img = imagecreatefrompng(SAVE_FOLDER . 'temp/' . $id . '.png');
		
		if($format == 'gif') {
			imagegif($img, SAVE_FOLDER . 'thumb_480x360/' . $id . '.gif');
			
		} elseif($format == 'jpg') {
			imagejpeg($img, SAVE_FOLDER . 'thumb_480x360/' . $id . '.jpg', $quality);
			
		}
		
		//Don't change anything if png is selected, since we are already in png
		return $id . '.' . $format;
	}
	
	/**
	 * Sanitize the name of a bitmap id
	 */
	function sanitize($id)
	{
		return str_replace(array('/', '.'), array('',''), $id);
	}
	
	/**
	*	Create thumbnail
	*/
	function createThumb( $name, $new_width, $new_height ) {
		
		$src = imagecreatefrompng( SAVE_FOLDER . 'temp/' . $name . '.png' );
		list( $width, $height ) = getimagesize( SAVE_FOLDER . 'temp/' . $name . '.png' );
		
		$img_ratio = $width/$height;
		$ratio = $new_width/$new_height;
		
		if ($img_ratio>$ratio) {
			$new_height=($height/$width)*$new_width;
		} else {
			$new_width=($width/$height)*$new_height;
		} 
		
		$tmp = imagecreatetruecolor($new_width,$new_height);
	
		imagecopyresampled( $tmp, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
		
		$filename = SAVE_FOLDER . 'thumb_240x180/' . $name . '.jpg';
		imagejpeg($tmp, $filename, 65 );
		
		//and remove the not used crap for clean library
		//unlink( SAVE_FOLDER . 'temp/' . $name . ".png"  );
		//unlink( SAVE_FOLDER . $name . '.rgb' );
		
	}
	
}
?>