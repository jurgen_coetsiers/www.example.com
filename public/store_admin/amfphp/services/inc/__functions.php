<?php //common functions

//SET DB 
if ($_SERVER['SERVER_NAME'] == "10.1.1.224") {
	define("MYSQL_DB", "brew_store");
	define("MYSQL_HOST", "localhost");
	define("MYSQL_USER", "root");
	define("MYSQL_PASSWD", "");
} else {
	define("MYSQL_DB", "inbev_breweryvisits");
	define("MYSQL_HOST", "mysql.tcsondemand.com");
	define("MYSQL_USER", "inbevbrewery");
	define("MYSQL_PASSWD", "Jc5qn7");
}

//OPEN DATABASE
function opendb() {
	mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWD) or die("There is a problem with the database. Please try again later.");
	mysql_select_db(MYSQL_DB);
}

//CLOSE DATABASE
function closedb() {
	mysql_close();
}

//FLASh <-> PHP conversie
function parseIt($in){ //php->flash
	return(urlencode(utf8_encode($in)));
}
function deparseIt($in){ //flash->php
	return(urldecode(utf8_decode($in)));
}

function convert_smart_quotes($string) { 
    $search = array(chr(145), 
						chr(146), 
						chr(147), 
						chr(148), 
						chr(151)); 
 
	$replace = array('&lsquo;', 
						 '&rsquo;', 
						 '&ldquo;', 
						 '&rdquo;', 
						 '&mdash;'); 
 
    return str_replace($search, $replace, $string); 
} 


//CHECK EMAIL
function checkEmail($email) { 
    	if( (preg_match('/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/', $email)) || 
    		(preg_match('/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/',$email)) ) { 
 			return true;
     	}
    	return false;
 }
	
/* Use safe queries ALWAYS */
function MR($val)	{
	return mysql_real_escape_string(stripslashes($val));
}

function TXT($val) {
	return nl2br(htmlentities(utf8_decode($val)));
}

/* Include php files */
function doInclude($text, $path='php/') {
	$inc1 = "[include]";
	$inc2 = "[/include]";
	
	if (strstr($text, $inc1)) {
		$st=strpos($text,$inc1);
		$end=strpos($text,$inc2);
		
		$phpfile = substr($text, ($st+9), ($end-($st+9)));
		
		$newtxt = explode("[include]".$phpfile."[/include]", $text);
		
		echo $newtxt[0];
		
		include($path.$phpfile);
				
		doInclude($newtxt[1]);
	} else {
		echo $text;
	}
}

//functie die een unique id genereert
function getrand($length) {
	// random key paramters
	$keyset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	// first 14 chars of transaction_id are date, last 4 random
	//$length = 6;
	// Random Key Generator
	$randkey = "";
	$max = strlen($keyset)-1;

	for ($i=0; $i<$length; $i++) {
		$randkey .= substr($keyset, rand(0,$max), 1);
	}
	return $randkey;
} 

//puts in format YYYYMMDDhhmmss
function uniqueID() {
	$date = date("ymd");
	$uniqueID = getrand(6).$date; 
	return $uniqueID;
}

//remove \n\r \r in string -> \n
function newline($str) {
	return str_replace(array("\r\n","\r"), "\n", $str);
}


//truncate strings for word break use " "
function trunc($string, $limit, $break=".", $pad="...") { 
	// return with no change if string is shorter than $limit  
	if(strlen($string) <= $limit) return $string; // is $break present between $limit and the end of the string?  
	if(false !== ($breakpoint = strpos($string, $break, $limit))) { 
		if($breakpoint < strlen($string) - 1) { 
			$string = substr($string, 0, $breakpoint) . $pad; 
		} 
	} 
	return $string; 
}

//make utf8
function u8($val) {
	return utf8_encode($val);
}


// MEMORY VERHOGEN
function setMemoryForImage( $filename ){    
	$imageInfo = getimagesize($filename);
    $MB = 1048576;  // number of bytes in 1M
    $K64 = 65536;    // number of bytes in 64K
    $TWEAKFACTOR = 1.5;  // Or whatever works for you
    $memoryNeeded = round( ( $imageInfo[0] * $imageInfo[1]
                                           * $imageInfo['bits']
                                           * $imageInfo['channels'] / 8
                             + $K64
                           ) * $TWEAKFACTOR
                         );
    //ini_get('memory_limit') only works if compiled with "--enable-memory-limit" also
    //Default memory limit is 8MB so well stick with that.
    //To find out what yours is, view your php.ini file.
    $memoryLimit = 8 * $MB;
    if (function_exists('memory_get_usage') && memory_get_usage() + $memoryNeeded > $memoryLimit){
        $newLimit = $memoryLimitMB + ceil( ( memory_get_usage() + $memoryNeeded - $memoryLimit) / $MB);
        ini_set( 'memory_limit', $newLimit . 'M' );
        return true;
    } else {
        return false;
    }
}

?>