<?php

include("inc/functions.php");

class products
{
    function products()
    {
        $this->methodTable = array
        (
			
			"addProduct" => array
			(
				"description" => "get all products",
				"access" => "remote")
        	);
    }
	
	/*
	*	ADD A PRODUCT
	*/
	
	function addProduct( $chck, $brand, $cat, $nm_nl, $nm_fr, $nm_en, $des_nl, $des_fr, $des_en, $price, $msg_nl, $msg_fr, $msg_en, $pic, $active, $show ) {
		
		
		opendb();
		
	   	if ($chck == "datzeiuwmoedergisterenavondook" ) {
			
			$qry = mysql_query("INSERT INTO products (brand, category, pName_nl, pName_fr, pName_en, pDes_nl, pDes_fr, pDes_en, price, message_nl, message_fr, message_en, picture, active, deleted ) VALUES('".MR($brand)."','".MR($cat)."','".MR($nm_nl)."','".MR($nm_fr)."','".MR($nm_en)."','".MR($des_nl)."','".MR($des_fr)."','".MR($des_en)."','".MR($price)."','".MR($msg_nl)."','".MR($msg_fr)."','".MR($msg_en)."','".MR($pic)."','".MR($active)."','".MR($show)."')");
			
			if($qry) {
				return "0"; //no rr
			} else {
				return "2"; //rr making query
			}
			
	   	} else {

			return  "1"; //not a valid checkString
	   		
	   	}
		
		
		closedb();
		
		
	}
	
	/*
	*	EDIT A PRODUCT
	*/
	
	function editProduct( $id, $chck, $brand, $cat, $nm_nl, $nm_fr, $nm_en, $des_nl, $des_fr, $des_en, $price, $msg_nl, $msg_fr, $msg_en, $pic, $active) {
		
		
		opendb();
		
	   	if ($chck == "datzeiuwmoedergisterenavondook" ) {
			
			$qry = mysql_query("UPDATE products SET brand='".MR($brand)."',category='".MR($cat)."',pName_nl='".MR($nm_nl)."',pName_fr='".MR($nm_fr)."',pName_en='".MR($nm_en)."',pDes_nl='".MR($des_nl)."', pDes_fr='".MR($des_fr)."', pDes_en='".MR($des_en)."', price='".MR($price)."', message_nl='".MR($msg_nl)."', message_fr='".MR($msg_fr)."', message_en='".MR($msg_en)."', picture='".MR($pic)."', active='".MR($active)."' WHERE id='".MR($id)."'");
			
			if($qry) {
				return "0"; //no rr
			} else {
				return "2"; //rr making query
			}
			
	   	} else {

			return  "1"; //not a valid checkString
	   		
	   	}
		
		
		closedb();
		
		
	}
	
	/*
	*	BROWSE THE PRODUCTS
	*/
	
	function getProducts( $chck ) {
		
		opendb();
		
		if ($chck == "datzeiuwmoedergisterenavondook" ) {
			
			 $qry = mysql_query("SELECT * FROM products WHERE deleted = 0");
			
			if($qry) {
				return  $qry;
			} else {
				return "2"; //rr making query
			}
			
	   	} else {

			return  "1"; //not a valid checkString
	   		
	   	}
		
		closedb();
	
	}
	
	/*
	*	DELETE A PRODUCT
	*/
	
	function deleteProduct( $chck, $id ) {
		
		opendb();
		
		//
		if ($chck == "datzeiuwmoedergisterenavondook" ) {
			
			$qry = mysql_query("DELETE FROM products WHERE id='".MR($id)."'");
			
			if($qry) {
				return "0";
			} else {
				return "2";
			}
			
		} else {
			return "1";
		}
		
		closedb();
	}
	
}
?>