<?php
session_start();
$thispage = $_SERVER['PHP_SELF'];
include_once("checklang.php");
// CONTROLEER AGE CHECK

if (isset($_GET['age'])) { 
	$age = $_GET['age'];
	$_SESSION['age'] = $age;	
	
} else {
	if (isset($_SESSION['age'])) $age = $_SESSION['age'];
	else header("Location: index.php");
}
if 	($_SESSION['age'] == 0) {
	header("Location: index.php");
	}
//CONTROLEER TAAL
if (isset($_GET['lng'])) { 
	$lng = $_GET['lng'];
	$_SESSION['lng'] = $lng;	
} else {
	if (isset($_SESSION['lng'])) $lng = $_SESSION['lng'];
	else $lng = 0;
}
$_SESSION['langfile'] = check_lang($lng);
include_once($_SESSION['langfile']); 

//CONTROLEER IMG PATH
if ($lng==0) $imgpath = "nl/";
elseif ($lng==2) $imgpath = "en/";
else $imgpath = "fr/";

//includes + error reporting
include('pager.php');
include('store_admin/amfphp/services/inc/functions.php');
error_reporting(E_ERROR);

//Filters
(!isset($_SESSION['filter']))?$_SESSION['filter']="5":NULL;
if (isset($_GET['filter'])) {
	$_SESSION['filter'] = $_GET['filter'];
}

$filter = $_SESSION['filter'];
$id = $_GET['id'];

//$lng = $_GET['lng'];
//$thispage = $_SERVER['PHP_SELF'];
$brand = "jupiler";

if ($filter==5) {
	$filterQry = "";
} else {
	$filterQry = " AND category='".$filter."'";
}

//language switch
switch($lng) {
	case 0:
		$mainQry = "SELECT pName_nl, pDes_nl, price, message_nl ,picture FROM products WHERE id='".$id."' ".$filterQry;
		$lngStr = "nl";
	break;
	case 1:
		$mainQry = "SELECT pName_fr, pDes_fr, price,message_fr, picture FROM products WHERE id='".$id."' ".$filterQry;
		$lngStr = "fr";
	break;
	case 2:
		$mainQry = "SELECT pName_en, pDes_en, price, message_en, picture FROM products WHERE id='".$id."' ".$filterQry;
		$lngStr = "en";
	break;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="imagetoolbar" content="no" />
<title>index</title>
<link rel="stylesheet" href="css/style_general.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/style_jupiler.css" type="text/css" media="screen" />
<link href="css/stella_intra.css" rel="stylesheet" type="text/css" />

<script src="js/swfobject.js" type="text/javascript"></script>

<link rel="stylesheet" href="css/SIFRscreen.css" type="text/css" media="screen">
<script src="js/sifr.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
	sIFR.prefetch({
		src: 'din.swf', 
		highsrc: 'din.swf'
	});

	sIFR.compatMode = true;
	sIFR.activate();
  
	sIFR.replace({
		selector: 'h2', 
		src: 'din.swf', 
		highsrc: 'din.swf',
		css: {
		  '.sIFR-root': { 'color': '#333333' }
		}
	});
	sIFR.replace({
		selector: 'h2.intro', 
		src: 'din.swf', 
		highsrc: 'din.swf',
		css: {
		  '.sIFR-root': { 'color': '#333333' }
		}
	});
	sIFR.replace({
		selector: 'h3', 
		src: 'din.swf', 
		highsrc: 'din.swf',
		css: {
		  '.sIFR-root': { 'color': '#5a5a5a' }
		}
	});
	//]]>
</script>
<!-- PRELOAD THE IMAGES NEEDED FOR THE MENU -->
<script language="JavaScript">
	
	var act = 5;
	var out = ["img/menu_bot_on.jpg","img/menu_glass_on.jpg","img/menu_shirt_on.jpg","img/menu_col_on.jpg","img/menu_var_on.jpg","img/menu_all_on.jpg",];
	var over = ["img/menu_bot_over.jpg","img/menu_glass_over.jpg","img/menu_shirt_over.jpg","img/menu_col_over.jpg","img/menu_var_over.jpg","img/menu_all_over.jpg"];
	
	function mouseOver(ndx) {
		if(act != ndx) {
			document.getElementById(ndx).src = over[ndx];	
		}
	}
	
	function mouseOut(ndx) {
		if(act != ndx) {
			document.getElementById(ndx).src = out[ndx];
		}
	}
	
	function mouseClick(ndx) {
		document.getElementById(ndx).src = over[ndx];	
		act = ndx;
	}
	
	function initMenu(ndx) {
		act = ndx;
		document.getElementById(ndx).src = over[ndx];	
	}
	
</script>

  <script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); </script> <script type="text/javascript"> try { var pageTracker = _gat._getTracker("UA-4619313-23"); pageTracker._trackPageview(); } catch(err) {}</script>
</head>

<body onload=<? echo "initMenu(". $filter . ")" ?>>
<?
opendb();
// get the pager input values  
$page = $_GET['page'];  
$limit = 8;  
$result = mysql_query("select count(*) from products WHERE brand='jupiler' ".$filterQry);  
$total = mysql_result($result, 0, 0);  


// work out the pager values  
$pager  = Pager::getPagerData($total, $limit, $page);  
$offset = $pager->offset;  
$limit  = $pager->limit;  
$page   = $pager->page;  

$qMain = mysql_query($mainQry." LIMIT $offset, $limit");
$dMainItems = @mysql_num_rows($qMain); 
?>
<div id="wrapper">
	<div id="header">
    	<div id="lng">
        	<a href="<?php echo $thispage.'?lng=1' ?>">FR</a> - <a href="<?php echo $thispage.'?lng=0' ?>">NL</a> - <a href="<?php echo $thispage.'?lng=2' ?>">EN</a>
            <h1><a href=""><img src="img/logoBrew.png" height="90" border="0" width="150" alt="Brewery Visit" /></a></h1>
        </div>
        <div id="flashcontent">
			<strong>You need to upgrade your Flash Player</strong>
		</div>
        <img src="img/maakkeuze<?php echo $txt['img_sufix'];?>.png" width="140" height="112" border="0" />

		<script type="text/javascript">
            // <![CDATA[
            
            var so = new SWFObject("flash/menu.swf", "menu", "300", "128", "9", "#FFFFFF");
            so.addParam("wmode", "transparent");
            so.addVariable("currentpage", "2"); // this line is optional, but this example uses the variable and displays this text inside the flash movie
            so.write("flashcontent");
            
            // ]]>
        </script>
	</div>
	<div id="nav">
		<ul id="navlist">
			<li><a href="jupiler_breweryvisit.php"><img src="img/nav_bezoekdebrouwerij<?php echo $txt['img_sufix'];?>.png" height="14" border="0" /></a></li>
            <li><img src="img/nav_fanshop<?php echo $txt['img_sufix'];?>.png" height="14" border="0" /></li>
            <li><a href="jupiler_touristinfo.php"><img src="img/nav_jupilerenomg<?php echo $txt['img_sufix'];?>.png"  height="14" border="0" /></a></li>
            <li><a href="jupiler_route.php"><img src="img/nav_route<?php echo $txt['img_sufix'];?>.png" height="14" border="0" /></a></li>
            <li><a href=<?php echo "web/" . $imgpath . "jupiler/showinfo.htm" ?>><img src="img/nav_reserveren<?php echo $txt['img_sufix'];?>.png" height="14" border="0" /></a></li>
        </ul>
    </div>
    
    
    
    <div id="container">
    	<div id="slideshow" style="float:right;">
           <strong>You need to upgrade your Flash Player</strong>
        </div>
		<script type="text/javascript">
            // <![CDATA[		
            var so2 = new SWFObject("flash/imageplayer.swf", "imageplayer", "370", "390", "9", "#FFFFFF");
            so2.addVariable("brand", "jupiler");
            so2.addVariable("i1", "shop_img_01.jpg");
            so2.write("slideshow");		
            // ]]>
        </script>
    
                
        <div id="cont_t"></div>
        <div id="content">
        	<div id="innercontent">
               <?php echo $txt['jupiler_2'];?>
              <table align="center" width="570px">
                    <!-- START MENU -->
                    <tr>
                        <td>
                            <a href="jupiler_shop_images.php?page=1&lng=0&filter=5" target="_self"><img src="img/menu_all_on.jpg" name="5" id="5" onMouseover="mouseOver(5)" onMouseout="mouseOut(5)" onClick="mouseClick(5)" border="0" /></a>
                            <a href="jupiler_shop_images.php?page=1&lng=0&filter=0" target="_self"><img src="img/menu_bot_on.jpg" name="0" id="0" onMouseover="mouseOver(0)" onMouseout="mouseOut(0)" onClick="mouseClick(0)" border="0" /></a>
                            <a href="jupiler_shop_images.php?page=1&lng=0&filter=1" target="_self"><img src="img/menu_glass_on.jpg" name="1" id="1" onMouseover="mouseOver(1)" onMouseout="mouseOut(1)" onClick="mouseClick(1)" border="0" /></a>
                            <a href="jupiler_shop_images.php?page=1&lng=0&filter=2" target="_self"><img src="img/menu_shirt_on.jpg" name="2" id="2" onMouseover="mouseOver(2)" onMouseout="mouseOut(2)" onClick="mouseClick(2)" border="0" /></a>
                            <a href="jupiler_shop_images.php?page=1&lng=0&filter=3" target="_self"><img src="img/menu_col_on.jpg" name="3" id="3" onMouseover="mouseOver(3)" onMouseout="mouseOut(3)" onClick="mouseClick(3)" border="0" /></a>
                            <a href="jupiler_shop_images.php?page=1&lng=0&filter=4" target="_self"><img src="img/menu_var_on.jpg" name="4" id="4" onMouseover="mouseOver(4)" onMouseout="mouseOut(4)" onClick="mouseClick(4)" border="0" /></a>
                        </td>
                    </tr>
                    <!-- END MENU -->

</table>
<table cellpadding="0" cellspacing="0" border="0" align="center" >
        	<tr>
            	<td colspan="3" style="padding-bottom:20px; padding-top:10px;"><img src="img/store_sep.gif" width="550" /></td>
        	</tr>
            <tr align="left">
        <?php 
		$cnt=1;
        while($dMain = @mysql_fetch_assoc($qMain))	{
        ?>
                <div id="itm<?=($cnt%4==0)?'lst':NULL?>">
                          
                            <td width="270"><div id="d_picture"><img src="store_admin/library/images/thumb_480x360/<?=$dMain['picture'].".jpg"?>" border="0" width="270" /></div></td>
                            <td width="35"></td>
                            <td width="245" valign="top">
                            	<div id="d_name"><?=$dMain['pName_'.$lngStr]?></div>
                                <div id="d_description"><?=$dMain['pDes_'.$lngStr]?></div>
                                <div id="d_special">Special note:</div>
                                <div id="d_message"><?=$dMain['message']?></div>
                                <div id="d_price"><?=$dMain['price']?>&nbsp;&euro;</div>
                            </td>
                      
                      
                </div>
        <? 
        $cnt++;
        } ?>  
        </tr>
        <tr>
        	<td style="padding-top:30px;" colspan="3"><a href="javascript:history.go(-1)"><img src="img/store_tolist.gif" width="101" height="19" border="0" /></a></td>
        </tr>
        </table>
   		  </div>
        </div>
        <div id="cont_b">
        </div>
    </div>
    <div id="footer">
    	<span class="ftr_l"><?php echo $txt['terms'];?></span><span class="ftr_r"><?php echo $txt['verstand'];?></span>
    </div>
</div>
</body>
</html>
<?php closedb(); ?>